# algpath

*algpath* is a proof-of-concept Rust package for the rigorous computation of the
continuation of a regular zero of a polynomial when changing a parameter.

## Compilation

This package only supports x86-64 CPUs with AVX2 extensions. By default, rust
will compile for maximum binary compatibility and will not enable AVX2, even if
your processor features it. So make sure to set the appropriate compilation
flags, for example `RUSTFLAGS="-Ctarget-cpu=native"`.

In bash, you can build the program with the following commands:

``` sh
git clone https://gitlab.inria.fr/lairez/algpath.git
cd algpath
$RUSTFLAGS="-Ctarget-cpu=native" cargo build --release
```

Then you can try one of the example files:

``` sh
./target/release/algpath data/3-3-3-3_1.json
```

## Usage

### Input format

The executable `algpath` accepts as argument a path to a JSON file.
If the path is `-`, the data is read from the standard input.

Here is a self-documenting input example. There are many more in the [data](./data) directory.
Both `I` and `im` can be used to represent the imaginary unit.

``` json
{
  "system": [
    "(-0.932203744865119 + 0.552213152383314*I)*t*x0^2 + 12.0000000000000*t*x1^2 - t*x0 + (0.932203744865119 - 0.552213152383314*I)*x0^2 + 14.0000000000000*t*x1 + (-1.06779625513488 - 0.552213152383314*I)*t - 0.932203744865119 + 0.552213152383314*I",
    "(-2.00000000000000)*t*x0^2 + t*x0*x1 + (0.559594195166760 + 0.593167846399201*I)*t*x1^2 - t*x0 + 3.00000000000000*t*x1 + (-0.559594195166760 - 0.593167846399201*I)*x1^2 + (0.440405804833240 - 0.593167846399201*I)*t + 0.559594195166760 + 0.593167846399201*I"
  ],
  "variables": [
    "x0",
    "x1"
  ],
  "parameters": [
    "t"
  ],
  "path": [
    [
      "0.0"
    ],
    [
      "1.0"
    ]
  ],
  "fiber": [
    [
      "1.00000000000000",
      "1.00000000000000"
    ],
    [
      "-1.00000000000000 + 1.22464679914735e-16*I",
      "1.00000000000000"
    ],
    [
      "1.00000000000000",
      "-1.00000000000000 + 1.22464679914735e-16*I"
    ],
    [
      "-1.00000000000000 + 1.22464679914735e-16*I",
      "-1.00000000000000 + 1.22464679914735e-16*I"
    ]
  ]
}
```

### Command line interface

Sveral options are available. See `algpath --help` or [USAGE.md](./USAGE.md).


### Output format

Currently the program is only proof-of-concept.
At the moment we are less interested in the result than in how the calculation takes place.
So the program does not output the result.


## Authors

* Alexandre Guillemot (theoretical foundations)
* Pierre Lairez (theoretical foundations, implementation)

## Acknowledgments

The interval arithmetic used in this package is derived from the [inari](https://github.com/unageek/inari), in particular the test suite.

## Licensing

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

