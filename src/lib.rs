// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

#[cfg(not(target_feature = "avx2"))]
mod avx_unavailable {
    compile_error!("AVX2 is not enabled. This program only support x86-64 with AVX2 extensions.");
}

pub mod circuit;
pub mod num;
pub mod parser;
pub mod prog;
pub mod taylormodel;
pub mod track;
pub mod cli;
pub mod reckless_interval;
