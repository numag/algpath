#![allow(dead_code)]
// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later


use super::circuit;
use super::num::Num;
use circuit::Circuit;
use circuit::Node;
use circuit::Visitor;
use std::collections::HashMap;
use std::collections::HashSet;
use std::marker::PhantomData;

#[derive(Debug, Clone, Copy)]
pub enum Instruction {
    Move(usize, usize),
    Add(usize, usize),
    Mul(usize, usize),
    AddScalar(usize, usize),
    MulScalar(usize, usize),
    Neg(usize),
    Scalar(usize, usize),
    Zero(usize),
    Nop,
}

use Instruction::*;

#[derive(Debug, Clone)]
pub struct Program<T: Num> {
    pub scalars: Vec<T>,
    pub instructions: Vec<Instruction>,
    pub vars: Vec<Circuit<T>>,
    pub outnodes: Vec<usize>,
    pub bufferlen: usize,
}

impl<T: Num> Program<T> {
    pub fn compile<'a, I: IntoIterator<Item = &'a Circuit<T>>>(circuits: I, vars: I) -> Self
    where
        T: 'a,
    {
        // 1. Compute the sequence of all node to be computed. Gather the output
        // nodes (which are the elements of the inputs of this function) in
        // outnodeset.
        let mut ord = circuit::TopologicalOrderingVisitor {
            nodes: Default::default(),
        };

        let mut outnodeset = HashSet::new();
        let mut outnodelist = Vec::new();
        let mut memo = Default::default();
        for circ in circuits {
            ord.compute(circ, &mut memo);
            outnodeset.insert(circ);
            outnodelist.push(circ.clone())
        }

        // 2. Initialize the program.
        let mut prog = Program {
            scalars: Default::default(),
            instructions: Default::default(),
            vars: Default::default(),
            outnodes: Default::default(),

            // Length of the buffer that we will need. We make so basic effort to
            // reuse free cells, but reordering the computation to minimize
            // bufferlen is a difficult combinatorial optimization problem.
            bufferlen: 0,
        };

        // 3. First pass over the nodes.

        // Associate to each intermediate circuit, the cell of prog.buffer in
        // which it will be computed.
        let mut indices = HashMap::<&Circuit<T>, usize>::default();

        // The input variables are allocated the firt indices
        for (i, v) in vars.into_iter().enumerate() {
            indices.insert(v, i);
            prog.vars.push(v.clone());
        }
        prog.bufferlen = prog.vars.len();

        // Associate to each intermediate circuit the number of times it is used
        // afterwards.
        let mut nb_read = HashMap::<Circuit<T>, usize>::default();

        // iterate over the intermediate circuit, in topological order: every
        // argument of an operations are treated before the operation.
        for circ in &ord.nodes {
            nb_read.insert(circ.clone(), if outnodeset.contains(circ) { 1 } else { 0 });
            match circ.node.as_ref() {
                Node::Add(x, y) | Node::Mul(x, y) => {
                    // The first argument cannot be a scalar. This is enforced by
                    // Node::simplify.
                    assert!(x.get_scalar().is_none());
                    *nb_read.get_mut(x).unwrap() += 1;
                    *nb_read.get_mut(y).unwrap() += 1;
                }
                Node::Neg(x) => {
                    // Enforced by Node::simplify
                    assert!(x.get_scalar().is_none());
                    *nb_read.get_mut(x).unwrap() += 1;
                }
                Node::Scalar(_) | Node::Zero => (),
                Node::Var(_) => {
                    assert!(indices.contains_key(circ));
                }
            };
        }

        // 4. Second pass

        let mut scalars_indices = HashMap::<T, usize>::default();
        let mut emitscalar = |c: &T| {
            if let Some(i) = scalars_indices.get(c) {
                *i
            } else {
                let i = prog.scalars.len();
                scalars_indices.insert(c.clone(), i);
                prog.scalars.push(c.clone());
                i
            }
        };

        // Contains all the free cells
        let mut freelist = Vec::<usize>::new();
        // let mut forgotten_nodes = HashSet::new();

        for circ in &ord.nodes {
            //dbg!(circ);

            // Find the index at which the result of the computation will be
            // stored.
            let mut getidx = || {
                let idx;
                if let Some(i) = freelist.pop() {
                    idx = i;
                } else {
                    idx = prog.bufferlen;
                    prog.bufferlen += 1;
                }
                indices.insert(circ, idx);
                idx
            };

            match circ.node.as_ref() {
                Node::Add(x, y) => {
                    let idx = getidx();
                    if let Node::Scalar(s) = y.node.as_ref() {
                        if idx != indices[x] {
                            prog.instructions.push(Move(idx, indices[x]));
                        }
                        prog.instructions.push(AddScalar(idx, emitscalar(s)));
                    } else {
                        if idx == indices[x] {
                            prog.instructions.push(Add(idx, indices[y]));
                        } else {
                            if idx != indices[y] {
                                prog.instructions.push(Move(idx, indices[y]));
                            }
                            prog.instructions.push(Add(idx, indices[x]));
                        }
                    }
                }
                Node::Mul(x, y) => {
                    let idx = getidx();
                    if let Node::Scalar(s) = y.node.as_ref() {
                        if idx != indices[x] {
                            prog.instructions.push(Move(idx, indices[x]));
                        }
                        prog.instructions.push(MulScalar(idx, emitscalar(s)));
                    } else {
                        if idx == indices[x] {
                            prog.instructions.push(Mul(idx, indices[y]));
                        } else if idx == indices[y] {
                            prog.instructions.push(Mul(idx, indices[x]));
                        } else {
                            prog.instructions.push(Move(idx, indices[x]));
                            prog.instructions.push(Mul(idx, indices[y]));
                        }
                    }
                }

                Node::Neg(x) => {
                    let idx = getidx();
                    if idx != indices[x] {
                        prog.instructions.push(Move(idx, indices[x]));
                    }
                    prog.instructions.push(Neg(idx));
                }
                Node::Var(_) => (),
                Node::Scalar(s) => {
                    if outnodeset.contains(circ) {
                        let idx = getidx();
                        prog.instructions.push(Scalar(idx, emitscalar(s)));
                    }
                }
                Node::Zero => {
                    let idx = getidx();
                    prog.instructions.push(Zero(idx));
                }
            }

            // Decrement the "nbread" counters and update freelist accordingly.
            match circ.node.as_ref() {
                Node::Add(x, y) | Node::Mul(x, y) => {
                    let rx = nb_read.get_mut(x).unwrap();
                    *rx -= 1;
                    if *rx == 0 {
                        if let Some(i) = indices.get(x) {
                            freelist.push(*i);
                            indices.remove(x);
                            //dbg!(&indices);
                        }
                    }
                    let ry = nb_read.get_mut(y).unwrap();
                    *ry -= 1;
                    if *ry == 0 {
                        if let Some(i) = indices.get(y) {
                            freelist.push(*i);
                            indices.remove(y);
                            //dbg!(&indices);
                        }
                    }
                }
                Node::Neg(x) => {
                    let rx = nb_read.get_mut(x).unwrap();
                    *rx -= 1;
                    if *rx == 0 {
                        if let Some(i) = indices.get(x) {
                            freelist.push(*i);
                            indices.remove(x);
                            //dbg!(&indices);
                        }
                    }
                }
                Node::Var(_) | Node::Scalar(_) | Node::Zero => (),
            }

        }

        prog.outnodes.reserve_exact(outnodelist.len());
        for circ in &outnodelist {
            prog.outnodes.push(indices[circ]);
        }

        return prog;
    }
}

pub trait Evaluator<S: Num, T: Num>
where
    T: Into<S>,
{
    fn add_assign(x: &mut S, y: &S);
    fn scalar_add_assign(x: &mut S, y: &T);
    fn mul_assign(x: &mut S, y: &S);
    fn scalar_mul_assign(x: &mut S, y: &T);

    fn maybe_set_csr() -> Option<u32> {
        None
    }
    fn maybe_restore_csr(_: Option<u32>) {}

    #[inline(never)]
    fn eval(prog: &Program<T>, buf: &mut Vec<S>) -> Vec<S> {
        if buf.len() < prog.bufferlen {
            buf.resize(prog.bufferlen, S::zero());
        }

        let csr = Self::maybe_set_csr();
        for instr in &prog.instructions {
            match instr {
                Move(dest, x) => unsafe { *buf.get_unchecked_mut(*dest) = *buf.get_unchecked(*x) },

                Add(dest, x) => unsafe {
                    let a = buf[*x];
                    Self::add_assign(buf.get_unchecked_mut(*dest), &a);
                },
                Mul(dest, x) => unsafe {
                    let a = buf[*x];
                    Self::mul_assign(buf.get_unchecked_mut(*dest), &a);
                },
                Neg(dest) => unsafe {
                    let c = buf.get_unchecked_mut(*dest);
                    *c = -*c;
                },
                AddScalar(dest, i) => unsafe {
                    Self::scalar_add_assign(buf.get_unchecked_mut(*dest), prog.scalars.get_unchecked(*i))
                },
                MulScalar(dest, i) => unsafe {
                    Self::scalar_mul_assign(buf.get_unchecked_mut(*dest), prog.scalars.get_unchecked(*i))
                },

                Scalar(dest, i) => unsafe {
                    *buf.get_unchecked_mut(*dest) = (*prog.scalars.get_unchecked(*i)).into()
                },
                Zero(dest) => unsafe { *buf.get_unchecked_mut(*dest) = S::zero() },

                Nop => (),
            }
        }
        Self::maybe_restore_csr(csr);

        let mut result = Vec::default();
        result.reserve_exact(prog.outnodes.len());
        for i in &prog.outnodes {
            result.push(buf[*i]);
        }

        return result;
    }

    fn eval_map(prog: &Program<T>, input: &HashMap<Circuit<T>, S>, buf: &mut Vec<S>) -> Vec<S> {
        if buf.len() < prog.bufferlen {
            buf.resize(prog.bufferlen, S::zero());
        }

        for (i, v) in prog.vars.iter().enumerate() {
            buf[i] = input[v].clone();
        }

        Self::eval(prog, buf)
    }
}

pub struct DefaultEvaluator<S: Num, T: Num> {
    _pd: PhantomData<(S, T)>,
}

impl<S: Num, T: Num> Evaluator<S, T> for DefaultEvaluator<S, T>
where
    T: Into<S>,
    S: std::ops::AddAssign<T>,
    S: std::ops::MulAssign<T>,
{
    fn add_assign(x: &mut S, y: &S) {
        *x += *y;
    }
    fn scalar_add_assign(x: &mut S, y: &T) {
        *x += *y
    }
    fn mul_assign(x: &mut S, y: &S) {
        *x *= *y
    }
    fn scalar_mul_assign(x: &mut S, y: &T) {
        *x *= *y
    }
}

#[cfg(test)]
mod evaluation {
    use super::*;

    #[test]
    fn test1() {
        let ctx = circuit::Context::<i64>::default();
        let x = ctx.var("x");
        let y = ctx.var("y");
        let z = -(ctx.scalar(1) + &x + ctx.zero() - ctx.scalar(2) * &y).pow(25);

        let prog = Program::<i64>::compile(&[z.clone()][..], &[x.clone(), y.clone()][..]);
        let mut pt = std::collections::HashMap::default();
        pt.insert(x, 1);
        pt.insert(y, 2);

        // dbg!(&prog);

        // dbg!(prog.eval(&pt)[0]);
        // dbg!(z.eval(&pt).unwrap());
        let mut buf = Vec::new();
        assert!(DefaultEvaluator::eval_map(&prog, &pt, &mut buf)[0] == z.eval(&pt).unwrap());
    }
}
