#![allow(dead_code)]
// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later


use std::{
    borrow::Borrow,
    collections::{HashMap, HashSet},
    fmt::{self, Debug, Display},
    hash::Hash,
    sync::{Weak, Arc, RwLock}, marker::PhantomData,
};
use nalgebra::DMatrix;
use weak_table::WeakHashSet;

use super::num::Num;

#[derive(Debug, Clone)]
pub struct Context<T: Num> {
    nodes: Arc<RwLock<WeakHashSet<Weak<Node<T>>>>>,
}

pub struct Circuit<T: Num> {
    pub node: Arc<Node<T>>,
    context: Context<T>,
}

#[derive(Clone, Copy, clap::ValueEnum)]
pub enum DifferentiationMode {
    Forward,
    Backward,
}

impl<T: Num> PartialEq for Circuit<T> {
    fn eq(&self, other: &Self) -> bool {
        return Arc::ptr_eq(&self.context.nodes, &other.context.nodes)
            && Arc::ptr_eq(&self.node, &other.node);
    }
}

impl<T: Num> Hash for Circuit<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        Arc::as_ptr(&self.node).hash(state);
        Arc::as_ptr(&self.context.nodes).hash(state);
    }
}

impl<T: Num> fmt::Debug for Circuit<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.node.fmt(f)
    }
}

impl<T: Num> Eq for Circuit<T> {}

impl<T: Num> Clone for Circuit<T> {
    fn clone(&self) -> Self {
        Circuit {
            node: self.node.clone(),
            context: self.context.clone(),
        }
    }
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum Node<T: Num> {
    Add(Circuit<T>, Circuit<T>),
    Mul(Circuit<T>, Circuit<T>),
    Neg(Circuit<T>),
    Var(String),
    Scalar(T),
    Zero,
}

use Node::*;


impl<T:Num + Display> Display for Circuit<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.node.borrow() {
            Add(x, y) => write!(f, "({} + {})", x, y),
            Mul(x, y) => write!(f, "({} * {})", x, y),
            Neg(x) => write!(f, "(-{})", x),
            Var(name) => write!(f, "{}", name),
            Scalar(t) => write!(f, "({})" , t),
            Zero => write!(f, "({})" , T::zero()),
        }
    }
}

// Basic constructors
impl<T: Num> Context<T> {
    fn hashcons(self, node: Node<T>) -> Circuit<T> {
        let node_weak_set = self.nodes.clone();
        let mut lock = node_weak_set.write().unwrap();
        let weak = lock.get(&node);
        match weak {
            Some(rc) => Circuit {
                node: rc,
                context: self,
            },
            None => {
                let rc = Arc::new(node);
                lock.insert(rc.clone());
                Circuit {
                    node: rc,
                    context: self,
                }
            }
        }
    }

    pub fn var<S: Into<String>>(&self, str: S) -> Circuit<T> {
        self.clone().hashcons(Node::Var(str.into()))
    }

    pub fn zero(&self) -> Circuit<T> {
        self.clone().hashcons(Node::Zero)
    }

    pub fn scalar(&self, c: T) -> Circuit<T> {
        if c.is_zero() {
            self.zero()
        } else {
            self.clone().hashcons(Node::Scalar(c))
        }
    }
}

impl<T: Num> Default for Context<T> {
    fn default() -> Self {
        return Context {
            nodes: Default::default(),
        };
    }
}

impl<T: Num> Circuit<T> {
    pub fn get_var_name(&self) -> Option<&String> {
        if let Var(name) = self.node.as_ref() {
            Some(name)
        } else {
            None
        }
    }
    pub fn get_scalar(&self) -> Option<&T> {
        if let Scalar(s) = self.node.as_ref() {
            Some(s)
        } else {
            None
        }
    }
    pub fn is_zero(&self) -> bool {
        match *self.node {
            Zero => true,
            Scalar(ref s) if s.is_zero() => true,
            _ => false,
        }
    }

    pub fn context(&self) -> Context<T> {
        self.context.clone()
    }
}

// Visitor

pub trait Visitor<T: Num> {
    type Output: Clone;

    fn zero(&mut self, c: &Circuit<T>) -> Self::Output;
    fn add(&mut self, c: &Circuit<T>, x: Self::Output, y: Self::Output) -> Self::Output;
    fn mul(&mut self, c: &Circuit<T>, x: Self::Output, y: Self::Output) -> Self::Output;
    fn neg(&mut self, c: &Circuit<T>, x: Self::Output) -> Self::Output;
    fn var(&mut self, c: &Circuit<T>, name: &String) -> Self::Output;
    fn scalar(&mut self, c: &Circuit<T>, t: &T) -> Self::Output;

    fn compute(
        &mut self,
        c: &Circuit<T>,
        memo: &mut HashMap<Circuit<T>, Self::Output>,
    ) -> Self::Output {
        match memo.get(c) {
            Some(val) => val.clone(),
            None => {
                let val = match c.node.as_ref() {
                    Add(cx, cy) => {
                        let x = self.compute(cx, memo);
                        let y = self.compute(cy, memo);
                        self.add(c, x, y)
                    }
                    Mul(cx, cy) => {
                        let x = self.compute(cx, memo);
                        let y = self.compute(cy, memo);
                        self.mul(c, x, y)
                    }
                    Neg(cx) => {
                        let x = self.compute(cx, memo);
                        self.neg(c, x)
                    }
                    Var(name) => self.var(c, name),
                    Scalar(t) => self.scalar(c, t),
                    Zero => self.zero(c),
                };

                memo.insert(c.clone(), val.clone());
                val
            }
        }
    }
}

// Arithmetic operations

impl<T: Num> Node<T> {
    fn simplify(self) -> Self {
        match &self {
            Zero => self,
            Add(x, y) => match (x.node.as_ref(), y.node.as_ref()) {
                (Scalar(a), Scalar(b)) => Scalar(*a + *b),
                (Scalar(_), _) => Add(y.clone(), x.clone()).simplify(),
                (a, Zero) => a.clone(),
                (Zero, b) => b.clone(),
                (Neg(a), Neg(b)) => Neg(a.clone() + b).simplify(),
                (Neg(a), Scalar(_)) => Neg(a.clone() - y),
                _ => self,
            },
            Mul(x, y) => match (x.node.as_ref(), y.node.as_ref()) {
                (Scalar(a), Scalar(b)) => Scalar(*a * *b),
                (Scalar(_), _) => Mul(y.clone(), x.clone()).simplify(),
                (_, Zero) | (Zero, _) => Zero,
                (a, Scalar(s)) if s.is_one() => a.clone(),
                (_, Neg(b)) => Neg(x.clone() * b).simplify(),
                (Neg(a), _) => Neg(y.clone() * a).simplify(),
                _ => self,
            },
            Neg(x) => match x.node.as_ref() {
                Scalar(a) => Scalar(-a.clone()),
                Zero => Zero,
                Neg(z) => z.node.as_ref().clone(),
                _ => self,
            },
            Var(_) => self,
            Scalar(t) => {
                if t.is_zero() {
                    Zero
                } else {
                    self
                }
            }
        }
    }
}

impl<T: Num, Rhs: Borrow<Circuit<T>>> std::ops::Add<Rhs> for Circuit<T> {
    type Output = Circuit<T>;
    fn add(self, other: Rhs) -> Self::Output {
        self.context
            .clone()
            .hashcons(Add(self.clone(), other.borrow().to_owned()).simplify())
    }
}

impl<T: Num, Rhs: Borrow<Circuit<T>>> std::ops::Sub<Rhs> for Circuit<T> {
    type Output = Circuit<T>;
    fn sub(self, other: Rhs) -> Self::Output {
        self + (-other.borrow())
    }
}

impl<T: Num, Rhs: Borrow<Circuit<T>>> std::ops::Mul<Rhs> for Circuit<T> {
    type Output = Circuit<T>;
    fn mul(self, other: Rhs) -> Self::Output {
        self.context
            .clone()
            .hashcons(Node::Mul(self, other.borrow().to_owned()).simplify())
    }
}

impl<T: Num> std::ops::Neg for Circuit<T> {
    type Output = Circuit<T>;
    fn neg(self) -> Self::Output {
        let ctx = self.context.clone();
        return ctx.hashcons(Node::Neg(self).simplify());
    }
}

impl<T: Num> std::ops::Neg for &Circuit<T> {
    type Output = Circuit<T>;
    fn neg(self) -> Self::Output {
        -self.clone()
    }
}

// impl<T: Num> Circuit<T> {
//     pub fn pow(&self, n: i64) -> Circuit<T> {
//         if n < 0 {
//             panic!("negative exponent")
//         } else if n == 0 {
//             self.context.scalar(T::one())
//         } else if n == 1 {
//             self.clone()
//         } else if n == 2 {
//             self.clone() * self.clone()
//         } else {
//             let m = n / 2;
//             let d = self.pow(m).pow(2);
//             if 2 * m == n {
//                 return d;
//             } else {
//                 return d * self;
//             }
//         }
//     }
// }

impl<T: Num> Circuit<T> {
    pub fn pow(&self, mut n: i64) -> Circuit<T> {
        if n < 0 {
            panic!("negative exponent")
        }

        let mut p = self.clone();
        let mut res: Option<Circuit<T>> = None;

        while n > 0 {
            if n % 2 != 0 {
                if let Some(ref mut r) = res {
                    *r = r.clone() * &p;
                } else {
                    res = Some(p.clone());
                }
            }
            p = p.clone() * &p;
            n /= 2;
        }

        if let Some(r) = res {
            return r;
        } else {
            return self.context.scalar(T::one());
        }
    }
}

#[cfg(test)]
mod arithmetic_and_deduplication {

    use super::*;

    #[test]
    fn test1() {
        let ctx = Context::<i64>::default();
        let x = ctx.var("x");
        let y = ctx.var("y");

        let z = x.clone() + ctx.scalar(2) * &y + x.clone() * &y;
        let zbis = x.clone() + ctx.scalar(2) * &y + x.clone() * &y;
        assert!(Arc::ptr_eq(&z.node, &zbis.node));

        let ybis = ctx.var("y");
        assert!(Arc::ptr_eq(&y.node, &ybis.node));
    }
}
// Evaluation

struct EvalVisitor<'a, T: Num> {
    pt: &'a HashMap<Circuit<T>, T>,
}

impl<'a, T: Num> Visitor<T> for EvalVisitor<'a, T> {
    type Output = Option<T>;

    fn zero(&mut self, _: &Circuit<T>) -> Self::Output {
        Some(T::zero())
    }
    fn add(&mut self, _: &Circuit<T>, x: Self::Output, y: Self::Output) -> Self::Output {
        Some(x? + y?)
    }
    fn mul(&mut self, _: &Circuit<T>, x: Self::Output, y: Self::Output) -> Self::Output {
        Some(x? * y?)
    }
    fn neg(&mut self, _: &Circuit<T>, x: Self::Output) -> Self::Output {
        Some(-x?)
    }
    fn var(&mut self, c: &Circuit<T>, _: &String) -> Self::Output {
        self.pt.get(c).cloned()
    }
    fn scalar(&mut self, _: &Circuit<T>, t: &T) -> Self::Output {
        Some(t.clone())
    }
}

impl<T: Num> Circuit<T> {
    pub fn eval_with_memo(
        &self,
        pt: &HashMap<Circuit<T>, T>,
        memo: &mut HashMap<Circuit<T>, Option<T>>,
    ) -> Option<T> {
        EvalVisitor { pt }.compute(self, memo)
    }
    pub fn eval(&self, pt: &HashMap<Circuit<T>, T>) -> Option<T> {
        self.eval_with_memo(pt, &mut Default::default())
    }
}

#[cfg(test)]
mod evaluation {
    use super::*;
    #[test]
    fn test1() {
        let ctx = Context::<i64>::default();
        let x = ctx.var("x");
        let y = -(ctx.scalar(1) + &x + ctx.zero()).pow(25);

        let mut pt = HashMap::default();
        pt.insert(x, 1);

        assert!(y.eval(&pt) == Some(-(1i64 << 25)));
    }
}

// Forward derivative

pub struct ForwardDerivativeVisitor<T: Num> {
    var: Circuit<T>,
}

impl<T: Num> Visitor<T> for ForwardDerivativeVisitor<T> {
    type Output = Circuit<T>;

    fn zero(&mut self, c: &Circuit<T>) -> Self::Output {
        c.context.zero()
    }
    fn add(&mut self, _: &Circuit<T>, dx: Self::Output, dy: Self::Output) -> Self::Output {
        dx + dy
    }
    fn mul(&mut self, c: &Circuit<T>, dx: Self::Output, dy: Self::Output) -> Self::Output {
        if let Mul(x, y) = c.node.as_ref() {
            x.clone() * dy + dx * y.clone()
        } else {
            panic!();
        }
    }
    fn neg(&mut self, _: &Circuit<T>, dx: Self::Output) -> Self::Output {
        -dx
    }
    fn var(&mut self, c: &Circuit<T>, _: &String) -> Self::Output {
        if c == &self.var {
            c.context.scalar(T::one())
        } else {
            c.context.zero()
        }
    }
    fn scalar(&mut self, c: &Circuit<T>, _: &T) -> Self::Output {
        c.context.zero()
    }
}

impl<T: Num> Circuit<T> {
    pub fn derivative_with_memo(
        &self,
        var: &Circuit<T>,
        memo: &mut HashMap<Circuit<T>, Circuit<T>>,
    ) -> Circuit<T> {
        ForwardDerivativeVisitor { var: var.clone() }.compute(self, memo)
    }
    pub fn derivative(&self, var: &Circuit<T>) -> Circuit<T> {
        self.derivative_with_memo(var, &mut Default::default())
    }
}

#[cfg(test)]
mod forward_derivative {
    use super::*;
    #[test]
    fn test1() {
        let ctx = Context::<i64>::default();
        let x = ctx.var("x");
        let y = ctx.var("y");
        let z = (-&x + ctx.scalar(1) + y.clone() * &y).pow(10);

        let mut pt = HashMap::default();
        pt.insert(x.clone(), 1);
        pt.insert(y.clone(), 2);

        assert!(z.eval(&pt) == Some(1048576));

        let dzdx = z.derivative(&x);
        assert!(dzdx.eval(&pt) == Some(-2621440));

        let dzdy = z.derivative(&y);
        assert!(dzdy.eval(&pt) == Some(10485760));
    }
}

pub struct TopologicalOrderingVisitor<T: Num> {
    pub nodes: Vec<Circuit<T>>,
}

impl<T: Num> Visitor<T> for TopologicalOrderingVisitor<T> {
    type Output = ();

    fn zero(&mut self, c: &Circuit<T>) -> Self::Output {
        self.nodes.push(c.clone())
    }
    fn add(&mut self, c: &Circuit<T>, _: Self::Output, _: Self::Output) -> Self::Output {
        self.nodes.push(c.clone())
    }
    fn mul(&mut self, c: &Circuit<T>, _: Self::Output, _: Self::Output) -> Self::Output {
        self.nodes.push(c.clone())
    }
    fn neg(&mut self, c: &Circuit<T>, _: Self::Output) -> Self::Output {
        self.nodes.push(c.clone())
    }
    fn var(&mut self, c: &Circuit<T>, _: &String) -> Self::Output {
        self.nodes.push(c.clone())
    }
    fn scalar(&mut self, c: &Circuit<T>, _: &T) -> Self::Output {
        self.nodes.push(c.clone())
    }
}

impl<T: Num> Circuit<T> {
    fn topological_ordering(&self) -> Vec<Circuit<T>> {
        let mut topo = TopologicalOrderingVisitor {
            nodes: Default::default(),
        };
        topo.compute(self, &mut Default::default());
        return topo.nodes;
    }

    pub fn grad(&self) -> HashMap<Circuit<T>, Circuit<T>> {
        let topo = self.topological_ordering();
        let mut diff: HashMap<Circuit<T>, Circuit<T>> = Default::default();
        let z = self.context.zero();
        diff.insert(self.clone(), self.context.scalar(T::one()));
        for c in topo.iter().rev() {
            let seed = diff.get(c).unwrap().clone();
            match c.node.as_ref() {
                Add(x, y) => {
                    // diff[x] += seed
                    // diff[y] += seed
                    diff.insert(x.clone(), diff.get(x).unwrap_or(&z).clone() + &seed);
                    diff.insert(y.clone(), diff.get(y).unwrap_or(&z).clone() + &seed);
                }
                Mul(x, y) => {
                    // diff[x] += y * seed
                    diff.insert(
                        x.clone(),
                        diff.get(x).unwrap_or(&z).clone() + y.clone() * &seed,
                    );
                    // diff[x] += x * seed
                    diff.insert(
                        y.clone(),
                        diff.get(y).unwrap_or(&z).clone() + x.clone() * &seed,
                    );
                }
                Neg(x) => {
                    diff.insert(x.clone(), diff.get(x).unwrap_or(&z).clone() - seed.clone());
                }

                Var(_) => {}
                Scalar(_) => {}
                Zero => {}
            }
        }
        diff
    }
}

#[cfg(test)]
mod backward_derivative {
    use super::*;
    #[test]
    fn test1() {
        let ctx = Context::<i64>::default();
        let x = ctx.var("x");
        let y = ctx.var("y");
        let z = (-&x + ctx.scalar(1) + y.clone() * &y).pow(10);

        let mut pt = HashMap::default();
        pt.insert(x.clone(), 1);
        pt.insert(y.clone(), 2);

        assert!(z.eval(&pt) == Some(1048576));

        let grad = z.grad();

        let dzdx = z.derivative(&x);
        assert!(dzdx.eval(&pt) == grad.get(&x).unwrap().eval(&pt));

        let dzdy = z.derivative(&y);
        assert!(dzdy.eval(&pt) == grad.get(&y).unwrap().eval(&pt));
    }
}

mod deserialize {
    use super::{Circuit, Context};
    use crate::num::CIF;

    thread_local! {
    static CONTEXT : Context<CIF> = Default::default();
    }

    struct CircuitVisitor;

    impl<'de> serde::de::Visitor<'de> for CircuitVisitor {
        type Value = Circuit<CIF>;

        fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
            write!(formatter, "a string representation of a polynomial")
        }

        fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
        where
            E: serde::de::Error,
        {
            CONTEXT.with(|ctx| crate::parser::EXPR.parse(ctx, s).map_err(|e| E::custom(e)))
        }
    }

    impl<'de> serde::Deserialize<'de> for Circuit<CIF> {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: serde::Deserializer<'de>,
        {
            deserializer.deserialize_str(CircuitVisitor)
        }
    }
}

pub struct CollectVisitor<T: Num> {
    var: Circuit<T>,
}

impl<T: Num> Visitor<T> for CollectVisitor<T> {
    type Output = Vec<Circuit<T>>;

    fn zero(&mut self, c: &Circuit<T>) -> Self::Output {
        [c.context.zero()].into()
    }

    fn add(&mut self, _: &Circuit<T>, mut cx: Self::Output, mut cy: Self::Output) -> Self::Output {
        if cx.len() < cy.len() {
            (cx, cy) = (cy, cx)
        }

        for i in 0..cy.len() {
            cx[i] = cx[i].clone() + cy[i].clone()
        }

        while cx.last().is_some_and(|c| c.is_zero()) {
            cx.pop();
        }

        return cx;
    }

    fn mul(&mut self, c: &Circuit<T>, cx: Self::Output, cy: Self::Output) -> Self::Output {
        let mut ret = Vec::new();
        if cx.len() + cy.len() > 0 {
            ret.resize(cx.len() + cy.len() - 1, c.context.zero());
            for i in 0..cx.len() {
                for j in 0..cy.len() {
                    ret[i + j] = ret[i + j].clone() + cx[i].clone() * cy[j].clone();
                }
            }
        }
        return ret;
    }

    fn neg(&mut self, _: &Circuit<T>, mut cx: Self::Output) -> Self::Output {
        for c in &mut cx {
            *c = -c.clone();
        }
        return cx;
    }

    fn var(&mut self, c: &Circuit<T>, _: &String) -> Self::Output {
        if c == &self.var {
            vec![c.context.zero(), c.context.scalar(T::one())]
        } else {
            vec![c.clone()]
        }
    }

    fn scalar(&mut self, c: &Circuit<T>, _: &T) -> Self::Output {
        vec![c.clone()]
    }
}

impl<T: Num> Circuit<T> {
    pub fn collect_with_memo(
        &self,
        var: &Circuit<T>,
        memo: &mut HashMap<Circuit<T>, Vec<Circuit<T>>>,
    ) -> Vec<Circuit<T>> {
        CollectVisitor { var: var.clone() }.compute(self, memo)
    }

    pub fn collect(&self, var: &Circuit<T>) -> Vec<Circuit<T>> {
        self.collect_with_memo(var, &mut Default::default())
    }

    pub fn horner(&self, vars: &[Circuit<T>]) -> Circuit<T> {
        if let Some(v) = vars.first() {
            let mut cs = self.collect(&v);
            for c in &mut cs {
                *c = c.horner(&vars[1..]);
            }

            let mut res = self.context.zero();
            for c in cs.iter().rev() {
                res = res * v + c;
            }

            return res;
        } else {
            return self.clone();
        }
    }
}

#[cfg(test)]
mod collect {
    use super::*;
    #[test]
    fn test_collect() {
        let ctx = Context::<i64>::default();
        let x = ctx.var("x");
        let y = -(ctx.scalar(1) + &x + ctx.zero()).pow(25);

        let c = y.collect(&x);

        assert!(c.len() == 26);

        assert!(
            c[0].get_scalar() == Some(&-1)
                && c[13].get_scalar() == Some(&-5200300)
                && c[25].get_scalar() == Some(&-1)
        );
    }

    #[test]
    fn test_horner_single_var() {
        let ctx = Context::<i64>::default();
        let x = ctx.var("x");
        let y = -(ctx.scalar(1) + &x + ctx.zero()).pow(10);

        let h = y.horner(&[x.clone()]);

        let mut pt = HashMap::default();
        pt.insert(x.clone(), 1);

        assert!(y.eval(&pt) == h.eval(&pt));
    }

    #[test]
    fn test_horner_two_vars_1() {
        let ctx = Context::<i64>::default();
        let x = ctx.var("x");
        let y = ctx.var("y");
        let z = (-(x.clone() + ctx.scalar(-1)) + y.clone() * &y)
            * (-(x.clone() + ctx.scalar(-1)) + y.clone() * &y);
        let h = z.horner(&[x.clone()]);

        let mut pt = HashMap::default();
        pt.insert(x.clone(), 5);
        pt.insert(y.clone(), 2);

        assert!(z.eval(&pt) == h.eval(&pt));
    }

    #[test]
    fn test_horner_two_vars_2() {
        let ctx = Context::<i64>::default();
        let x = ctx.var("x");
        let y = ctx.var("y");
        let z = (-&x + ctx.scalar(1) + y.clone() * &y).pow(10);

        let mut pt = HashMap::default();
        pt.insert(x.clone(), 1);
        pt.insert(y.clone(), 2);

        let val = Some(1048576);
        assert!(z.eval(&pt) == val);

        let h1 = z.horner(&[x.clone(), y.clone()]);
        let h2 = z.horner(&[y.clone(), x.clone()]);
        let h3 = h1.horner(&[y.clone(), x.clone()]);

        assert!(h1.eval(&pt) == z.eval(&pt));
        assert!(h2 == h3);
    }
}

pub struct HomogenizationVisitor<T: Num> {
    pub var: Circuit<T>,
    pub othervars: HashSet<Circuit<T>>,
}

impl<T: Num> Visitor<T> for HomogenizationVisitor<T> {
    type Output = (i64, Circuit<T>);

    fn zero(&mut self, c: &Circuit<T>) -> Self::Output {
        (0, c.clone())
    }
    fn add(
        &mut self,
        _: &Circuit<T>,
        (dx, cx): Self::Output,
        (dy, cy): Self::Output,
    ) -> Self::Output {
        if dx >= dy {
            (dx, cx + self.var.pow(dx - dy) * cy)
        } else {
            (dy, self.var.pow(dy - dx) * cx + cy)
        }
    }
    fn mul(
        &mut self,
        _: &Circuit<T>,
        (dx, cx): Self::Output,
        (dy, cy): Self::Output,
    ) -> Self::Output {
        (dx + dy, cx * cy)
    }
    fn neg(&mut self, _: &Circuit<T>, (dx, cx): Self::Output) -> Self::Output {
        (dx, -cx)
    }
    fn var(&mut self, c: &Circuit<T>, _: &String) -> Self::Output {
        assert!(c != &self.var);
        if self.othervars.contains(c) {
            (1, c.clone())
        } else {
            (0, c.clone())
        }
    }
    fn scalar(&mut self, c: &Circuit<T>, _: &T) -> Self::Output {
        (0, c.clone())
    }
}

impl<T: Num> Circuit<T> {
    pub fn homogenize_with_memo(
        &self,
        vars: &[Circuit<T>],
        var: &Circuit<T>,
        memo: &mut HashMap<Circuit<T>, (i64, Circuit<T>)>,
    ) -> (i64, Circuit<T>) {
        HomogenizationVisitor {
            var: var.clone(),
            othervars: HashSet::from_iter(vars.iter().cloned()),
        }
        .compute(self, memo)
    }
    pub fn homogenize(&self, vars: &[Circuit<T>], var: &Circuit<T>) -> (i64, Circuit<T>) {
        self.homogenize_with_memo(vars, var, &mut HashMap::new())
    }
}

pub struct SubsVisitor<T: Num> {
    _pd: PhantomData<T>
}

impl<T: Num> Visitor<T> for SubsVisitor<T> {
    type Output = Circuit<T>;

    fn zero(&mut self, c: &Circuit<T>) -> Self::Output {
        c.clone()
    }
    fn add(&mut self, _: &Circuit<T>, x: Self::Output, y: Self::Output) -> Self::Output {
        x + y
    }
    fn mul(&mut self, _: &Circuit<T>, x: Self::Output, y: Self::Output) -> Self::Output {
        x * y
    }
    fn neg(&mut self, _: &Circuit<T>, x: Self::Output) -> Self::Output {
        -x
    }
    fn var(&mut self, c: &Circuit<T>, _: &String) -> Self::Output {
        c.clone()
    }
    fn scalar(&mut self, c: &Circuit<T>, _: &T) -> Self::Output {
        c.clone()
    }
}


impl<T: Num> Circuit<T> {
    pub fn subs_with_memo(
        &self,
        memo: &mut HashMap<Circuit<T>, Circuit<T>>,
    ) -> Circuit<T> {
        SubsVisitor { _pd: Default::default() }.compute(self, memo)
    }
    pub fn subs(&self, map: &HashMap<Circuit<T>, Circuit<T>>) -> Circuit<T> {
        self.subs_with_memo(&mut map.clone())
    }
}



impl<T: Num + 'static> Circuit<T> {
    pub fn jacobian_matrix(sys: &[Self], vars: &[Self], mode: DifferentiationMode) -> DMatrix<Self> {
        assert!(vars.len() > 0);
        match mode {
            DifferentiationMode::Forward => {
                DMatrix::from_fn(sys.len(), vars.len(), |i, j| {
                    sys[i].derivative(&vars[j])
                })
            }
            DifferentiationMode::Backward => {
                let mut grads = Vec::new();
                for eq in sys {
                    grads.push(eq.grad());
                }
                DMatrix::from_fn(sys.len(), vars.len(), |i, j| {
                    grads[i].get(&vars[j]).unwrap_or(&vars[0].context().zero()).clone()
                })

            }
        }
    }
}


#[cfg(test)]
mod homogenize {
    use super::*;

    #[test]
    fn test1() {
        let ctx = Context::<i64>::default();
        let t = ctx.var("t");
        let x = ctx.var("x");
        let y = ctx.var("y");
        let z = ctx.var("z");
        let f = (-&x - ctx.scalar(1) + &t + y.clone() * &y).pow(10);

        let mut pt = HashMap::default();
        pt.insert(t.clone(), 2);
        pt.insert(x.clone(), 2);
        pt.insert(y.clone(), 3);
        pt.insert(z.clone(), -1);

        let (deg, fh) = f.homogenize(&[x, y], &z);

        assert!(deg == 20);
        assert!(fh.eval(&pt) == Some(61917364224));
    }
}
