// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{arch::x86_64::*, fmt::Debug, hash::Hash, ops::*};

pub use crate::reckless_interval::{ComplexInterval, Interval};
use crate::{prog::Evaluator, reckless_interval::complex, taylormodel::TaylorModel};

pub trait Num:
    Debug
    + Hash
    + Eq
    + Clone
    + Copy
    + AddAssign
    + SubAssign
    + MulAssign
    + Add<Output = Self>
    + Sub<Output = Self>
    + Mul<Output = Self>
    + Neg<Output = Self>
    + num_traits::identities::Zero
    + num_traits::identities::One
{
}

impl Num for i64 {}

impl Num for Interval {}

impl Num for ComplexInterval {}

pub type CIF = ComplexInterval;

pub struct ComplexIntervalEvaluator {}

impl Evaluator<ComplexInterval, ComplexInterval> for ComplexIntervalEvaluator {
    fn add_assign(x: &mut ComplexInterval, y: &ComplexInterval) {
        *x = complex::arith::_add(*x, *y);
    }
    fn mul_assign(x: &mut ComplexInterval, y: &ComplexInterval) {
        *x = complex::arith::_mul(*x, *y);
    }

    fn scalar_add_assign(x: &mut ComplexInterval, y: &ComplexInterval) {
        *x = complex::arith::_add(*x, *y);
    }

    fn scalar_mul_assign(x: &mut ComplexInterval, y: &ComplexInterval) {
        *x = complex::arith::_mul(*x, *y);
    }

    fn maybe_set_csr() -> Option<u32> {
        #[allow(deprecated)]
        unsafe {
            let csr = _mm_getcsr();
            _mm_setcsr(_MM_ROUND_UP | _MM_MASK_MASK);
            return Some(csr);
        }
    }
    fn maybe_restore_csr(optcsr: Option<u32>) {
        #[allow(deprecated)]
        unsafe {
            if let Some(csr) = optcsr {
                _mm_setcsr(csr);
            }
        }
    }
}

pub struct TaylorModelEvaluator {}

impl<const N: usize> Evaluator<TaylorModel<N>, ComplexInterval> for TaylorModelEvaluator {
    fn add_assign(x: &mut TaylorModel<N>, y: &TaylorModel<N>) {
        x._add_assign(y);
    }
    fn mul_assign(x: &mut TaylorModel<N>, y: &TaylorModel<N>) {
        x._mul_assign(y);
    }

    fn scalar_add_assign(x: &mut TaylorModel<N>, y: &ComplexInterval) {
        unsafe {
            let c = x.coeffs.get_unchecked_mut(0);
            *c = complex::arith::_add(*c, *y);
        }
    }

    fn scalar_mul_assign(x: &mut TaylorModel<N>, y: &ComplexInterval) {
        unsafe {
            for i in 0..N {
                let c = x.coeffs.get_unchecked_mut(i);
                *c = complex::arith::_mul(*c, *y);
            }
        }
    }

    fn maybe_set_csr() -> Option<u32> {
        #[allow(deprecated)]
        unsafe {
            let csr = _mm_getcsr();
            _mm_setcsr(_MM_ROUND_UP | _MM_MASK_MASK);
            return Some(csr);
        }
    }
    fn maybe_restore_csr(optcsr: Option<u32>) {
        #[allow(deprecated)]
        unsafe {
            if let Some(csr) = optcsr {
                _mm_setcsr(csr);
            }
        }
    }
}

pub struct TaylorModel5Evaluator {}

impl Evaluator<TaylorModel<5>, ComplexInterval> for TaylorModel5Evaluator {
    fn add_assign(x: &mut TaylorModel<5>, y: &TaylorModel<5>) {
        x._add_assign_5(y);
    }
    fn mul_assign(x: &mut TaylorModel<5>, y: &TaylorModel<5>) {
        //let mut cpy = x.clone();
        x._mul_assign_5(y);
        //cpy._mul_assign(y);
        //assert_eq!(*x, cpy);
    }

    fn scalar_add_assign(x: &mut TaylorModel<5>, y: &ComplexInterval) {
        x.coeffs[0] = complex::arith::_add(x.coeffs[0], *y);
    }

    fn scalar_mul_assign(x: &mut TaylorModel<5>, y: &ComplexInterval) {
        use complex::arith::_mul as m;
        x.coeffs[0] = m(x.coeffs[0], *y);
        x.coeffs[1] = m(x.coeffs[1], *y);
        x.coeffs[2] = m(x.coeffs[2], *y);
        x.coeffs[3] = m(x.coeffs[3], *y);
        x.coeffs[4] = m(x.coeffs[4], *y);
    }

    fn maybe_set_csr() -> Option<u32> {
        #[allow(deprecated)]
        unsafe {
            let csr = _mm_getcsr();
            _mm_setcsr(_MM_ROUND_UP | _MM_MASK_MASK);
            return Some(csr);
        }
    }
    fn maybe_restore_csr(optcsr: Option<u32>) {
        #[allow(deprecated)]
        unsafe {
            if let Some(csr) = optcsr {
                _mm_setcsr(csr);
            }
        }
    }
}
