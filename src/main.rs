// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    ops::Deref,
    sync::{Arc, Mutex},
    thread,
};

use algpath::track::ProjectivePoint;
use clap::Parser;

use serde_json::json;

fn main() {
    let conf = algpath::cli::Configuration::parse();
    if conf.markdown_help {
        clap_markdown::print_help_markdown::<algpath::cli::Configuration>();
        return;
    }

    use std::time::Instant;
    let before_compilation = Instant::now();

    let mut input_problem = algpath::cli::Problem::new_from_input(&conf).unwrap();
    let parametric_system = input_problem.build_parametric_system(&conf).unwrap();

    dbg!(parametric_system.f[0].instructions.len());
    dbg!(parametric_system.df[0].instructions.len());

    let card_fiber = input_problem.fiber.len();
    // let workq = Arc::new(Mutex::new(input_problem.fiber));
    let indices: Vec<_> = (0..card_fiber).collect();
    let input_problem = Arc::new(input_problem);
    let workq = Arc::new(Mutex::new(indices));

    let num_jobs: usize;
    if let Some(n) = conf.jobs {
        num_jobs = n;
    } else if let Ok(n) = thread::available_parallelism() {
        num_jobs = n.into();
    } else {
        num_jobs = 1;
    }

    let after_compilation = Instant::now();

    let mut handles = Vec::new();
    let allresults = Arc::new(Mutex::new(Vec::new()));
    for _ in 0..num_jobs {
        let workq = workq.clone();
        let allresults = allresults.clone();
        let mut parametric_system = parametric_system.clone();
        let input_problem = input_problem.clone();
        let path = input_problem.path.clone();
        let handle = thread::spawn(move || {
            let mut results = Vec::new();
            loop {
                let point_index;
                let mut workq = workq.lock().unwrap();
                match workq.pop() {
                    Some(p) => point_index = p,
                    None => break,
                }
                let point = &input_problem.fiber[point_index];
                eprintln!("continuation {}/{}", card_fiber - workq.len(), card_fiber);
                std::mem::drop(workq);

                let proj_point = ProjectivePoint {
                    coord: point.clone().into(),
                    chart: 0,
                };
                let mbox = parametric_system
                    .find_moore_box(&path[0].clone().into(), proj_point, &mut Vec::new())
                    .unwrap();

                let tropt = parametric_system.track_path(mbox, &path.clone().into());
                match tropt {
                    Ok(tr) => {
                        eprintln!("{}", tr.0.len());
                        results.push((point_index, Some(tr.0.len())));
                    }
                    Err(e) => {
                        eprintln!("{}", e);
                        results.push((point_index, None));
                    }
                }
            }
            let mut allresults = allresults.lock().unwrap();
            allresults.append(&mut results);
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    let elapsed = after_compilation.elapsed();
    let elapsed_with_overhead = before_compilation.elapsed();

    let mut allresults = allresults.lock().unwrap();
    allresults.reverse();

    allresults.sort_by_key(|t| t.0);

    // Compute some statistics
    let mut iters: Vec<f64> = Vec::new();
    let mut failures = 0usize;
    for (_, r) in allresults.deref() {
        match r {
            Some(l) => iters.push(*l as f64),
            None => {
                iters.push(f64::NAN);
                failures += 1;
            }
        }
    }

    let output = json!({
        "instructions": {
            "f": parametric_system.f[0].instructions.len(),
            "df": parametric_system.df[0].instructions.len(),
        },
        "steplist": iters,
        "failures": failures,
        "time": elapsed.as_secs_f32(),
        "overheadtime": elapsed_with_overhead.as_secs_f32(),
    });

    println!("{}", output.to_string());

    // for i in 0..allresults.len() {
    //     let (idx, r) = &allresults[i];
    //     assert!(*idx == i);

    //     if r.len() == 0 {
    //         println!("path {} failed", i);
    //         continue;
    //     }

    //     let l = &r.last().unwrap().1;
    //     let f = &r.first().unwrap().1;
    //     print!("start root {}: ", i);
    //     for c in &f.to_chart(0).coord {
    //         print!("{} + {}*im, ", c.re().mid(), c.im().mid());
    //     }
    //     println!("");
    //     print!("root {}: ", i);
    //     for i in 0..input_problem.system.len() + 1 {
    //         let c = l.projective_coordinate(i);
    //         print!("{} + {}*im, ", c.re().mid(), c.im().mid());
    //     }

    //     println!("\n");
    // }
}
