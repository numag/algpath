// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{fmt::Display, ops::SubAssign};

use nalgebra::{DMatrix, DVector};
use num_traits::{One, Zero};

use crate::{
    num::{ComplexIntervalEvaluator, Interval, Num, TaylorModel5Evaluator, CIF},
    prog::{Evaluator, Program},
    reckless_interval::with_rounding_up,
    taylormodel::TaylorModel,
};

#[derive(Clone, Debug)]
pub enum Error {
    MatrixInversion,
    InaccurateNewtonIteration,
    TooManyIterations(usize),
    TooFarFromZero,
    StepSizeTooSmall(f64),
    NonConvergentNewtonIteration,
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::MatrixInversion => write!(f, "could not inverse a matrix in floating point arithmetic"),
            Self::InaccurateNewtonIteration => write!(f, "the roundoff error in the Newton iteration exceeds 5%"),
            Self::TooManyIterations(c) => write!(f, "the Moore box construction procedure has still not finished after {} iterations, usually means that roundoff errors dominate", c),
            Self::TooFarFromZero => write!(f, "could not find a Moore box around this approximate zero"),
            Self::StepSizeTooSmall(step_size) => write!(f, "step size is {} which looks ridiculously small", step_size),
            Self::NonConvergentNewtonIteration => write!(f, "The newton iteration does not converge. This should never happen, please report to the authors.")
        }
    }
}

type Result<T> = std::result::Result<T, Error>;

#[derive(Clone)]
pub struct ParametricSystem {
    pub nparams: usize,
    pub dim: usize,
    pub chart_switch_threshold: f64,
    pub f: Vec<Program<CIF>>,
    pub fdot: Vec<Program<CIF>>,
    pub df: Vec<Program<CIF>>,
}

#[derive(Debug, Clone)]
pub struct ProjectivePoint<T> {
    pub coord: DVector<T>,
    pub chart: usize,
}

impl<T: Num> SubAssign<&DVector<T>> for ProjectivePoint<T> {
    fn sub_assign(&mut self, delta: &DVector<T>) {
        let mut offset = 0;
        for i in 0..self.coord.len() {
            if i == self.chart {
                offset = 1;
            } else {
                self.coord[i] -= delta[i - offset];
            }
        }
    }
}

impl<T: Num> ProjectivePoint<T> {
    pub fn projective_coordinate(&self, i: usize) -> T {
        if i == self.chart {
            return T::one();
        } else if i < self.chart {
            return self.coord[i];
        } else {
            return self.coord[i - 1];
        }
    }

    fn projective_coordinate_mut(&mut self, i: usize) -> Option<&mut T> {
        if i == self.chart {
            return None;
        } else if i < self.chart {
            return Some(&mut self.coord[i]);
        } else {
            return Some(&mut self.coord[i - 1]);
        }
    }
}

impl ProjectivePoint<CIF> {
    fn to_canonical_chart(&self) -> Self {
        let mut chart = 0;
        let mut ccoeff = CIF::zero();
        let mut maxmag = 1.0;
        for i in 0..self.coord.len() {
            let c = self.projective_coordinate(i);
            if c.mag() > maxmag {
                chart = i;
                ccoeff = c;
                maxmag = c.mag();
            }
        }

        if maxmag == 1.0 {
            return self.clone();
        }

        let mut pt = ProjectivePoint {
            coord: DVector::zeros(self.coord.len()),
            chart,
        };

        for i in 0..pt.coord.len() + 1 {
            if let Some(c) = pt.projective_coordinate_mut(i) {
                *c = self.projective_coordinate(i) / ccoeff;
            }
        }

        return pt;
    }

    #[allow(dead_code)]
    pub fn to_chart(&self, chart: usize) -> Self {
        let ccoeff = self.projective_coordinate(chart);
        let mut pt = ProjectivePoint {
            coord: DVector::zeros(self.coord.len()),
            chart,
        };

        for i in 0..pt.coord.len() + 1 {
            if let Some(c) = pt.projective_coordinate_mut(i) {
                *c = self.projective_coordinate(i) / ccoeff;
            }
        }

        return pt;
    }
}

const TMORDER: usize = 5;
type TM = TaylorModel<TMORDER>;

impl ParametricSystem {
    fn prepare_input<S: Num>(params: &DVector<S>, pt: &ProjectivePoint<S>, buf: &mut Vec<S>) {
        let p = params.len();
        let n = pt.coord.len();
        if buf.len() < p + n + 1 {
            buf.resize(p + n + 1, S::zero());
        }
        buf[..p].copy_from_slice(params.as_slice());
        buf[p..p + n].copy_from_slice(&pt.coord.as_slice());
    }

    fn df_with_prepared_buffer<S: Num + 'static, E: Evaluator<S, CIF>>(
        &self,
        chart: usize,
        buf: &mut Vec<S>,
        _: E,
    ) -> DMatrix<S>
    where
        CIF: Into<S>,
        S: std::ops::Add<CIF, Output = S>,
        S: std::ops::Mul<CIF, Output = S>,
        S: std::ops::AddAssign<CIF>,
        S: std::ops::MulAssign<CIF>,
    {
        let n = self.dim;
        let fulldf = E::eval(&self.df[chart], buf);
        return DMatrix::from_vec(n, n, fulldf);
    }

    #[allow(dead_code)]
    pub fn df<S: Num + 'static, E: Evaluator<S, CIF>>(
        &self,
        params: &DVector<S>,
        pt: &ProjectivePoint<S>,
        buf: &mut Vec<S>,
        e: E,
    ) -> DMatrix<S>
    where
        CIF: Into<S>,
        S: std::ops::Add<CIF, Output = S>,
        S: std::ops::Mul<CIF, Output = S>,
        S: std::ops::AddAssign<CIF>,
        S: std::ops::MulAssign<CIF>,
    {
        Self::prepare_input(params, pt, buf);
        return self.df_with_prepared_buffer(pt.chart, buf, e);
    }

    pub fn f<S: Num + 'static, E: Evaluator<S, CIF>>(
        &self,
        params: &DVector<S>,
        pt: &ProjectivePoint<S>,
        buf: &mut Vec<S>,
        _: E,
    ) -> DVector<S>
    where
        CIF: Into<S>,
        S: std::ops::Add<CIF, Output = S>,
        S: std::ops::Mul<CIF, Output = S>,
        S: std::ops::AddAssign<CIF>,
        S: std::ops::MulAssign<CIF>,
    {
        Self::prepare_input(params, pt, buf);
        return E::eval(&self.f[pt.chart], buf).into();
    }

    pub fn distortion<S: Num + 'static, E: Evaluator<S, CIF>>(
        &self,
        params: &DVector<S>,
        pt: &ProjectivePoint<S>,
        mat: &nalgebra::DMatrix<CIF>,
        rad: f64,
        buf: &mut Vec<S>,
        e: E,
    ) -> DVector<S>
    where
        CIF: Into<S>,
        S: std::ops::Add<CIF, Output = S>,
        S: std::ops::Mul<CIF, Output = S>,
        S: std::ops::AddAssign<CIF>,
        S: std::ops::MulAssign<CIF>,
    {
        let n = self.dim;

        Self::prepare_input(params, pt, buf);
        for i in 0..pt.coord.len() {
            buf[params.len() + i] += CIF::square(rad);
        }
        let df = self.df_with_prepared_buffer(pt.chart, buf, e);
        let mut distort = DVector::zeros(n);

        // compute the matrix product by hand, because the coefficients do not have the same types
        with_rounding_up(
            #[inline(always)]
            || {
                for i in 0..n {
                    for k in 0..n {
                        let mut m = S::zero();
                        for j in 0..n {
                            // we should write the multiplication the other way around,
                            // but we have the convention to have scalar multiplication
                            // on the right.
                            // m += df[(j, k)] * mat[(i, j)];
                            let mut p = df[(j, k)];
                            E::scalar_mul_assign(&mut p, &mat[(i, j)]);
                            E::add_assign(&mut m, &p);
                        }
                        if i == k {
                            E::scalar_add_assign(&mut m, &(-CIF::one()));
                        }
                        // distort[i] += m * CIF::square(1.0);
                        E::scalar_mul_assign(&mut m, &CIF::square(1.0));
                        unsafe {
                            E::add_assign(distort.get_unchecked_mut(i), &m);
                        }
                    }
                }
            },
        );

        return distort;
    }

    pub fn inv_df(
        &mut self,
        params: &DVector<CIF>,
        pt: &ProjectivePoint<CIF>,
        buf: &mut Vec<CIF>,
    ) -> Result<DMatrix<CIF>> {
        Self::prepare_input(params, pt, buf);
        let df = self.df_with_prepared_buffer(pt.chart, buf, ComplexIntervalEvaluator {});

        let mid_df = df.map(|c| nalgebra::Complex {
            re: c.re().mid(),
            im: c.im().mid(),
        });
        let inv = mid_df.try_inverse().ok_or(Error::MatrixInversion)?;

        let mut invdfvec = Vec::new();
        invdfvec.reserve_exact(inv.len());
        for c in &inv {
            invdfvec.push(CIF::new(
                Interval::new(c.re, c.re),
                Interval::new(c.im, c.im),
            ))
        }

        return Ok(DMatrix::from_vec(inv.nrows(), inv.ncols(), invdfvec));
    }

    #[inline(never)]
    pub fn find_moore_box(
        &mut self,
        params: &DVector<CIF>,
        pt: ProjectivePoint<CIF>,
        buf: &mut Vec<CIF>,
    ) -> Result<MooreBox> {
        let mut rad = 1.0 as f64;
        let inv_df = self.inv_df(params, &pt, buf)?;
        for _ in 1..100 {
            let distort =
                self.distortion(params, &pt, &inv_df, rad, buf, ComplexIntervalEvaluator {});
            if CIF::matrix_mag(&distort) <= 0.5 {
                let f = self.f(params, &pt, buf, ComplexIntervalEvaluator {});
                let h: CIF = Interval::new(rad, rad).recip().into();
                let krawczyk = &inv_df * &f * h + distort;

                if CIF::matrix_mag(&krawczyk) <= 0.875 {
                    return Ok(MooreBox {
                        params: params.clone(),
                        pt,
                        mat: inv_df,
                        rad,
                    });
                } else {
                    return Err(Error::TooFarFromZero);
                }
            }

            rad *= 0.5;
        }

        return Err(Error::TooManyIterations(100));
    }

    #[inline(never)]
    pub fn refine(&mut self, mut mbox: MooreBox, tau: f64, buf: &mut Vec<CIF>) -> Result<MooreBox> {
        let mut rad = mbox.rad;

        // This is a a useful check, but there may be legitimate situations where it fails.
        // {
        //     let f = self.f(&mbox.params, &mbox.pt, buf, ComplexIntervalEvaluator {});
        //     let h: CIF = Interval::new(rad, rad).recip().into();
        //     let krawczyk = &mbox.mat * &f * h
        //         + self.distortion(
        //             &mbox.params,
        //             &mbox.pt,
        //             &mbox.mat,
        //             rad,
        //             buf,
        //             ComplexIntervalEvaluator {},
        //         );
        //     dbg!(CIF::matrix_mag(&krawczyk));
        //     assert!(CIF::matrix_mag(&krawczyk) < 1.0);
        // }

        // print!("r");

        rad *= 1.1;
        for _ in 0..2 {
            let f = self.f(&mbox.params, &mbox.pt, buf, ComplexIntervalEvaluator {});
            let delta = with_rounding_up(|| &mbox.mat * &f);
            let mag_delta = CIF::matrix_mag(&delta);
            let mut coord = &mbox.pt.coord - &delta;
            if 20.0 * CIF::matrix_wid(&coord) > mag_delta {
                break;
            } else {
                // print!("*");
                coord.apply(|z| *z = z.squash());
                mbox.pt.coord = coord;
            }
        }

        let mut inv_df = self.inv_df(&mbox.params, &mbox.pt, buf)?;
        let mut cnt = 0usize;
        loop {
            // print!(".");
            cnt += 1;
            let f = self.f(&mbox.params, &mbox.pt, buf, ComplexIntervalEvaluator {});
            let h: CIF = Interval::new(rad, rad).recip().into();
            let mut krawczyk = with_rounding_up(|| &inv_df * &f * h);

            // quick test first
            if 7.0 * CIF::matrix_mag(&krawczyk) <= tau {
                // try full test
                // print!("?");
                let distort = self.distortion(
                    &mbox.params,
                    &mbox.pt,
                    &inv_df,
                    rad,
                    buf,
                    ComplexIntervalEvaluator {},
                );
                krawczyk += distort;
                // dbg!(CIF::matrix_mag(&krawczyk));
                if CIF::matrix_mag(&krawczyk) <= tau {
                    break;
                }
            }

            // the test has fail, we must improve something
            let delta = with_rounding_up(|| &mbox.mat * &f);
            let mag_delta = CIF::matrix_mag(&delta);

            if 64.0 * mag_delta <= tau * rad {
                // print!("/");
                rad *= 0.5;

                if rad < 1e-50 || 32.0 * rad <= tau * mbox.rad {
                    return Err(Error::TooManyIterations(cnt));
                }
            } else {
                // print!("*");
                mbox.pt.coord -= &delta;
                if 20.0 * CIF::matrix_wid(&mbox.pt.coord) > mag_delta {
                    // dbg!(rad, mag_delta);
                    return Err(Error::InaccurateNewtonIteration);
                } else {
                    mbox.pt.coord.apply(|z| *z = z.squash());
                    inv_df = self.inv_df(&mbox.params, &mbox.pt, buf)?;
                }
            }
        }

        // println!("!");
        return Ok(MooreBox {
            params: mbox.params,
            pt: mbox.pt,
            mat: inv_df,
            rad,
        });
    }

    // This function is one line in Julia, can we do better in rust? Main issue
    // is to have multiplication with heterogeneous types.
    #[inline(never)]
    fn predictor(
        p0: &DVector<CIF>,
        v0: &DVector<CIF>,
        p1opt: &Option<DVector<CIF>>,
        v1opt: &Option<DVector<CIF>>,
        previous_step_size: f64,
        domain: Interval,
    ) -> DVector<TM> {
        // https://en.wikipedia.org/wiki/Cubic_Hermite_spline
        let h: CIF = Interval::new(previous_step_size.recip(), previous_step_size.recip()).into();
        let mut pred = DVector::<TM>::zeros(p0.len());

        let coeffs = if let (Some(p1), Some(v1)) = (p1opt, v1opt) {
            let dp = (p0 - p1) * h;
            [
                p0.clone(),
                v0.clone(),
                ((v0 * CIF::from(2) + v1) - &dp * CIF::from(3)) * h,
                ((v0 + v1) - dp.clone() * CIF::from(2)) * (h * h),
            ]
        } else {
            [
                p0.clone(),
                v0.clone(),
                DVector::zeros(p0.len()),
                DVector::zeros(p0.len()),
            ]
        };

        for i in 0..p0.len() {
            pred[i].domain = domain;
            for j in 0..4 {
                pred[i].coeffs[j] += coeffs[j][i].squash();
            }
        }

        return pred;
    }

    #[inline(never)]
    fn try_certify(
        &mut self,
        param: &DVector<TM>,
        point: &ProjectivePoint<TM>,
        mat: &DMatrix<CIF>,
        rad: f64,
        buf: &mut Vec<TM>,
    ) -> Option<Interval> {
        let mut domain = Interval::ENTIRE;

        // print!("#(rad: {})", rad);
        let f = self.f(param, &point, buf, TaylorModel5Evaluator {});
        let mut krawczyk: DVector<TM> = DVector::zeros(self.dim);
        let distort = self.distortion(param, point, mat, rad, buf, TaylorModel5Evaluator {});
        let h: CIF = Interval::new(rad, rad).recip().into();

        with_rounding_up(|| {
            for i in 0..self.dim {
                for j in 0..self.dim {
                    krawczyk[i] += f[j] * (mat[(i, j)] * h);
                }
                domain = domain.intersection(krawczyk[i].domain);
            }
            krawczyk += distort;

            //assert!(CIF::matrix_mag(&krawczyk.map(|c| c.enclosure(Interval::ZERO))) <= 0.25);
        });

        // Now tries to find to find a small subdomain of "domain"
        // on which we can check Moore's criterion.
        let step_size = domain.sup();

        // println!("(step_size: {})", step_size);
        let to_be_tested = [
            step_size,
            0.875 * step_size,
            0.75 * step_size,
            0.625 * step_size,
        ];

        // This is the smallest acceptable subdomain.
        for sup in to_be_tested {
            // print!(",");
            let testdom = Interval::new(0.0, sup);
            if CIF::matrix_mag(&krawczyk.map(|c| c.enclosure(testdom))) <= 0.875 {
                // println!("!");
                return Some(testdom);
            }
        }

        // println!("X");
        return None;
    }

    #[inline(never)]
    pub fn track(
        &mut self,
        mut mbox: MooreBox,
        target_param: &DVector<CIF>,
    ) -> Result<(Vec<(f64, ProjectivePoint<TM>)>, MooreBox)> {
        let mut t: f64 = 0.0;

        let bufcif = &mut Vec::new();
        let buftm = &mut Vec::new();

        let begin = mbox.params.clone();
        let direction = target_param - &begin;

        let mut step_size = 1.0;

        let mut ret = Vec::new();

        // This information is useful for the Hermite predictor
        let mut previous_point = None;
        let mut previous_speed = None;

        while t < 1.0 {
            // dbg!(t);
            // dbg!(CIF::matrix_mag(&mbox.pt.coord));

            mbox = self.refine(mbox, 0.25, bufcif)?;
            if CIF::matrix_mag(&mbox.pt.coord) > self.chart_switch_threshold {
                // print!("C");
                let pt = mbox.pt.to_canonical_chart();
                mbox = self.find_moore_box(&mbox.params, pt, bufcif)?;
                mbox = self.refine(mbox, 0.25, bufcif)?;
                previous_point = None;
                previous_speed = None;
            }

            let interval_t = Interval::new(t, t);
            let param = with_rounding_up(|| &begin + &direction * CIF::from(interval_t));

            Self::prepare_input(&param, &mbox.pt, bufcif);
            let fdot: DMatrix<CIF> =
                DMatrix::from_vec(self.dim, self.nparams, ComplexIntervalEvaluator::eval(&self.fdot[mbox.pt.chart], bufcif).into());
            let mut speed: DVector<CIF> = with_rounding_up(|| -&mbox.mat * (fdot * &direction));
            speed.apply(|c| *c = c.squash());

            // Initialization of the Taylor models
            let mut next_step_size = step_size * 1.25;
            let domain = Interval::new(0.0, next_step_size);
            let dt = TM::identity(domain);
            let mut point_tm = ProjectivePoint {
                coord: Self::predictor(
                    &mbox.pt.coord,
                    &speed,
                    &previous_point,
                    &previous_speed,
                    step_size,
                    domain,
                ),
                chart: mbox.pt.chart,
            };
            let mut param_tm = &param.map(|c| TM::from(c)) + &direction.map(|c| dt.clone() * c);

            loop {
                let cert = self.try_certify(&param_tm, &point_tm, &mbox.mat, mbox.rad, buftm);
                if let Some(domain) = cert {
                    point_tm.coord.apply(|c| c.domain = domain);
                    step_size = domain.sup();
                    break;
                }

                next_step_size *= 0.3;
                if next_step_size <= 1e-20 {
                    return Err(Error::StepSizeTooSmall(next_step_size));
                }

                let domain = Interval::new(0.0, next_step_size);
                param_tm.apply(|c| c.domain = domain);
                point_tm.coord.apply(|c| c.domain = domain);
            }

            previous_point = Some(mbox.pt.coord);
            previous_speed = Some(speed);

            let mut i_dt = Interval::new(step_size, step_size);
            let next_t = (interval_t + i_dt).inf();
            
            if next_t == t {
                return Err(Error::StepSizeTooSmall(next_step_size));
            } else if next_t >= 1.0 {
                i_dt = Interval::one() - Interval::new(t, t);
            }

            t = next_t;
            mbox.pt.coord = point_tm.coord.map(|c| c.enclosure(i_dt).squash());
            mbox.params = param_tm.map(|c| c.enclosure(i_dt).squash());
            ret.push((t, point_tm));
        }

        return Ok((ret, mbox));
    }

    #[inline(never)]
    pub fn track_path(
        &mut self,
        mut mbox: MooreBox,
        path: &Vec<Vec<CIF>>,
    ) -> Result<(Vec<(f64, ProjectivePoint<TM>)>, MooreBox)> {
        let mut res = vec![];

        for i in 1..path.len() {
            let mut tropt = self.track(mbox, &path[i].clone().into())?;
            res.append(&mut tropt.0);
            mbox = tropt.1;
        }
        return Ok((res, mbox));
    }
}

#[derive(Debug)]
pub struct MooreBox {
    params: DVector<CIF>,
    pt: ProjectivePoint<CIF>,
    mat: nalgebra::DMatrix<CIF>,
    pub rad: f64,
}
