// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use lalrpop_util::lalrpop_mod;

lalrpop_mod!(pub expr, "/parser/expr.rs");
lalrpop_mod!(pub complex, "/parser/complex.rs");

lazy_static::lazy_static! {
    pub static ref COMPLEX: complex::ComplexParser = complex::ComplexParser::new();
    pub static ref EXPR: expr::ExprParser = expr::ExprParser::new();
}
