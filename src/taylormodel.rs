// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::num::{Interval, Num, CIF};
use crate::reckless_interval::complex::{self, arith::*};
use crate::reckless_interval::with_rounding_up;
use num_traits::identities::*;
use std::{
    fmt::Display,
    ops::*,
};

#[derive(Debug, Clone, Hash, PartialEq, Eq, Copy)]
pub struct TaylorModel<const N: usize> {
    pub coeffs: [CIF; N],
    pub domain: Interval,
}

impl<const N: usize> Display for TaylorModel<N> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.coeffs[0])?;
        for i in 1..N {
            write!(f, " + ({})*t^{}", self.coeffs[i], i)?;
        }
        Ok(())
    }
}

impl<const N: usize> TaylorModel<N> {
    pub fn identity(domain: Interval) -> Self {
        Self::affine(CIF::zero(), CIF::one(), domain)
    }

    pub fn affine(a: CIF, b: CIF, domain: Interval) -> Self {
        let mut coeffs = [CIF::zero(); N];
        coeffs[0] = a;
        coeffs[1] = b;
        TaylorModel { coeffs, domain }
    }

    fn dominter(&mut self, dom: Interval) {
        if self.domain != dom {
            self.domain = self.domain.intersection(dom);
        }
    }

    pub fn enclosure(&self, dom: Interval) -> CIF {
        assert!(dom.subset(self.domain));
        return with_rounding_up(|| {
            let mut z = CIF::zero();
            for c in self.coeffs.iter().rev() {
                z = complex::arith::_mul_interval(z, dom);
                z = complex::arith::_add(z, *c);
            }
            z
        });
    }
}

impl<const N: usize> From<CIF> for TaylorModel<N> {
    fn from(a: CIF) -> Self {
        let mut coeffs = [CIF::zero(); N];
        coeffs[0] = a;
        TaylorModel {
            coeffs,
            domain: Interval::ENTIRE,
        }
    }
}

impl<const N: usize> AddAssign<Self> for TaylorModel<N> {
    fn add_assign(&mut self, other: Self) {
        self.dominter(other.domain);
        with_rounding_up(|| self._add_assign(&other));
    }
}

impl<const N: usize> SubAssign<Self> for TaylorModel<N> {
    fn sub_assign(&mut self, other: Self) {
        self.dominter(other.domain);
        for i in 0..N {
            self.coeffs[i] -= other.coeffs[i];
        }
    }
}

impl<const N: usize> TaylorModel<N> {
    #[allow(dead_code)]
    fn reference_mul_assign(&mut self, other: Self) {
        self.dominter(other.domain);
        let mut err = CIF::zero();
        for k in (N..2 * N - 1).rev() {
            for i in k - N + 1..N {
                err += self.coeffs[i] * other.coeffs[k - i];
            }
            err *= self.domain;
        }

        for k in (0..N).rev() {
            self.coeffs[k] *= other.coeffs[0];
            for i in 1..=k {
                self.coeffs[k] += self.coeffs[k - i] * other.coeffs[i];
            }
        }

        self.coeffs[N - 1] += err;
    }

    pub fn _mul_assign(&mut self, other: &Self) {
        self.dominter(other.domain);
        unsafe {
            let mut err = CIF::zero();
            let mut k: usize = 2 * N - 2;
            while k >= N {
                for i in k - N + 1..N {
                    err = _add(
                        err,
                        _mul(
                            *self.coeffs.get_unchecked(i),
                            *other.coeffs.get_unchecked(k - i),
                        ),
                    );
                }
                err = _mul_interval(err, self.domain);
                k -= 1;
            }

            loop {
                let c = self.coeffs.get_unchecked_mut(k);
                *c = _mul(*c, *other.coeffs.get_unchecked(0));
                for i in 1..k + 1 {
                    let p = _mul(
                        *self.coeffs.get_unchecked(k - i),
                        *other.coeffs.get_unchecked(i),
                    );
                    let c = self.coeffs.get_unchecked_mut(k);
                    *c = _add(*c, p);
                }
                if k == 0 {
                    break;
                }
                k -= 1;
            }

            let c = self.coeffs.get_unchecked_mut(N - 1);
            *c = _add(*c, err);
        }
    }

    pub fn _add_assign(&mut self, other: &Self) {
        self.dominter(other.domain);
        unsafe {
            for i in 0..N {
                let c = self.coeffs.get_unchecked_mut(i);
                *c = _add(*c, *other.coeffs.get_unchecked(i));
            }
        }
    }
}

impl TaylorModel<5> {
    // Invariant: if one of the interval coefficient is unbounded, the last coefficient is [-∞,∞]

    pub fn _add_assign_5(&mut self, other: &Self) {
        self.coeffs[0] = _add(self.coeffs[0], other.coeffs[0]);
        self.coeffs[1] = _add(self.coeffs[1], other.coeffs[1]);
        self.coeffs[2] = _add(self.coeffs[2], other.coeffs[2]);
        self.coeffs[3] = _add(self.coeffs[3], other.coeffs[3]);
        self.coeffs[4] = _add(self.coeffs[4], other.coeffs[4]);

        for i in 0..5 {
            if !self.coeffs[i].is_common() {
                self.coeffs[4] = CIF::new(Interval::ENTIRE, Interval::ENTIRE);
            }
        }
    }

    //#[inline(never)]
    pub fn _mul_assign_5(&mut self, other: &Self) {
        let [a0, a1, a2, a3, a4] = self.coeffs;
        let [b0, b1, b2, b3, b4] = other.coeffs;
        let dst = &mut self.coeffs;

        use crate::reckless_interval::complex::arith::{_add as a, _mul_bounded as m};

        dst[0] = m(a0, b0);
        dst[1] = m(a0, b1);
        dst[2] = m(a0, b2);
        dst[3] = m(a0, b3);
        dst[4] = m(a0, b4);

        dst[1] = a(dst[1], m(a1, b0));
        dst[2] = a(dst[2], m(a1, b1));
        dst[3] = a(dst[3], m(a1, b2));
        dst[4] = a(dst[4], m(a1, b3));

        dst[2] = a(dst[2], m(a2, b0));
        dst[3] = a(dst[3], m(a2, b1));
        dst[4] = a(dst[4], m(a2, b2));

        dst[3] = a(dst[3], m(a3, b0));
        dst[4] = a(dst[4], m(a3, b1));

        dst[4] = a(dst[4], m(a4, b0));

        let mut err = m(a4, b4);
        err = _mul_interval(err, self.domain);

        err = a(err, m(a3, b4));
        err = a(err, m(a4, b3));
        err = _mul_interval(err, self.domain);

        err = a(err, m(a2, b4));
        err = a(err, m(a3, b3));
        err = a(err, m(a4, b2));
        err = _mul_interval(err, self.domain);

        err = a(err, m(a1, b4));
        err = a(err, m(a2, b3));
        err = a(err, m(a3, b2));
        err = a(err, m(a4, b1));
        err = _mul_interval(err, self.domain);

        dst[4] = a(dst[4], err);

        if !(a4.is_common()
            & b4.is_common()
            & dst[0].is_common()
            & dst[1].is_common()
            & dst[2].is_common()
            & dst[3].is_common()
            & dst[4].is_common())
        {
            dst[4] = CIF::new(Interval::ENTIRE, Interval::ENTIRE);
        }

    }
}

impl<const N: usize> MulAssign<Self> for TaylorModel<N> {
    #[inline(never)]
    fn mul_assign(&mut self, other: Self) {
        self.dominter(other.domain);
        with_rounding_up(|| self._mul_assign(&other));
    }
}

impl<const N: usize> Add for TaylorModel<N> {
    type Output = Self;
    fn add(mut self, other: Self) -> Self {
        self.dominter(other.domain);
        self += other;
        return self;
    }
}

impl<const N: usize> Sub for TaylorModel<N> {
    type Output = Self;
    fn sub(mut self, other: Self) -> Self {
        self.dominter(other.domain);
        self -= other;
        return self;
    }
}

impl<const N: usize> Mul for TaylorModel<N> {
    type Output = Self;
    fn mul(mut self, other: Self) -> Self {
        self.dominter(other.domain);
        self *= other;
        return self;
    }
}

impl<const N: usize> Neg for TaylorModel<N> {
    type Output = Self;
    fn neg(mut self) -> Self {
        for i in 0..N {
            self.coeffs[i] = -self.coeffs[i];
        }
        return self;
    }
}

impl<const N: usize> num_traits::Zero for TaylorModel<N> {
    fn zero() -> Self {
        TaylorModel {
            coeffs: [CIF::zero(); N],
            domain: Interval::ENTIRE,
        }
    }

    fn is_zero(&self) -> bool {
        for c in &self.coeffs {
            if !c.is_zero() {
                return false;
            }
        }
        return true;
    }
    fn set_zero(&mut self) {
        *self = Self::zero();
    }
}

impl<const N: usize> num_traits::One for TaylorModel<N> {
    fn one() -> Self {
        let mut z = Self::zero();
        z.coeffs[0] = CIF::one();
        return z;
    }
    fn is_one(&self) -> bool {
        if !self.coeffs[0].is_one() {
            return false;
        }
        for i in 1..N {
            if !self.coeffs[i].is_zero() {
                return false;
            }
        }
        return true;
    }
    fn set_one(&mut self) {
        *self = Self::one();
    }
}

impl<const N: usize> AddAssign<CIF> for TaylorModel<N> {
    fn add_assign(&mut self, other: CIF) {
        self.coeffs[0] += other;
    }
}

impl<const N: usize> SubAssign<CIF> for TaylorModel<N> {
    fn sub_assign(&mut self, other: CIF) {
        self.coeffs[0] -= other;
    }
}

impl<const N: usize> MulAssign<CIF> for TaylorModel<N> {
    fn mul_assign(&mut self, other: CIF) {
        for c in &mut self.coeffs {
            *c *= other;
        }
    }
}

impl<const N: usize> Add<CIF> for TaylorModel<N> {
    type Output = Self;
    fn add(mut self, other: CIF) -> Self {
        self += other;
        return self;
    }
}

impl<const N: usize> Sub<CIF> for TaylorModel<N> {
    type Output = Self;
    fn sub(mut self, other: CIF) -> Self {
        self -= other;
        return self;
    }
}

impl<const N: usize> Mul<CIF> for TaylorModel<N> {
    type Output = Self;
    fn mul(mut self, other: CIF) -> Self {
        self *= other;
        return self;
    }
}

impl<const N: usize> Num for TaylorModel<N> {}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn mul() {
        let dom = Interval::new(-2.0, 3.0);
        let t = TaylorModel::<5>::identity(dom);
        let t2 = t * t;
        let t3 = t2 * t;
        let t4 = t3 * t;
        assert_eq!(t4.coeffs[3], CIF::ZERO);
        assert_eq!(t4.coeffs[4], CIF::ONE);

        let t5 = t4 * t;
        dbg!(t4);
        dbg!(t5);
        assert_eq!(t5.coeffs[4], dom.into());

        let t8 = t4 * t4;
        let mut t8ref = t4;
        t8ref.reference_mul_assign(t4);
        assert_eq!(t8, t8ref);

        with_rounding_up(|| {
            let mut t8fast = t4;
            t8fast._mul_assign_5(&t4);
            assert_eq!(t8fast, t8ref);
        })
    }
}
