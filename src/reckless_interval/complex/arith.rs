// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use std::arch::x86_64::*;

use crate::reckless_interval::Interval;

use super::ComplexInterval;

pub fn _add(x: ComplexInterval, y: ComplexInterval) -> ComplexInterval {
    ComplexInterval(unsafe { _mm256_add_pd(x.0, y.0) })
}

impl std::ops::Neg for ComplexInterval {
    type Output = ComplexInterval;
    fn neg(self) -> Self::Output {
        ComplexInterval(unsafe { _mm256_permute_pd(self.0, 5) })
    }
}

// see Lambov, B. (2008). Interval Arithmetic Using SSE-2. In P. Hertling, C. M.
// Hoffmann, W. Luther, & N. Revol (Eds.), Reliable Implementation of Real
// Number Algorithms: Theory and Practice (pp. 102–113). Springer.
// https://doi.org/10.1007/978-3-540-85521-7_6
fn ivchoice256(mut a: __m256d, b: __m256d) -> __m256d {
    unsafe {
        // a = [a0, a1, a2, a3]
        // b = [b0, b1, b2, b3]
        //
        // output
        // [c0, c1, c2, c3]
        // where c0 = b0 >= 0 ? a0 : -a1
        // and c1 = b1 >= 0 ? a0 : -a1
        // where c2 = b2 >= 0 ? a2 : -a3
        // and c2 = b3 >= 0 ? a2 : -a3

        // a = [a0, -a1]
        // _mm_set_pd has the low part as last argument
        a = _mm256_xor_pd(a, _mm256_set_pd(-0.0, 0.0, -0.0, 0.0));

        // cmp[1] = sign bit of b0
        // cmp[65] = sign bit of b1
        let cmp = _mm256_srli_epi64(_mm256_castpd_si256(b), 62); // AVX2
        //let cmp = _mm256_castpd_si256(_mm256_cmp_pd(b, _mm256_set_pd(0.0, 0.0, 0.0, 0.0), _CMP_LT_OQ));
        return _mm256_permutevar_pd(a, cmp);
    }
}

pub fn _mul_componentwise(x: ComplexInterval, y: ComplexInterval) -> ComplexInterval {
    let a = x.0;
    let b = y.0;

    unsafe {
        let arev = _mm256_permute_pd(a, 5);
        let brev = _mm256_permute_pd(b, 5);

        let mut u = _mm256_mul_pd(ivchoice256(a, brev), brev);
        let mut v = _mm256_mul_pd(ivchoice256(arev, b), b);

        let cu = _mm256_cmp_pd(u, u, _CMP_EQ_OQ);
        let cv = _mm256_cmp_pd(v, v, _CMP_EQ_OQ);

        // if _mm_movemask_pd(_mm_and_pd(cu, cv)) != 3 {
        // NaNs may come from a multiplication ∞ × 0, which should be 0
        // in the context of interval arithmetic, or from the input.

        // replace NaNs with zeros
        u = _mm256_and_pd(cu, u);
        v = _mm256_and_pd(cv, v);

        let w = _mm256_max_pd(u, v);

        return ComplexInterval(w);
    }
}

pub fn _mul_interval(x: ComplexInterval, y: Interval) -> ComplexInterval {
    let yy = ComplexInterval::new(y, y);
    _mul_componentwise(x, yy)
}

//#[inline(always)]
pub fn _mul(x: ComplexInterval, y: ComplexInterval) -> ComplexInterval {
    let aa = ComplexInterval::new(x.re(), x.re());
    let bb = ComplexInterval::new(x.im(), x.im());
    let mdc = ComplexInterval::new(-y.im(), y.re());

    _add(_mul_componentwise(aa, y), _mul_componentwise(bb, mdc))
}

#[inline(always)]
pub fn _mul_componentwise_bounded(x: ComplexInterval, y: ComplexInterval) -> ComplexInterval {
    let a = x.0;
    let b = y.0;

    unsafe {
        let arev = _mm256_permute_pd(a, 5);
        let brev = _mm256_permute_pd(b, 5);

        let u = _mm256_mul_pd(ivchoice256(a, brev), brev);
        let v = _mm256_mul_pd(ivchoice256(arev, b), b);

        let w = _mm256_max_pd(u, v);
        return ComplexInterval(w);
    }
}

//#[inline(always)]
pub fn _mul_bounded(x: ComplexInterval, y: ComplexInterval) -> ComplexInterval {
    let aa = ComplexInterval::new(x.re(), x.re());
    let bb = ComplexInterval::new(x.im(), x.im());
    let mdc = ComplexInterval::new(-y.im(), y.re());

    _add(_mul_componentwise_bounded(aa, y), _mul_componentwise_bounded(bb, mdc))
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add() {
        assert_eq!(
            _add(
                ComplexInterval::new(Interval::new(2.0, 3.0), Interval::new(4.0, 6.0)),
                ComplexInterval::new(Interval::new(10.0, 20.0), Interval::new(40.0, 60.0))
            ),
            ComplexInterval::new(Interval::new(12.0, 23.0), Interval::new(44.0, 66.0))
        )
    }

    #[test]
    fn mul() {
        assert_eq!(
            _mul(
                ComplexInterval::new(Interval::new(2.0, 3.0), Interval::new(4.0, 6.0)),
                ComplexInterval::new(Interval::new(10.0, 20.0), Interval::new(40.0, 60.0))
            ),
            ComplexInterval::new(Interval::new(-340.0, -100.0), Interval::new(120.0, 300.0))
        );

        assert_eq!(
            _mul_interval(ComplexInterval::ZERO, Interval::ENTIRE),
            ComplexInterval::ZERO
        );
    }
}
