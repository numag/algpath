// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use std::arch::x86_64::*;

use super::f64extra::add_round_up;

#[derive(Copy, Clone, Debug)]
#[repr(C)]
pub struct Interval(pub __m128d);

impl PartialEq for Interval {
    fn eq(&self, other: &Self) -> bool {
        (self.lo().is_nan() & other.lo().is_nan()) | (self.rep() == other.rep())
    }
}

impl Eq for Interval {}

static PRINT_MID_ONLY : bool = true;

impl std::fmt::Display for Interval {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        if PRINT_MID_ONLY {
            write!(f, "{}", self.mid())
        } else {
            write!(f, "[{}, {}]", self.inf(), self.sup())
        }
    }
}

impl std::hash::Hash for Interval {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.inf().to_bits().hash(state);
        self.sup().to_bits().hash(state);
    }
}

impl num_traits::identities::Zero for Interval {
    fn zero() -> Self {
        Self::ZERO
    }
    fn is_zero(&self) -> bool {
        *self == Self::ZERO
    }
    fn set_zero(&mut self) {
        *self = Interval::zero();
    }
}

impl num_traits::identities::One for Interval {
    fn one() -> Self {
        Self::ONE
    }
    fn is_one(&self) -> bool {
        *self == Self::ONE
    }
    fn set_one(&mut self) {
        *self = Interval::one();
    }
}


impl Interval {
    pub const ENTIRE: Interval =
        Interval(unsafe { std::mem::transmute((f64::INFINITY, f64::INFINITY)) });
    pub const EMPTY: Interval = Interval(unsafe { std::mem::transmute((f64::NAN, f64::NAN)) });
    pub const ZERO: Interval = Interval(unsafe { std::mem::transmute((0.0, 0.0)) });
    pub const ONE: Interval = Interval(unsafe { std::mem::transmute((-1.0, 1.0)) });

    pub(crate) fn new_raw(lo: f64, hi: f64) -> Self {
        unsafe { Interval(_mm_set_pd(hi, lo)) }
    }

    pub fn new(inf: f64, sup: f64) -> Self {
        if inf <= sup {
            Self::new_raw(-inf, sup)
        } else {
            Self::EMPTY
        }
    }

    pub(crate) fn rep(self) -> [f64; 2] {
        let mut pair: [f64; 2] = [0.0, 0.0];
        unsafe {
            _mm_store_pd(pair.as_mut_ptr(), self.0);
        }
        pair
    }

    pub(crate) fn lo(self) -> f64 {
        self.rep()[0]
    }

    pub(crate) fn hi(self) -> f64 {
        self.rep()[1]
    }

    /// Returns the lower bound of `self`.
    ///
    /// The lower bound of an interval $𝒙$ is:
    ///
    /// $$
    /// \inf(𝒙) = \begin{cases}
    ///   +∞ & \if 𝒙 = ∅, \\\\
    ///   a  & \if 𝒙 = \[a, b\].
    ///  \end{cases}
    /// $$
    ///
    /// The exact value is returned.  When $a = 0$, `-0.0` is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert_eq!(Interval::new(-2.0, 3.0).inf(), -2.0);
    /// assert_eq!(Interval::EMPTY.inf(), f64::INFINITY);
    /// assert_eq!(Interval::ENTIRE.inf(), f64::NEG_INFINITY);
    /// ```
    ///
    /// See also: [`Interval::sup`].
    pub fn inf(self) -> f64 {
        let x = -self.lo();
        if x.is_nan() {
            // The empty interval.
            f64::INFINITY
        } else if x == 0.0 {
            -0.0
        } else {
            x
        }
    }

    /// Returns the magnitude of `self` if it is nonempty; otherwise, a NaN.
    ///
    /// The magnitude of a nonempty interval $𝒙 = \[a, b\]$ is:
    ///
    /// $$
    /// \begin{align*}
    ///  \mag(𝒙) &= \sup \set{|x| ∣ x ∈ 𝒙} \\\\
    ///   &= \max \set{|a|, |b|}.
    /// \end{align*}
    /// $$
    ///
    /// The exact value is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert_eq!(Interval::new(-2.0, 3.0).mag(), 3.0);
    /// assert!(Interval::EMPTY.mag().is_nan());
    /// assert_eq!(Interval::ENTIRE.mag(), f64::INFINITY);
    /// ```
    ///
    /// See also: [`Interval::mig`].
    pub fn mag(self) -> f64 {
        f64::max(self.lo(), self.hi())
    }

    /// Returns the midpoint of `self` if it is nonempty; otherwise, a NaN.
    ///
    /// The midpoint of a nonempty interval $𝒙 = \[a, b\]$ is:
    ///
    /// $$
    /// \mid(𝒙) = \frac{a + b}{2}.
    /// $$
    ///
    /// As an approximation in [`f64`], it returns:
    ///
    /// - `0.0`, if $\self = \[-∞, +∞\]$;
    /// - [`f64::MIN`], if $\self = \[-∞, b\]$, where $b ∈ \R$;
    /// - [`f64::MAX`], if $\self = \[a, +∞\]$, where $a ∈ \R$;
    /// - otherwise, the closest [`f64`] number to $\mid(\self)$, away from zero in case of ties.
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert_eq!(Interval::new(-2.0, 3.0).mid(), 0.5);
    /// assert_eq!(Interval::new(f64::NEG_INFINITY, 3.0).mid(), f64::MIN);
    /// assert_eq!(Interval::new(-2.0, f64::INFINITY).mid(), f64::MAX);
    /// assert!(Interval::EMPTY.mid().is_nan());
    /// assert_eq!(Interval::ENTIRE.mid(), 0.0);
    /// ```
    ///
    /// See also: [`Interval::rad`].
    // See Table VII in https://doi.org/10.1145/2493882 for the implementation.
    pub fn mid(self) -> f64 {
        let a = self.lo();
        let b = self.hi();

        match (a == f64::INFINITY, b == f64::INFINITY) {
            (false, false) => {
                let mid = 0.5 * (b - a);
                if mid.is_infinite() {
                    0.5 * b - 0.5 * a
                } else if mid == 0.0 {
                    0.0
                } else {
                    mid
                }
            }
            (false, true) => f64::MAX,
            (true, false) => f64::MIN,
            (true, true) => 0.0,
        }
    }

    pub fn contains_zero(self) -> bool {
        unsafe {
            let cmp = _mm_cmpge_pd(self.0, _mm_set_pd(0.0, 0.0));
            _mm_movemask_pd(cmp) == 3
        }
    }

    /// Returns the mignitude of `self` if it is nonempty; otherwise, a NaN.
    ///
    /// The mignitude of a nonempty interval $𝒙 = \[a, b\]$ is:
    ///
    /// $$
    /// \begin{align*}
    ///  \mig(𝒙) &= \inf \set{|x| ∣ x ∈ 𝒙} \\\\
    ///   &= \begin{cases}
    ///     \min \set{|a|, |b|} & \if \sgn(a) = \sgn(b), \\\\
    ///     0                   & \otherwise.
    ///    \end{cases}
    /// \end{align*}
    /// $$
    ///
    /// The exact value is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert_eq!(Interval::new(-2.0, 3.0).mig(), 0.0);
    /// assert_eq!(Interval::new(2.0, 3.0).mig(), 2.0);
    /// assert!(Interval::EMPTY.mig().is_nan());
    /// assert_eq!(Interval::ENTIRE.mig(), 0.0);
    /// ```
    ///
    /// See also: [`Interval::mag`].
    pub fn mig(self) -> f64 {
        if self.contains_zero() {
            return 0.0;
        }

        f64::max(-self.lo(), -self.hi())
    }

    /// Returns the radius of `self` if it is nonempty; otherwise, a NaN.
    ///
    /// The radius of a nonempty interval $𝒙 = \[a, b\]$ is:
    ///
    /// $$
    /// \rad(𝒙) = \frac{b - a}{2}.
    /// $$
    ///
    /// As an approximation in [`f64`], it returns the least [`f64`] number `r` that satisfies
    /// $\self ⊆ \[𝚖 - 𝚛, 𝚖 + 𝚛\]$, where `m` is the midpoint returned by [`Self::mid`].
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert_eq!(Interval::new(-2.0, 3.0).rad(), 2.5);
    /// assert!(Interval::EMPTY.rad().is_nan());
    /// assert_eq!(Interval::ENTIRE.rad(), f64::INFINITY);
    /// ```
    ///
    /// See also: [`Interval::mid`].
    pub fn rad(self) -> f64 {
        let m = self.mid();
        f64::max(add_round_up(self.sup(), -m), add_round_up(m, -self.inf()))
    }

    /// Returns the upper bound of `self`.
    ///
    /// The upper bound of an interval $𝒙$ is:
    ///
    /// $$
    /// \sup(𝒙) = \begin{cases}
    ///   -∞ & \if 𝒙 = ∅, \\\\
    ///   b  & \if 𝒙 = \[a, b\].
    ///  \end{cases}
    /// $$
    ///
    /// The exact value is returned.  When $b = 0$, `0.0` (the
    /// positive zero) is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert_eq!(Interval::new(-2.0, 3.0).sup(), 3.0);
    /// assert_eq!(Interval::EMPTY.sup(), f64::NEG_INFINITY);
    /// assert_eq!(Interval::ENTIRE.sup(), f64::INFINITY);
    /// ```
    ///
    /// See also: [`Interval::inf`].
    pub fn sup(self) -> f64 {
        let x = self.hi();
        if x.is_nan() {
            // The empty interval.
            f64::NEG_INFINITY
        } else if x == 0.0 {
            0.0
        } else {
            x
        }
    }

    /// Returns the width of `self` if it is nonempty; otherwise, a NaN.
    ///
    /// The width of a nonempty interval $𝒙 = \[a, b\]$ is:
    ///
    /// $$
    /// \wid(𝒙) = b - a.
    /// $$
    ///
    /// As an approximation in [`f64`], it returns the closest [`f64`] number toward $+∞$.
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert_eq!(Interval::new(-2.0, 3.0).wid(), 5.0);
    /// assert_eq!(Interval::new(-1.0, f64::MAX).wid(), f64::INFINITY);
    /// assert!(Interval::EMPTY.wid().is_nan());
    /// assert_eq!(Interval::ENTIRE.wid(), f64::INFINITY);
    /// ```
    pub fn wid(self) -> f64 {
        add_round_up(self.hi(), self.lo())
    }


    pub fn squash(self) -> Self {
        let m = self.mid();
        Self::new(m, m)
    }

}
