// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use std::arch::x86_64::*;
use super::*;

#[inline(always)]
pub fn with_rounding_up<T, F>(f: F) -> T
where
    F: FnOnce() -> T,
{
    #[allow(deprecated)]
    unsafe {
        let csr = _mm_getcsr();
        let newcsr = _MM_ROUND_UP | _MM_MASK_MASK;
        if csr & newcsr != newcsr {
            _mm_setcsr(_MM_ROUND_UP | _MM_MASK_MASK);
        }

        let ret = f();

        if csr & newcsr != newcsr {
            _mm_setcsr(csr);
        }
        return ret;
    }
}

// see Lambov, B. (2008). Interval Arithmetic Using SSE-2. In P. Hertling, C. M.
// Hoffmann, W. Luther, & N. Revol (Eds.), Reliable Implementation of Real
// Number Algorithms: Theory and Practice (pp. 102–113). Springer.
// https://doi.org/10.1007/978-3-540-85521-7_6
fn ivchoice(mut a: __m128d, b: __m128d) -> __m128d {
    unsafe {
        // a = [a0, a1]
        // b = [b0, b1]
        //
        // output
        // [c0, c1]
        // where c0 = b0 >= 0 ? a0 : -a1
        // and c1 = b1 >= 0 ? a0 : -a1

        // a = [a0, -a1]
        // _mm_set_pd has the low part as last argument
        a = _mm_xor_pd(a, _mm_set_pd(-0.0, 0.0));

        // cmp[1] = sign bit of b0
        // cmp[65] = sign bit of b1
        let cmp = _mm_srli_epi64(_mm_castpd_si128(b), 62);
        //let cmp = _mm_castpd_si128(_mm_cmplt_pd(b, _mm_set_pd(0.0, 0.0)));
        return _mm_permutevar_pd(a, cmp);
    }
}

impl Interval {
    fn _add(x: Self, y: Self) -> Self {
        Self(unsafe { _mm_add_pd(x.0, y.0) })
    }

    fn _sub(x: Self, y: Self) -> Self {
        Self::_add(x, -y)
    }

    // assume x and y non empty
    fn _mul(x: Self, y: Self) -> Self {
        let a = x.0;
        let b = y.0;

        unsafe {
            let arev = _mm_permute_pd(a, 1);
            let brev = _mm_permute_pd(b, 1);

            let mut u = _mm_mul_pd(ivchoice(a, brev), brev);
            let mut v = _mm_mul_pd(ivchoice(arev, b), b);

            let cu = _mm_cmpeq_pd(u, u);
            let cv = _mm_cmpeq_pd(v, v);

            // if _mm_movemask_pd(_mm_and_pd(cu, cv)) != 3 {
            // NaNs may come from a multiplication ∞ × 0, which should be 0
            // in the context of interval arithmetic, or from the input.

            // replace NaNs with zeros
            u = _mm_and_pd(cu, u);
            v = _mm_and_pd(cv, v);
            let w = _mm_max_pd(u, v);

            return Self(w);
        }
    }

    // assumes that is 0 is not in the interior of y and that x and y are not empty
    fn _div(x: Self, y: Self) -> Self {
        let mut a = x.0;
        let mut b = y.0;

        unsafe {
            b = _mm_xor_pd(b, _mm_set_pd(-0.0, -0.0));

            if y.hi() <= 0.0 {
                a = _mm_permute_pd(a, 1);
                b = _mm_permute_pd(b, 1);
            }

            let num = a;
            let den = ivchoice(b, a);

            // replace ∞ / 0 by positive infinities
            // and 0 / 0 by 0
            let mut w = _mm_div_pd(num, den);
            let zden = _mm_cmpeq_pd(den, Self::ZERO.0);
            let znum = _mm_cmpeq_pd(num, Self::ZERO.0);
            w = _mm_blendv_pd(w, _mm_set_pd(f64::INFINITY, f64::INFINITY), zden);
            w = _mm_blendv_pd(w, _mm_set_pd(0.0, 0.0), znum);

            return Interval(w);
        }
    }

    pub fn recip(self) -> Self {
        Self::ONE / self
    }

    // pub unsafe fn mul(x: RecklessInterval, y: RecklessInterval) -> RecklessInterval {
    //     let a = x.0;
    //     let b = y.0;

    //     unsafe {
    //         let brev = _mm_shuffle_pd(b, b, 1);

    //         let z = _mm_set_pd(0.0, 0.0);
    //         let mut w;
    //         if _mm_ucomile_sd(b, z) != 0 {
    //             w = _mm_mul_pd(ivchoice(brev, a), a);
    //         } else if _mm_ucomile_sd(brev, z) != 0 {
    //             let arev = _mm_shuffle_pd(a, a, 1);
    //             w = _mm_mul_pd(ivchoice(b, arev), arev);
    //         } else {
    //             let arev = _mm_shuffle_pd(a, a, 1);
    //             let aminus = _mm_broadcastsd_pd(a);
    //             let aplus = _mm_broadcastsd_pd(arev);

    //             let mut u = _mm_mul_pd(aminus, brev);
    //             let mut v = _mm_mul_pd(aplus, b);

    //             let cu = _mm_cmpeq_pd(u, u);
    //             let cv = _mm_cmpeq_pd(v, v);
    //             u = _mm_and_pd(u, cu);
    //             v = _mm_and_pd(v, cv);

    //             w = _mm_max_pd(u, v);
    //         }

    //         let cw = _mm_cmpeq_pd(w, w);
    //         w = _mm_and_pd(w, cw);
    //         return RecklessInterval(w);
    //     }
    // }
}

impl std::ops::Add for Interval {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        with_rounding_up(|| Self::_add(self, other))
    }
}

impl std::ops::AddAssign for Interval {
    fn add_assign(&mut self, other: Self) {
        *self = *self + other;
    }
}

impl std::ops::Sub for Interval {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        with_rounding_up(|| Self::_sub(self, other))
    }
}

impl std::ops::SubAssign for Interval {
    fn sub_assign(&mut self, other: Self) {
        *self = *self - other;
    }
}

impl std::ops::Mul for Interval {
    type Output = Self;
    fn mul(self, other: Self) -> Self {
        if self.lo().is_nan() | other.lo().is_nan() {
            return Self::EMPTY;
        }
        with_rounding_up(|| Self::_mul(self, other))
    }
}

impl std::ops::MulAssign for Interval {
    fn mul_assign(&mut self, other: Self) {
        *self = *self * other;
    }
}

impl std::ops::Div for Interval {
    type Output = Self;
    fn div(self, other: Self) -> Self {
        if self.is_empty() | other.is_empty() {
            return Self::EMPTY;
        } else if other == Self::ZERO {
            return Self::EMPTY;
        } else if self == Self::ZERO {
            return Self::ZERO;
        } else if other.sup() > 0.0 && other.inf() < 0.0 {
            return Self::ENTIRE;
        }

        with_rounding_up(|| Self::_div(self, other))
    }
}

impl std::ops::Neg for Interval {
    type Output = Self;
    fn neg(self) -> Self {
        Self(unsafe { _mm_permute_pd(self.0, 1) })
    }
}
