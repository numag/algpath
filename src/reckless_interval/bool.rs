// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use super::*;
use std::arch::x86_64::*;

impl Interval {
    /// Returns `true` if `rhs` is a member of `self`: $\rhs ∈ \self$.
    ///
    /// The result is `false` whenever `rhs` is infinite or NaN.
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert!(Interval::new(1.0, 2.0).contains(1.0));
    /// assert!(!Interval::EMPTY.contains(1.0));
    /// assert!(Interval::ENTIRE.contains(1.0));
    /// ```
    ///
    /// $±∞$ and NaN are not real numbers, thus do not belong to any interval:
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert!(!Interval::ENTIRE.contains(f64::INFINITY));
    /// assert!(!Interval::ENTIRE.contains(f64::NEG_INFINITY));
    /// assert!(!Interval::ENTIRE.contains(f64::NAN));
    /// ```
    pub fn contains(self, rhs: f64) -> bool {
        rhs.is_finite() & Self::new(rhs, rhs).subset(self)
    }

    /// Returns `true` if `self` and `rhs` are disjoint:
    ///
    /// $$
    /// \self ∩ \rhs = ∅,
    /// $$
    ///
    /// or equivalently,
    ///
    /// $$
    /// ∀x ∈ \self, ∀y ∈ \rhs : x ≠ y,
    /// $$
    ///
    /// or equivalently,
    ///
    /// |                    | $\rhs = ∅$ | $\rhs = \[c, d\]$ |
    /// | :----------------: | :--------: | :---------------: |
    /// | $\self = ∅$        | `true`     | `true`            |
    /// | $\self = \[a, b\]$ | `true`     | $b < c ∨ d < a$   |
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// //assert!(Interval::new(1.0, 2.0).disjoint(Interval::new(3.0, 4.0)));
    /// //assert!(!Interval::new(1.0, 3.0).disjoint(Interval::new(3.0, 4.0)));
    /// //assert!(!Interval::new(1.0, 5.0).disjoint(Interval::new(3.0, 4.0)));
    /// //assert!(Interval::EMPTY.disjoint(Interval::EMPTY));
    /// assert!(Interval::EMPTY.disjoint(Interval::ENTIRE));
    /// ```
    pub fn disjoint(self, rhs: Self) -> bool {
        dbg!(self, rhs, self.strict_precedes(rhs));
        self.strict_precedes(rhs) | rhs.strict_precedes(self)
    }

    /// Returns `true` if `self` is interior to `rhs`:
    ///
    /// $$
    /// (∀x ∈ \self, ∃y ∈ \rhs : x < y) ∧ (∀x ∈ \self, ∃y ∈ \rhs : y < x),
    /// $$
    ///
    /// or equivalently,
    ///
    /// |                    | $\rhs = ∅$ | $\rhs = \[c, d\]$ |
    /// | :----------------: | :--------: | :---------------: |
    /// | $\self = ∅$        | `true`     | `true`            |
    /// | $\self = \[a, b\]$ | `false`    | $c <′ a ∧ b <′ d$ |
    ///
    /// where $<′$ is defined as:
    ///
    /// $$
    /// x <′ y :⟺ x < y ∨ x = y = -∞ ∨ x = y = +∞.
    /// $$
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert!(Interval::new(1.1, 1.9).interior(Interval::new(1.0, 2.0)));
    /// assert!(!Interval::new(1.1, 2.0).interior(Interval::new(1.0, 2.0)));
    /// assert!(Interval::EMPTY.interior(Interval::EMPTY));
    /// assert!(Interval::ENTIRE.interior(Interval::ENTIRE));
    /// ```
    pub fn interior(self, rhs: Self) -> bool {
        if self.is_empty() {
            return true;
        }
        unsafe {
            let u = _mm_cmplt_pd(self.0, rhs.0);
            let v = _mm_cmpeq_pd(rhs.0, Self::ENTIRE.0);
            _mm_movemask_pd(_mm_or_pd(u, v)) == 3
        }
    }

    /// Returns `true` if `self` is nonempty and bounded.
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert!(Interval::new(1.0, 2.0).is_common_interval());
    /// assert!(!Interval::new(1.0, f64::INFINITY).is_common_interval());
    /// assert!(!Interval::EMPTY.is_common_interval());
    /// assert!(!Interval::ENTIRE.is_common_interval());
    /// ```
    pub fn is_common_interval(self) -> bool {
        self.lo().is_finite() & self.hi().is_finite()
    }

    /// Returns `true` if `self` is empty: $\self = ∅$.
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert!(!Interval::new(1.0, 1.0).is_empty());
    /// assert!(Interval::EMPTY.is_empty());
    /// assert!(!Interval::ENTIRE.is_empty());
    /// ```
    pub fn is_empty(self) -> bool {
        self.lo().is_nan()
    }

    /// Returns `true` if $\self = \[-∞, +∞\]$.
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert!(!Interval::new(1.0, f64::INFINITY).is_entire());
    /// assert!(!Interval::EMPTY.is_entire());
    /// assert!(Interval::ENTIRE.is_entire());
    /// ```
    pub fn is_entire(self) -> bool {
        self == Self::ENTIRE
    }

    /// Returns `true` if `self` consists of a single real number:
    ///
    /// $$
    /// ∃x ∈ ℝ : \self = \[x, x\].
    /// $$
    ///
    /// The result is `false` whenever `self` is empty or unbounded.
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert!(Interval::new(1.0, 1.0).is_singleton());
    /// assert!(!Interval::new(1.0, 2.0).is_singleton());
    /// assert!(!Interval::EMPTY.is_singleton());
    /// assert!(!Interval::ENTIRE.is_singleton());
    /// ```
    ///
    /// 0.1 is not representable as a [`f64`] number:
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// // The singleton interval that consists of the closest [`f64`] number to 0.1.
    /// assert!(Interval::new(0.1, 0.1).is_singleton());
    /// // The tightest interval that encloses 0.1.
    /// #[cfg(feature = "gmp")]
    /// assert!(!interval!("[0.1, 0.1]").unwrap().is_singleton());
    /// ```
    pub fn is_singleton(self) -> bool {
        -self.lo() == self.hi()
    }

    /// Returns `true` if `self` is weakly less than `rhs`:
    ///
    /// $$
    /// (∀x ∈ \self, ∃y ∈ \rhs : x ≤ y) ∧ (∀y ∈ \rhs, ∃x ∈ \self : x ≤ y),
    /// $$
    ///
    /// or equivalently,
    ///
    /// |                    | $\rhs = ∅$ | $\rhs = \[c, d\]$ |
    /// | :----------------: | :--------: | :---------------: |
    /// | $\self = ∅$        | `true`     | `false`           |
    /// | $\self = \[a, b\]$ | `false`    | $a ≤ c ∧ b ≤ d$   |
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert!(Interval::new(1.0, 2.0).less(Interval::new(3.0, 4.0)));
    /// assert!(Interval::new(1.0, 3.0).less(Interval::new(2.0, 4.0)));
    /// assert!(Interval::new(1.0, 4.0).less(Interval::new(1.0, 4.0)));
    /// assert!(Interval::EMPTY.less(Interval::EMPTY));
    /// assert!(!Interval::EMPTY.less(Interval::ENTIRE));
    /// assert!(!Interval::ENTIRE.less(Interval::EMPTY));
    /// assert!(Interval::ENTIRE.less(Interval::ENTIRE));
    /// ```
    pub fn less(self, rhs: Self) -> bool {
        self.is_empty() && rhs.is_empty() || self.sup() <= rhs.sup() && self.inf() <= rhs.inf()
    }

    /// Returns `true` if `self` is to the left of `rhs` but may touch it:
    ///
    /// $$
    /// ∀x ∈ \self, ∀y ∈ \rhs : x ≤ y,
    /// $$
    ///
    /// or equivalently,
    ///
    /// |                    | $\rhs = ∅$ | $\rhs = \[c, d\]$ |
    /// | :----------------: | :--------: | :---------------: |
    /// | $\self = ∅$        | `true`     | `true`            |
    /// | $\self = \[a, b\]$ | `true`     | $b ≤ c$           |
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert!(Interval::new(1.0, 2.0).precedes(Interval::new(3.0, 4.0)));
    /// assert!(Interval::new(1.0, 3.0).precedes(Interval::new(3.0, 4.0)));
    /// assert!(!Interval::new(1.0, 3.0).precedes(Interval::new(2.0, 4.0)));
    /// assert!(Interval::EMPTY.precedes(Interval::EMPTY));
    /// assert!(Interval::EMPTY.precedes(Interval::ENTIRE));
    /// assert!(Interval::ENTIRE.precedes(Interval::EMPTY));
    /// assert!(!Interval::ENTIRE.precedes(Interval::ENTIRE));
    /// ```
    pub fn precedes(self, rhs: Self) -> bool {
        self.sup() <= rhs.inf()
    }

    /// Returns `true` if `self` is strictly less than `rhs`:
    ///
    /// $$
    /// (∀x ∈ \self, ∃y ∈ \rhs : x < y) ∧ (∀y ∈ \self, ∃x ∈ \rhs : x < y),
    /// $$
    ///
    /// or equivalently,
    ///
    /// |                    | $\rhs = ∅$ | $\rhs = \[c, d\]$ |
    /// | :----------------: | :--------: | :---------------: |
    /// | $\self = ∅$        | `true`     | `false`           |
    /// | $\self = \[a, b\]$ | `false`    | $a <′ c ∧ b <′ d$ |
    ///
    /// where $<′$ is defined as:
    ///
    /// $$
    /// x <′ y :⟺ x < y ∨ x = y = -∞ ∨ x = y = +∞.
    /// $$
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert!(Interval::new(1.0, 2.0).strict_less(Interval::new(3.0, 4.0)));
    /// assert!(Interval::new(1.0, 3.0).strict_less(Interval::new(2.0, 4.0)));
    /// assert!(!Interval::new(1.0, 4.0).strict_less(Interval::new(2.0, 4.0)));
    /// assert!(Interval::new(1.0, f64::INFINITY).strict_less(Interval::new(2.0, f64::INFINITY)));
    /// assert!(Interval::EMPTY.strict_less(Interval::EMPTY));
    /// assert!(!Interval::EMPTY.strict_less(Interval::ENTIRE));
    /// assert!(!Interval::ENTIRE.strict_less(Interval::EMPTY));
    /// assert!(Interval::ENTIRE.strict_less(Interval::ENTIRE));
    /// ```
    pub fn strict_less(self, rhs: Self) -> bool {
        self.is_empty() && rhs.is_empty()
            || !(self.sup() - rhs.sup() >= 0.0) && !(self.inf() - rhs.inf() >= 0.0)
    }

    /// Returns `true` if `self` is strictly to the left of `rhs`:
    ///
    /// $$
    /// ∀x ∈ \self, ∀y ∈ \rhs : x < y,
    /// $$
    ///
    /// or equivalently,
    ///
    /// |                    | $\rhs = ∅$ | $\rhs = \[c, d\]$ |
    /// | :----------------: | :--------: | :---------------: |
    /// | $\self = ∅$        | `true`     | `true`            |
    /// | $\self = \[a, b\]$ | `true`     | $b < c$           |
    ///
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert!(Interval::new(1.0, 2.0).strict_precedes(Interval::new(3.0, 4.0)));
    /// assert!(!Interval::new(1.0, 3.0).strict_precedes(Interval::new(3.0, 4.0)));
    /// assert!(!Interval::new(1.0, 3.0).strict_precedes(Interval::new(2.0, 4.0)));
    /// assert!(Interval::EMPTY.strict_precedes(Interval::EMPTY));
    /// assert!(Interval::EMPTY.strict_precedes(Interval::ENTIRE));
    /// assert!(Interval::ENTIRE.strict_precedes(Interval::EMPTY));
    /// ```
    pub fn strict_precedes(self, rhs: Self) -> bool {
        self.is_empty() || rhs.is_empty() || self.sup() < rhs.inf()
    }

    /// Returns `true` if `self` is a subset of `rhs`:
    ///
    /// $$
    /// \self ⊆ \rhs,
    /// $$
    ///
    /// or equivalently,
    ///
    /// $$
    /// ∀x ∈ \self, ∃y ∈ \rhs : x = y,
    /// $$
    ///
    /// or equivalently,
    ///
    /// |                    | $\rhs = ∅$ | $\rhs = \[c, d\]$ |
    /// | :----------------: | :--------: | :---------------: |
    /// | $\self = ∅$        | `true`     | `true`            |
    /// | $\self = \[a, b\]$ | `false`    | $c ≤ a ∧ b ≤ d$   |
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert!(Interval::new(1.0, 2.0).subset(Interval::new(1.0, 2.0)));
    /// assert!(Interval::EMPTY.subset(Interval::EMPTY));
    /// assert!(Interval::EMPTY.subset(Interval::ENTIRE));
    /// assert!(Interval::ENTIRE.subset(Interval::ENTIRE));
    /// ```
    pub fn subset(self, rhs: Self) -> bool {
        self.is_empty() | unsafe { _mm_movemask_pd(_mm_cmple_pd(self.0, rhs.0)) == 3 }
    }

    /// Returns `true` if `self` is nonempty and bounded.
    ///
    /// # Examples
    ///
    /// ```
    /// use algpath::reckless_interval::*;
    /// assert!(Interval::new(1.0, 2.0).is_common_interval());
    /// assert!(!Interval::new(1.0, f64::INFINITY).is_common_interval());
    /// assert!(!Interval::EMPTY.is_common_interval());
    /// assert!(!Interval::ENTIRE.is_common_interval());
    /// ```
    pub fn is_bounded(self) -> bool {
        (self.lo() < f64::INFINITY) & (self.hi() < f64::INFINITY)
    }
}
