// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use std::arch::x86_64::*;
use super::*;

impl Interval {
    pub fn intersection(self, other: Self) -> Self {
        let i = Self(unsafe{_mm_min_pd(self.0, other.0)});
        if -i.lo() <= i.hi() {
            i
        } else {
            Self::EMPTY
        }
    }
}
