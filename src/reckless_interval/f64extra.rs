// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later


fn twosum(a: f64, b: f64) -> (f64, f64) {
    let x = a + b;
    let z = x - a;
    let y = (a - (x - z)) + (b - z);
    return (x, y);
}

// https://doc.rust-lang.org/beta/src/core/num/f64.rs.html#778
pub fn next_up(x: f64) -> f64 {
    // We must use strictly integer arithmetic to prevent denormals from
    // flushing to zero after an arithmetic operation on some platforms.
    const TINY_BITS: u64 = 0x1; // Smallest positive f64.
    const CLEAR_SIGN_MASK: u64 = 0x7fff_ffff_ffff_ffff;

    let bits = x.to_bits();
    if x.is_nan() || bits == f64::INFINITY.to_bits() {
        return x;
    }

    let abs = bits & CLEAR_SIGN_MASK;
    let next_bits = if abs == 0 {
        TINY_BITS
    } else if bits == abs {
        bits + 1
    } else {
        bits - 1
    };
    f64::from_bits(next_bits)
}

fn round_up((hi, lo): (f64, f64)) -> f64 {
    if lo <= 0.0 {
        hi
    } else {
        next_up(hi)
    }
}

pub fn add_round_up(x: f64, y: f64) -> f64 {
    round_up(twosum(x, y))
}
