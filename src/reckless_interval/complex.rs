// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use super::*;
use std::arch::x86_64::*;
use std::ops::*;

pub mod arith;

#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct ComplexInterval(__m256d);

impl std::fmt::Display for ComplexInterval {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} + {}*im", self.re(), self.im())
    }
}

impl std::hash::Hash for ComplexInterval {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.re().hash(state);
        self.im().hash(state);
    }
}

impl PartialEq for ComplexInterval {
    fn eq(&self, other: &Self) -> bool {
        self.re() == other.re() && self.im() == other.im()
    }
}

impl Eq for ComplexInterval {}

impl ComplexInterval {
    pub const IM: Self = Self::new(Interval::ZERO, Interval::ONE);
    pub const ZERO: Self = Self::new(Interval::ZERO, Interval::ZERO);
    pub const ONE: Self = Self::new(Interval::ONE, Interval::ZERO);

    fn rep(self) -> [Interval; 2] {
        unsafe { std::mem::transmute(self) }
    }

    pub fn re(self) -> Interval {
        self.rep()[0]
    }

    pub fn im(self) -> Interval {
        self.rep()[1]
    }

    pub const fn new(re: Interval, im: Interval) -> Self {
        unsafe { std::mem::transmute([re, im]) }
    }

    pub fn conj(&self) -> Self {
        Self::new(self.re(), -self.im())
    }

    pub fn abs2(&self) -> Interval {
        self.re() * self.re() + self.im() * self.im()
    }

    pub fn square(r: f64) -> Self {
        Self::new(Interval::new(-r, r), Interval::new(-r, r))
    }

    pub fn mag(self) -> f64 {
        self.re().mag().max(self.im().mag())
    }

    pub fn wid(self) -> f64 {
        self.re().wid().max(self.im().wid())
    }

    pub fn mid(self) -> nalgebra::Complex<f64> {
        nalgebra::Complex {
            re: self.re().mid(),
            im: self.im().mid(),
        }
    }

    pub fn squash(self) -> Self {
        Self::new(self.re().squash(), self.im().squash())
    }

    pub fn is_common(self) -> bool {
        let inf = Self::new(Interval::ENTIRE, Interval::ENTIRE);
        unsafe {
            let c = _mm256_cmp_pd(self.0, inf.0, _CMP_LT_OQ);
            return _mm256_movemask_pd(c) == 15;
        }
    }
}

impl From<Interval> for ComplexInterval {
    fn from(t: Interval) -> Self {
        Self::new(t, Interval::ZERO)
    }
}

impl Add for ComplexInterval {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        with_rounding_up(|| arith::_add(self, other))
    }
}

impl AddAssign for ComplexInterval {
    fn add_assign(&mut self, other: Self) {
        *self = *self + other;
    }
}

impl Sub for ComplexInterval {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        self + (-other)
    }
}

impl SubAssign for ComplexInterval {
    fn sub_assign(&mut self, other: Self) {
        *self = *self - other;
    }
}

impl Mul for ComplexInterval {
    type Output = Self;
    fn mul(self, other: Self) -> Self {
        with_rounding_up(|| arith::_mul(self, other))
        // Self::new(
        //     self.re() * other.re() - self.im() * other.im(),
        //     self.im() * other.re() + self.re() * other.im(),
        // )
    }
}

impl Mul<Interval> for ComplexInterval {
    type Output = Self;
    fn mul(self, other: Interval) -> Self {
        Self::new(self.re() * other, self.im() * other)
    }
}

impl MulAssign<ComplexInterval> for ComplexInterval {
    fn mul_assign(&mut self, other: ComplexInterval) {
        *self = *self * other;
    }
}

impl MulAssign<Interval> for ComplexInterval {
    fn mul_assign(&mut self, other: Interval) {
        *self = *self * other;
    }
}

impl Div for ComplexInterval {
    type Output = Self;
    fn div(self, other: Self) -> Self {
        self * other.conj() * other.abs2().recip()
    }
}

impl num_traits::identities::Zero for ComplexInterval {
    fn zero() -> Self {
        Self::ZERO
    }
    fn is_zero(&self) -> bool {
        *self == Self::ZERO
    }
    fn set_zero(&mut self) {
        *self = Self::ZERO;
    }
}

impl num_traits::identities::One for ComplexInterval {
    fn one() -> Self {

        Self::ONE
    }
    fn is_one(&self) -> bool {
        *self == Self::ONE
    }
    fn set_one(&mut self) {
        *self = Self::ONE;
    }
}

impl From<i64> for ComplexInterval {
    fn from(o: i64) -> Self {
        Interval::new(o as f64, o as f64).into()
    }
}

impl ComplexInterval {
    pub fn matrix_mag<
        R: nalgebra::Dim,
        C: nalgebra::Dim,
        S: nalgebra::RawStorage<ComplexInterval, R, C>,
    >(
        mat: &nalgebra::Matrix<ComplexInterval, R, C, S>,
    ) -> f64 {
        let mut m: f64 = 0.0;
        for c in mat {
            m = m.max(c.mag());
        }
        return m;
    }

    pub fn matrix_wid<
        R: nalgebra::Dim,
        C: nalgebra::Dim,
        S: nalgebra::RawStorage<ComplexInterval, R, C>,
    >(
        mat: &nalgebra::Matrix<ComplexInterval, R, C, S>,
    ) -> f64 {
        let mut m: f64 = 0.0;
        for c in mat {
            m = m.max(c.wid());
        }
        return m;
    }
}

struct ComplexFloatVisitor;

impl<'de> serde::de::Visitor<'de> for ComplexFloatVisitor {
    type Value = ComplexInterval;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "a floating point complex number")
    }

    fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        crate::parser::COMPLEX.parse(s).map_err(|e| E::custom(e))
    }
}

impl<'de> serde::Deserialize<'de> for ComplexInterval {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_str(ComplexFloatVisitor)
    }
}
