// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

// COMMAND LINE INTERFACE

// OPTIONS

use std::{collections::HashMap, io::Read};

use num_traits::{One, Zero};

use crate::{
    circuit::{self, Circuit},
    num::CIF,
    prog::Program,
    track::ParametricSystem,
};

#[derive(clap::Parser)]
#[command(author, version, about)]
pub struct Configuration {
    /// puts the equations into Horner form before processing
    #[arg(long)]
    pub horner: bool,

    /// enable homogenization of equations, does nothing if the input is already homogeneous
    #[arg(long)]
    pub homogenize: bool,

    /// input in legacy format
    #[arg(long, hide = true)]
    pub legacy: bool,

    /// differentiation mode for circuits
    #[arg(long, default_value = "forward")]
    pub diff: circuit::DifferentiationMode,

    /// Number of threads. Default to the number of available CPUs, or 1 if the
    /// program fails to determine this number.
    #[arg(long)]
    pub jobs: Option<usize>,

    /// Input file in JSON syntax. If none given, read standard input.
    pub path: Option<std::path::PathBuf>,

    #[arg(long, hide = true)]
    pub markdown_help: bool,
}

impl Default for Configuration {
    fn default() -> Self {
        Configuration {
            horner: false,
            homogenize: false,
            legacy: false,
            diff: circuit::DifferentiationMode::Forward,
            jobs: None,
            path: None,
            markdown_help: false,
        }
    }
}

// JSON INPUT

#[derive(serde::Deserialize, Clone)]
pub struct Problem {
    pub system: Vec<Circuit<CIF>>,
    pub variables: Vec<Circuit<CIF>>,
    pub parameters: Vec<Circuit<CIF>>,
    pub path: Vec<Vec<CIF>>,
    pub fiber: Vec<Vec<CIF>>,
}

#[derive(serde::Deserialize, Clone)]
struct ProblemOldFormat {
    vars: Vec<Circuit<CIF>>,
    parameter: Circuit<CIF>,
    sys: Vec<Circuit<CIF>>,
    initial_sols: Vec<Vec<CIF>>,
}

impl From<ProblemOldFormat> for Problem {
    fn from(pb: ProblemOldFormat) -> Self {
        Problem {
            variables: pb.vars,
            parameters: vec![pb.parameter],
            system: pb.sys,
            path: vec![vec![CIF::zero()], vec![CIF::one()]],
            fiber: pb.initial_sols,
        }
    }
}

impl Problem {
    pub fn new_from_string(str: &String, conf: &Configuration) -> Result<Self, std::io::Error> {
        let pb: Problem;
        if conf.legacy {
            let pb_legacy_format: ProblemOldFormat = serde_json::from_str(str)?;
            pb = pb_legacy_format.into();
        } else {
            pb = serde_json::from_str(str)?;
        }
        return Ok(pb);
    }

    pub fn new_from_input(conf: &Configuration) -> Result<Self, std::io::Error> {
        let mut input = String::new();
        if let Some(path) = &conf.path {
            let mut fs = std::fs::File::open(path)?;
            fs.read_to_string(&mut input).unwrap();
        } else {
            std::io::stdin().read_to_string(&mut input)?;
        }

        Self::new_from_string(&input, conf)
    }

    pub fn build_parametric_system(
        &mut self,
        conf: &Configuration,
    ) -> Result<ParametricSystem, std::io::Error> {
        let dim = self.system.len();
        let nparams = self.parameters.len();
        assert!(dim == self.variables.len());

        let mut allvars = Vec::new();
        allvars.extend_from_slice(&self.parameters);
        allvars.extend_from_slice(&self.variables);

        if conf.horner {
            for eq in &mut self.system {
                *eq = eq.horner(&allvars);
            }
        }

        let jac = Circuit::jacobian_matrix(&self.system, &allvars, conf.diff);
        let f0 = Program::compile(&self.system, &allvars);

        let jacp = jac.columns(0, self.parameters.len());
        let fdot0 = Program::compile(&jacp.iter().cloned().collect(), &allvars);

        let jacv = jac.columns(self.parameters.len(), self.variables.len());
        let df0 = Program::compile(&jacv.iter().cloned().collect(), &allvars);

        let mut f = vec![f0];
        let mut df = vec![df0];
        let mut fdot = vec![fdot0];

        let mut chart_switch_threshold = f64::INFINITY;

        if conf.homogenize {
            let ctx = self.variables[0].context();
            chart_switch_threshold = 2.0;
            let homogenization_var = ctx.var("__homogenization_variable__");

            let mut homogenized_equations = Vec::new();
            for eq in &mut self.system {
                let (_degree, heq) = eq.homogenize(&self.variables, &homogenization_var);
                homogenized_equations.push(heq);
            }

            for i in 0..dim {
                let mut dict = HashMap::new();
                dict.insert(self.variables[i].clone(), ctx.scalar(CIF::ONE));
                let sys_in_chart: Vec<_> = homogenized_equations
                    .iter()
                    .map(|c| c.subs_with_memo(&mut dict))
                    .collect();

                let mut allvars = Vec::new();
                allvars.extend_from_slice(&self.parameters);
                allvars.push(homogenization_var.clone());
                allvars.extend_from_slice(&self.variables[..i]);
                allvars.extend_from_slice(&self.variables[i+1..]);

                let jac = Circuit::jacobian_matrix(&sys_in_chart, &allvars, conf.diff);
                f.push(Program::compile(&sys_in_chart, &allvars));

                let jacp = jac.columns(0, self.parameters.len());
                fdot.push(Program::compile(&jacp.iter().cloned().collect(), &allvars));

                let jacv = jac.columns(self.parameters.len(), self.variables.len());
                df.push(Program::compile(&jacv.iter().cloned().collect(), &allvars));
            }
        }

        return Ok(ParametricSystem {
            nparams,
            dim,
            f,
            df,
            fdot,
            chart_switch_threshold,
        });
    }
}
