mod itf1788_tests {
    //mod abs_rev;
    // mod atan2;
    // mod c_xsc;
    // mod fi_lib;
    // mod ieee1788_constructors;
    // mod ieee1788_exceptions;
    mod libieeep1788_bool;
    // mod libieeep1788_cancel;
    // mod libieeep1788_class;
    mod libieeep1788_elem;
    // mod libieeep1788_mul_rev;
    mod libieeep1788_num;
    // mod libieeep1788_overlap;
    // mod libieeep1788_rec_bool;
    //mod libieeep1788_reduction;
    //mod libieeep1788_rev;
    // mod libieeep1788_set;
    // mod mpfi;
    //mod pow_rev;
}

#[macro_export]
macro_rules! assert_feq {
    ($left:expr, $right:expr $(,)?) => {
        match (&$left, &$right) {
            (left_val, right_val) => {
                if !(f64::is_nan(*left_val) && f64::is_nan(*right_val) || *left_val == *right_val) {
                    panic!(
                        r#"assertion failed: `(left == right)`
  left: `{:?}`,
 right: `{:?}`"#,
                        &*left_val, &*right_val
                    )
                }
            }
        }
    };
}
