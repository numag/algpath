// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{collections::HashMap, path::PathBuf};

use algpath::{
    num::{ComplexIntervalEvaluator, CIF},
    reckless_interval::Interval,
    track::ProjectivePoint,
};

use nalgebra::DVector;

#[test]
fn eval_katsura_20() {
    let path = PathBuf::from("data/katsura-20-partial.json");
    let conf = algpath::cli::Configuration {
        path: Some(path),
        ..Default::default()
    };

    let mut input_problem = algpath::cli::Problem::new_from_input(&conf).unwrap();
    let parametric_system = input_problem.build_parametric_system(&conf).unwrap();

    let n = input_problem.system.len();
    let mut pt: Vec<CIF> = Vec::new();
    for i in 1..=n {
        pt.push(CIF::new(Interval::new(i as f64, i as f64), Interval::ZERO));
    }
    let mut buf: Vec<CIF> = Vec::new();

    let val = parametric_system.f(
        &DVector::from_element(1, CIF::ONE),
        &ProjectivePoint {
            coord: pt.clone().into(),
            chart: 0,
        },
        &mut buf,
        ComplexIntervalEvaluator {},
    );

    let reference_values = [
        460, 6620, 6158, 5701, 5252, 4814, 4390, 3983, 3596, 3232, 2894, 2585, 2308, 2066, 1862,
        1699, 1580, 1508, 1486, 1517, 1604,
    ];

    for i in 0..=20 {
        println!("{}", input_problem.system[i]);
        // direct evaluation
        let mut pt_dict = HashMap::new();
        pt_dict.insert(input_problem.parameters[0].clone(), CIF::ONE);
        for i in 0..=20 {
            pt_dict.insert(input_problem.variables[i].clone(), pt[i]);
        }
        let val0 = input_problem.system[i].eval(&pt_dict).unwrap();

        assert!(val0.im().contains_zero());
        assert!(val0.re().contains(reference_values[i] as f64));

        assert!(val[i].im().contains_zero());
        assert!(val[i].re().contains(reference_values[i] as f64));
    }
}
