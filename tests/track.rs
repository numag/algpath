// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use std::path::PathBuf;


#[test]
fn track() {
    let path = PathBuf::from("data/2-2_1.json");
    let mut conf = algpath::cli::Configuration {
        path: Some(path),
        ..Default::default()
    };

    for homogenize in [true, false] {
        for horner in [true, false] {
            conf.homogenize = homogenize;
            conf.horner = horner;

            let mut input_problem = algpath::cli::Problem::new_from_input(&conf).unwrap();
            let mut parametric_system = input_problem.build_parametric_system(&conf).unwrap();

            let path = &input_problem.path;
            for point in &input_problem.fiber {
                let proj_point = algpath::track::ProjectivePoint {
                    coord: point.clone().into(),
                    chart: 0,
                };
                let mbox = parametric_system
                    .find_moore_box(&path[0].clone().into(), proj_point, &mut Vec::new())
                    .unwrap();

                let _tr = parametric_system
                    .track(mbox, &path[1].clone().into())
                    .unwrap();
            }
        }
    }
}
