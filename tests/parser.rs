// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use std::collections::HashMap;

use algpath::{circuit::Context, num::{CIF, Interval}};

#[test]
fn expr_parse() {
    let ctx = Context::<CIF>::default();
    let parser = algpath::parser::expr::ExprParser::new();

    {
        let p = parser.parse(&ctx, "x");
        assert!(p == Ok(ctx.var("x")));
    }

    {
        let p = parser.parse(&ctx, "-(1.0+ 2*(x-y)^8+(-x))*(y+im)").unwrap();
        let mut pt = HashMap::new();
        pt.insert(ctx.var("x"), Interval::new(0.15, 0.15).into());
        pt.insert(ctx.var("y"), Interval::new(0.2, 0.2).into());

        let ev = p.eval(&pt).unwrap();
        dbg!(ev);
        assert!(ev.re().contains(-0.170000000015625) && ev.im().contains(-0.850000000078125));
    }

    {
        let s = ["(-0.932203744865119 + 0.552213152383314*I)*t*x0^2 + 12.0000000000000*t*x1^2 - t*x0 + (0.932203744865119 - 0.552213152383314*I)*x0^2 + 14.0000000000000*t*x1 + (-1.06779625513488 - 0.552213152383314*I)*t - 0.932203744865119 + 0.552213152383314*I",
        "(-2.00000000000000)*t*x0^2 + t*x0*x1 + (0.559594195166760 + 0.593167846399201*I)*t*x1^2 - t*x0 + 3.00000000000000*t*x1 + (-0.559594195166760 - 0.593167846399201*I)*x1^2 + (0.440405804833240 - 0.593167846399201*I)*t + 0.559594195166760 + 0.593167846399201*I"];

        assert!(parser.parse(&ctx, s[0]).is_ok());
        assert!(parser.parse(&ctx, s[1]).is_ok());
    }
}

#[test]
fn complex_parse() {
    let parser = algpath::parser::complex::ComplexParser::new();

    let inis = [
        "0.309016994374947 + 0.951056516295154*I",
        "0.309016994374947 - 0.951056516295154*I",
        "-0.309016994374948 + 0.951056516295154*I",
        "-0.309016994374948 - 0.951056516295154*I",
        "-0.809016994374947 + 0.587785252292473*I",
        "-0.809016994374947 - 0.587785252292473*I",
        "0.809016994374947 + 0.587785252292473*I",
        "0.809016994374947 - 0.587785252292473*I",
        "-1.00000000000000",
        "1.00000000000000",
        "-1.00000000000000 + 1.22464679914735e-16*I",
        "-I",
        "0.309016994374947 + 0.951056516295154*im",
        "0.309016994374947 - 0.951056516295154*im",
        "-0.309016994374948 + 0.951056516295154*im",
        "-0.309016994374948 - 0.951056516295154*im",
        "-0.809016994374947 + 0.587785252292473*im",
        "-0.809016994374947 - 0.587785252292473*im",
        "0.809016994374947 + 0.587785252292473*im",
        "0.809016994374947 - 0.587785252292473*im",
        "0.309016994374947+0.951056516295154*im",
        "0.309016994374947-0.951056516295154*im",
        "-0.309016994374948+0.951056516295154*im",
        "-0.309016994374948-0.951056516295154*im",
        "-0.809016994374947+0.587785252292473*im",
        "-0.809016994374947-0.587785252292473*im",
        "0.809016994374947+0.587785252292473*im",
        "0.809016994374947-0.587785252292473*im",
        "-1.00000000000000",
        "1.00000000000000",
        "-1.00000000000000 + 1.22464679914735e-16*im",
    ];

    for ini in inis {
        parser.parse(ini).unwrap();
        assert!(parser.parse(ini).is_ok());
    }
}
