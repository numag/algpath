/*
 *
 * Unit tests from libieeep1788 for elementary interval functions
 * (Original author: Marco Nehmeier)
 * converted into portable ITL format by Oliver Heimlich.
 *
 * Copyright 2013-2015 Marco Nehmeier (nehmeier@informatik.uni-wuerzburg.de)
 * Copyright 2015-2017 Oliver Heimlich (oheim@posteo.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
//Language imports

//Test library imports

//Arithmetic library imports

//Preamble
use algpath::reckless_interval::Interval as I;

#[test]
fn minimal_neg_test() {
    assert_eq!(-I::new(1.0, 2.0), I::new(-2.0, -1.0));
    assert_eq!(-I::EMPTY, I::EMPTY);
    assert_eq!(-I::ENTIRE, I::ENTIRE);
    assert_eq!(-I::new(1.0, f64::INFINITY), I::new(f64::NEG_INFINITY, -1.0));
    assert_eq!(-I::new(f64::NEG_INFINITY, 1.0), I::new(-1.0, f64::INFINITY));
    assert_eq!(-I::new(0.0, 2.0), I::new(-2.0, 0.0));
    assert_eq!(-I::new(-0.0, 2.0), I::new(-2.0, 0.0));
    assert_eq!(-I::new(-2.0, 0.0), I::new(0.0, 2.0));
    assert_eq!(-I::new(-2.0, -0.0), I::new(0.0, 2.0));
    assert_eq!(-I::new(0.0, -0.0), I::new(0.0, 0.0));
    assert_eq!(-I::new(-0.0, -0.0), I::new(0.0, 0.0));
}

#[test]
fn minimal_add_test() {
    assert_eq!(I::EMPTY + I::EMPTY, I::EMPTY);
    assert_eq!(I::new(-1.0, 1.0) + I::EMPTY, I::EMPTY);
    assert_eq!(I::EMPTY + I::new(-1.0, 1.0), I::EMPTY);
    assert_eq!(I::EMPTY + I::ENTIRE, I::EMPTY);
    assert_eq!(I::ENTIRE + I::EMPTY, I::EMPTY);
    assert_eq!(I::ENTIRE + I::new(f64::NEG_INFINITY, 1.0), I::ENTIRE);
    assert_eq!(I::ENTIRE + I::new(-1.0, 1.0), I::ENTIRE);
    assert_eq!(I::ENTIRE + I::new(-1.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(I::ENTIRE + I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(f64::NEG_INFINITY, 1.0) + I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(-1.0, 1.0) + I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(-1.0, f64::INFINITY) + I::ENTIRE, I::ENTIRE);
    assert_eq!(
        I::new(f64::NEG_INFINITY, 2.0) + I::new(f64::NEG_INFINITY, 4.0),
        I::new(f64::NEG_INFINITY, 6.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 2.0) + I::new(3.0, 4.0),
        I::new(f64::NEG_INFINITY, 6.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 2.0) + I::new(3.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(1.0, 2.0) + I::new(f64::NEG_INFINITY, 4.0),
        I::new(f64::NEG_INFINITY, 6.0)
    );
    assert_eq!(I::new(1.0, 2.0) + I::new(3.0, 4.0), I::new(4.0, 6.0));
    assert_eq!(
        I::new(1.0, 2.0) + I::new(3.0, f64::INFINITY),
        I::new(4.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(1.0, f64::INFINITY) + I::new(f64::NEG_INFINITY, 4.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(1.0, f64::INFINITY) + I::new(3.0, 4.0),
        I::new(4.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(1.0, f64::INFINITY) + I::new(3.0, f64::INFINITY),
        I::new(4.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(1.0, 1.7976931348623157e+308) + I::new(3.0, 4.0),
        I::new(4.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-1.7976931348623157e+308, 2.0) + I::new(-3.0, 4.0),
        I::new(f64::NEG_INFINITY, 6.0)
    );
    assert_eq!(
        I::new(-1.7976931348623157e+308, 2.0) + I::new(-3.0, 1.7976931348623157e+308),
        I::ENTIRE
    );
    assert_eq!(
        I::new(1.0, 1.7976931348623157e+308) + I::new(0.0, 0.0),
        I::new(1.0, 1.7976931348623157e+308)
    );
    assert_eq!(
        I::new(1.0, 1.7976931348623157e+308) + I::new(-0.0, -0.0),
        I::new(1.0, 1.7976931348623157e+308)
    );
    assert_eq!(I::new(0.0, 0.0) + I::new(-3.0, 4.0), I::new(-3.0, 4.0));
    assert_eq!(
        I::new(-0.0, -0.0) + I::new(-3.0, 1.7976931348623157e+308),
        I::new(-3.0, 1.7976931348623157e+308)
    );
    assert_eq!(
        I::new(1.9999999999999964, 1.9999999999999964) + I::new(0.1, 0.1),
        I::new(2.099999999999996, 2.0999999999999965)
    );
    assert_eq!(
        I::new(1.9999999999999964, 1.9999999999999964) + I::new(-0.1, -0.1),
        I::new(1.8999999999999964, 1.8999999999999966)
    );
    assert_eq!(
        I::new(-1.9999999999999964, 1.9999999999999964) + I::new(0.1, 0.1),
        I::new(-1.8999999999999966, 2.0999999999999965)
    );
}


#[test]
fn minimal_sub_test() {
    assert_eq!(I::EMPTY - I::EMPTY, I::EMPTY);
    assert_eq!(I::new(-1.0, 1.0) - I::EMPTY, I::EMPTY);
    assert_eq!(I::EMPTY - I::new(-1.0, 1.0), I::EMPTY);
    assert_eq!(I::EMPTY - I::ENTIRE, I::EMPTY);
    assert_eq!(I::ENTIRE - I::EMPTY, I::EMPTY);
    assert_eq!(I::ENTIRE - I::new(f64::NEG_INFINITY, 1.0), I::ENTIRE);
    assert_eq!(I::ENTIRE - I::new(-1.0, 1.0), I::ENTIRE);
    assert_eq!(I::ENTIRE - I::new(-1.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(I::ENTIRE - I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(f64::NEG_INFINITY, 1.0) - I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(-1.0, 1.0) - I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(-1.0, f64::INFINITY) - I::ENTIRE, I::ENTIRE);
    assert_eq!(
        I::new(f64::NEG_INFINITY, 2.0) - I::new(f64::NEG_INFINITY, 4.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 2.0) - I::new(3.0, 4.0),
        I::new(f64::NEG_INFINITY, -1.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 2.0) - I::new(3.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, -1.0)
    );
    assert_eq!(
        I::new(1.0, 2.0) - I::new(f64::NEG_INFINITY, 4.0),
        I::new(-3.0, f64::INFINITY)
    );
    assert_eq!(I::new(1.0, 2.0) - I::new(3.0, 4.0), I::new(-3.0, -1.0));
    assert_eq!(
        I::new(1.0, 2.0) - I::new(3.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, -1.0)
    );
    assert_eq!(
        I::new(1.0, f64::INFINITY) - I::new(f64::NEG_INFINITY, 4.0),
        I::new(-3.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(1.0, f64::INFINITY) - I::new(3.0, 4.0),
        I::new(-3.0, f64::INFINITY)
    );
    assert_eq!(I::new(1.0, f64::INFINITY) - I::new(3.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(
        I::new(1.0, 1.7976931348623157e+308) - I::new(-3.0, 4.0),
        I::new(-3.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-1.7976931348623157e+308, 2.0) - I::new(3.0, 4.0),
        I::new(f64::NEG_INFINITY, -1.0)
    );
    assert_eq!(
        I::new(-1.7976931348623157e+308, 2.0) - I::new(-1.7976931348623157e+308, 4.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(1.0, 1.7976931348623157e+308) - I::new(0.0, 0.0),
        I::new(1.0, 1.7976931348623157e+308)
    );
    assert_eq!(
        I::new(1.0, 1.7976931348623157e+308) - I::new(-0.0, -0.0),
        I::new(1.0, 1.7976931348623157e+308)
    );
    assert_eq!(I::new(0.0, 0.0) - I::new(-3.0, 4.0), I::new(-4.0, 3.0));
    assert_eq!(
        I::new(-0.0, -0.0) - I::new(-3.0, 1.7976931348623157e+308),
        I::new(-1.7976931348623157e+308, 3.0)
    );
    assert_eq!(
        I::new(1.9999999999999964, 1.9999999999999964) - I::new(0.1, 0.1),
        I::new(1.8999999999999964, 1.8999999999999966)
    );
    assert_eq!(
        I::new(1.9999999999999964, 1.9999999999999964) - I::new(-0.1, -0.1),
        I::new(2.099999999999996, 2.0999999999999965)
    );
    assert_eq!(
        I::new(-1.9999999999999964, 1.9999999999999964) - I::new(0.1, 0.1),
        I::new(-2.0999999999999965, 1.8999999999999966)
    );
}


#[test]
fn minimal_mul_test() {
    assert_eq!(I::EMPTY * I::EMPTY, I::EMPTY);
    assert_eq!(I::new(-1.0, 1.0) * I::EMPTY, I::EMPTY);
    assert_eq!(I::EMPTY * I::new(-1.0, 1.0), I::EMPTY);
    assert_eq!(I::EMPTY * I::ENTIRE, I::EMPTY);
    assert_eq!(I::ENTIRE * I::EMPTY, I::EMPTY);
    assert_eq!(I::new(0.0, 0.0) * I::EMPTY, I::EMPTY);
    assert_eq!(I::EMPTY * I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(I::new(-0.0, -0.0) * I::EMPTY, I::EMPTY);
    assert_eq!(I::EMPTY * I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(I::ENTIRE * I::new(0.0, 0.0), I::new(0.0, 0.0));
    assert_eq!(I::ENTIRE * I::new(-0.0, -0.0), I::new(0.0, 0.0));
    assert_eq!(I::ENTIRE * I::new(-5.0, -1.0), I::ENTIRE);
    assert_eq!(I::ENTIRE * I::new(-5.0, 3.0), I::ENTIRE);
    assert_eq!(I::ENTIRE * I::new(1.0, 3.0), I::ENTIRE);
    assert_eq!(I::ENTIRE * I::new(f64::NEG_INFINITY, -1.0), I::ENTIRE);
    assert_eq!(I::ENTIRE * I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE);
    assert_eq!(I::ENTIRE * I::new(-5.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(I::ENTIRE * I::new(1.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(I::ENTIRE * I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(1.0, f64::INFINITY) * I::new(0.0, 0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(1.0, f64::INFINITY) * I::new(-0.0, -0.0), I::new(0.0, 0.0));
    assert_eq!(
        I::new(1.0, f64::INFINITY) * I::new(-5.0, -1.0),
        I::new(f64::NEG_INFINITY, -1.0)
    );
    assert_eq!(I::new(1.0, f64::INFINITY) * I::new(-5.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(1.0, f64::INFINITY) * I::new(1.0, 3.0),
        I::new(1.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(1.0, f64::INFINITY) * I::new(f64::NEG_INFINITY, -1.0),
        I::new(f64::NEG_INFINITY, -1.0)
    );
    assert_eq!(
        I::new(1.0, f64::INFINITY) * I::new(f64::NEG_INFINITY, 3.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(1.0, f64::INFINITY) * I::new(-5.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(1.0, f64::INFINITY) * I::new(1.0, f64::INFINITY),
        I::new(1.0, f64::INFINITY)
    );
    assert_eq!(I::new(1.0, f64::INFINITY) * I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(-1.0, f64::INFINITY) * I::new(0.0, 0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-1.0, f64::INFINITY) * I::new(-0.0, -0.0), I::new(0.0, 0.0));
    assert_eq!(
        I::new(-1.0, f64::INFINITY) * I::new(-5.0, -1.0),
        I::new(f64::NEG_INFINITY, 5.0)
    );
    assert_eq!(I::new(-1.0, f64::INFINITY) * I::new(-5.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(-1.0, f64::INFINITY) * I::new(1.0, 3.0),
        I::new(-3.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-1.0, f64::INFINITY) * I::new(f64::NEG_INFINITY, -1.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(-1.0, f64::INFINITY) * I::new(f64::NEG_INFINITY, 3.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(-1.0, f64::INFINITY) * I::new(-5.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(-1.0, f64::INFINITY) * I::new(1.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(I::new(-1.0, f64::INFINITY) * I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(f64::NEG_INFINITY, 3.0) * I::new(0.0, 0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(f64::NEG_INFINITY, 3.0) * I::new(-0.0, -0.0), I::new(0.0, 0.0));
    assert_eq!(
        I::new(f64::NEG_INFINITY, 3.0) * I::new(-5.0, -1.0),
        I::new(-15.0, f64::INFINITY)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, 3.0) * I::new(-5.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(f64::NEG_INFINITY, 3.0) * I::new(1.0, 3.0),
        I::new(f64::NEG_INFINITY, 9.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 3.0) * I::new(f64::NEG_INFINITY, -1.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 3.0) * I::new(f64::NEG_INFINITY, 3.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 3.0) * I::new(-5.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 3.0) * I::new(1.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(I::new(f64::NEG_INFINITY, 3.0) * I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(f64::NEG_INFINITY, -3.0) * I::new(0.0, 0.0), I::new(0.0, 0.0));
    assert_eq!(
        I::new(f64::NEG_INFINITY, -3.0) * I::new(-0.0, -0.0),
        I::new(0.0, 0.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -3.0) * I::new(-5.0, -1.0),
        I::new(3.0, f64::INFINITY)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, -3.0) * I::new(-5.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(f64::NEG_INFINITY, -3.0) * I::new(1.0, 3.0),
        I::new(f64::NEG_INFINITY, -3.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -3.0) * I::new(f64::NEG_INFINITY, -1.0),
        I::new(3.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -3.0) * I::new(f64::NEG_INFINITY, 3.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -3.0) * I::new(-5.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -3.0) * I::new(1.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, -3.0)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, -3.0) * I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(0.0, 0.0) * I::new(0.0, 0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) * I::new(-0.0, -0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) * I::new(-5.0, -1.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) * I::new(-5.0, 3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) * I::new(1.0, 3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) * I::new(f64::NEG_INFINITY, -1.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) * I::new(f64::NEG_INFINITY, 3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) * I::new(-5.0, f64::INFINITY), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) * I::new(1.0, f64::INFINITY), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) * I::ENTIRE, I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) * I::new(0.0, 0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) * I::new(-0.0, -0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) * I::new(-5.0, -1.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) * I::new(-5.0, 3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) * I::new(1.0, 3.0), I::new(0.0, 0.0));
    assert_eq!(
        I::new(-0.0, -0.0) * I::new(f64::NEG_INFINITY, -1.0),
        I::new(0.0, 0.0)
    );
    assert_eq!(I::new(-0.0, -0.0) * I::new(f64::NEG_INFINITY, 3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) * I::new(-5.0, f64::INFINITY), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) * I::new(1.0, f64::INFINITY), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) * I::ENTIRE, I::new(0.0, 0.0));
    assert_eq!(I::new(1.0, 5.0) * I::new(0.0, 0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(1.0, 5.0) * I::new(-0.0, -0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(1.0, 5.0) * I::new(-5.0, -1.0), I::new(-25.0, -1.0));
    assert_eq!(I::new(1.0, 5.0) * I::new(-5.0, 3.0), I::new(-25.0, 15.0));
    assert_eq!(I::new(1.0, 5.0) * I::new(1.0, 3.0), I::new(1.0, 15.0));
    assert_eq!(
        I::new(1.0, 5.0) * I::new(f64::NEG_INFINITY, -1.0),
        I::new(f64::NEG_INFINITY, -1.0)
    );
    assert_eq!(
        I::new(1.0, 5.0) * I::new(f64::NEG_INFINITY, 3.0),
        I::new(f64::NEG_INFINITY, 15.0)
    );
    assert_eq!(
        I::new(1.0, 5.0) * I::new(-5.0, f64::INFINITY),
        I::new(-25.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(1.0, 5.0) * I::new(1.0, f64::INFINITY),
        I::new(1.0, f64::INFINITY)
    );
    assert_eq!(I::new(1.0, 5.0) * I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(-1.0, 5.0) * I::new(0.0, 0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-1.0, 5.0) * I::new(-0.0, -0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-1.0, 5.0) * I::new(-5.0, -1.0), I::new(-25.0, 5.0));
    //min max
    assert_eq!(I::new(-1.0, 5.0) * I::new(-5.0, 3.0), I::new(-25.0, 15.0));
    assert_eq!(I::new(-10.0, 2.0) * I::new(-5.0, 3.0), I::new(-30.0, 50.0));
    assert_eq!(I::new(-1.0, 5.0) * I::new(-1.0, 10.0), I::new(-10.0, 50.0));
    assert_eq!(I::new(-2.0, 2.0) * I::new(-5.0, 3.0), I::new(-10.0, 10.0));
    //end min max
    assert_eq!(I::new(-1.0, 5.0) * I::new(1.0, 3.0), I::new(-3.0, 15.0));
    assert_eq!(I::new(-1.0, 5.0) * I::new(f64::NEG_INFINITY, -1.0), I::ENTIRE);
    assert_eq!(I::new(-1.0, 5.0) * I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE);
    assert_eq!(I::new(-1.0, 5.0) * I::new(-5.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(I::new(-1.0, 5.0) * I::new(1.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(I::new(-1.0, 5.0) * I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(-10.0, -5.0) * I::new(0.0, 0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-10.0, -5.0) * I::new(-0.0, -0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-10.0, -5.0) * I::new(-5.0, -1.0), I::new(5.0, 50.0));
    assert_eq!(I::new(-10.0, -5.0) * I::new(-5.0, 3.0), I::new(-30.0, 50.0));
    assert_eq!(I::new(-10.0, -5.0) * I::new(1.0, 3.0), I::new(-30.0, -5.0));
    assert_eq!(
        I::new(-10.0, -5.0) * I::new(f64::NEG_INFINITY, -1.0),
        I::new(5.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-10.0, -5.0) * I::new(f64::NEG_INFINITY, 3.0),
        I::new(-30.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-10.0, -5.0) * I::new(-5.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 50.0)
    );
    assert_eq!(
        I::new(-10.0, -5.0) * I::new(1.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, -5.0)
    );
    assert_eq!(I::new(-10.0, -5.0) * I::ENTIRE, I::ENTIRE);
    assert_eq!(
        I::new(0.1, 1.9999999999999964) * I::new(-1.9999999999999964, f64::INFINITY),
        I::new(-3.9999999999999862, f64::INFINITY)
    );
    assert_eq!(
        I::new(-0.1, 1.9999999999999964) * I::new(-1.9999999999999964, -0.1),
        I::new(-3.9999999999999862, 0.19999999999999968)
    );
    assert_eq!(
        I::new(-0.1, 0.1) * I::new(-1.9999999999999964, 0.1),
        I::new(-0.19999999999999968, 0.19999999999999968)
    );
    assert_eq!(
        I::new(-1.9999999999999964, -0.1) * I::new(0.1, 1.9999999999999964),
        I::new(-3.9999999999999862, -0.01)
    );
}


#[test]
fn minimal_div_test() {
    assert_eq!(I::EMPTY / I::EMPTY, I::EMPTY);
    assert_eq!(I::new(-1.0, 1.0) / I::EMPTY, I::EMPTY);
    assert_eq!(I::EMPTY / I::new(-1.0, 1.0), I::EMPTY);
    assert_eq!(I::EMPTY / I::new(0.1, 1.0), I::EMPTY);
    assert_eq!(I::EMPTY / I::new(-1.0, -0.1), I::EMPTY);
    assert_eq!(I::EMPTY / I::ENTIRE, I::EMPTY);
    assert_eq!(I::ENTIRE / I::EMPTY, I::EMPTY);
    assert_eq!(I::new(0.0, 0.0) / I::EMPTY, I::EMPTY);
    assert_eq!(I::EMPTY / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(I::new(-0.0, -0.0) / I::EMPTY, I::EMPTY);
    assert_eq!(I::EMPTY / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(I::ENTIRE / I::new(-5.0, -3.0), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(3.0, 5.0), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(f64::NEG_INFINITY, -3.0), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(3.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(I::ENTIRE / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(I::ENTIRE / I::new(-3.0, 0.0), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(-3.0, -0.0), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(0.0, 3.0), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(f64::NEG_INFINITY, 0.0), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(-0.0, 3.0), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(f64::NEG_INFINITY, -0.0), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(-3.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(0.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::new(-0.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(I::ENTIRE / I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(-30.0, -15.0) / I::new(-5.0, -3.0), I::new(3.0, 10.0));
    assert_eq!(I::new(-30.0, -15.0) / I::new(3.0, 5.0), I::new(-10.0, -3.0));
    assert_eq!(
        I::new(-30.0, -15.0) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(0.0, 10.0)
    );
    assert_eq!(I::new(-30.0, -15.0) / I::new(3.0, f64::INFINITY), I::new(-10.0, 0.0));
    assert_eq!(I::new(-30.0, -15.0) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(I::new(-30.0, -15.0) / I::new(-3.0, 0.0), I::new(5.0, f64::INFINITY));
    assert_eq!(I::new(-30.0, -15.0) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(I::new(-30.0, -15.0) / I::new(-3.0, -0.0), I::new(5.0, f64::INFINITY));
    assert_eq!(I::new(-30.0, -15.0) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(-30.0, -15.0) / I::new(0.0, 3.0),
        I::new(f64::NEG_INFINITY, -5.0)
    );
    assert_eq!(
        I::new(-30.0, -15.0) / I::new(f64::NEG_INFINITY, 0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-30.0, -15.0) / I::new(-0.0, 3.0),
        I::new(f64::NEG_INFINITY, -5.0)
    );
    assert_eq!(
        I::new(-30.0, -15.0) / I::new(f64::NEG_INFINITY, -0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(-30.0, -15.0) / I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE);
    assert_eq!(I::new(-30.0, -15.0) / I::new(-3.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(
        I::new(-30.0, -15.0) / I::new(0.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(-30.0, -15.0) / I::new(-0.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(-30.0, -15.0) / I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(-30.0, 15.0) / I::new(-5.0, -3.0), I::new(-5.0, 10.0));
    assert_eq!(I::new(-30.0, 15.0) / I::new(3.0, 5.0), I::new(-10.0, 5.0));
    assert_eq!(
        I::new(-30.0, 15.0) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(-5.0, 10.0)
    );
    assert_eq!(I::new(-30.0, 15.0) / I::new(3.0, f64::INFINITY), I::new(-10.0, 5.0));
    assert_eq!(I::new(-30.0, 15.0) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(I::new(-30.0, 15.0) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(I::new(-30.0, 15.0) / I::new(-3.0, 0.0), I::ENTIRE);
    assert_eq!(I::new(-30.0, 15.0) / I::new(-3.0, -0.0), I::ENTIRE);
    assert_eq!(I::new(-30.0, 15.0) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(I::new(-30.0, 15.0) / I::new(0.0, 3.0), I::ENTIRE);
    assert_eq!(I::new(-30.0, 15.0) / I::new(f64::NEG_INFINITY, 0.0), I::ENTIRE);
    assert_eq!(I::new(-30.0, 15.0) / I::new(-0.0, 3.0), I::ENTIRE);
    assert_eq!(I::new(-30.0, 15.0) / I::new(f64::NEG_INFINITY, -0.0), I::ENTIRE);
    assert_eq!(I::new(-30.0, 15.0) / I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE);
    assert_eq!(I::new(-30.0, 15.0) / I::new(-3.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(I::new(-30.0, 15.0) / I::new(0.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(I::new(-30.0, 15.0) / I::new(-0.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(I::new(-30.0, 15.0) / I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(15.0, 30.0) / I::new(-5.0, -3.0), I::new(-10.0, -3.0));
    assert_eq!(I::new(15.0, 30.0) / I::new(3.0, 5.0), I::new(3.0, 10.0));
    assert_eq!(
        I::new(15.0, 30.0) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(-10.0, 0.0)
    );
    assert_eq!(I::new(15.0, 30.0) / I::new(3.0, f64::INFINITY), I::new(0.0, 10.0));
    assert_eq!(I::new(15.0, 30.0) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(
        I::new(15.0, 30.0) / I::new(-3.0, 0.0),
        I::new(f64::NEG_INFINITY, -5.0)
    );
    assert_eq!(I::new(15.0, 30.0) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(
        I::new(15.0, 30.0) / I::new(-3.0, -0.0),
        I::new(f64::NEG_INFINITY, -5.0)
    );
    assert_eq!(I::new(15.0, 30.0) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(I::new(15.0, 30.0) / I::new(0.0, 3.0), I::new(5.0, f64::INFINITY));
    assert_eq!(
        I::new(15.0, 30.0) / I::new(f64::NEG_INFINITY, 0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(15.0, 30.0) / I::new(-0.0, 3.0), I::new(5.0, f64::INFINITY));
    assert_eq!(
        I::new(15.0, 30.0) / I::new(f64::NEG_INFINITY, -0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(15.0, 30.0) / I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE);
    assert_eq!(I::new(15.0, 30.0) / I::new(-3.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(
        I::new(15.0, 30.0) / I::new(0.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(15.0, 30.0) / I::new(-0.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(15.0, 30.0) / I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(0.0, 0.0) / I::new(-5.0, -3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(3.0, 5.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(f64::NEG_INFINITY, -3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(3.0, f64::INFINITY), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(I::new(0.0, 0.0) / I::new(-3.0, 0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(I::new(0.0, 0.0) / I::new(-3.0, -0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(-3.0, 3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(0.0, 3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(f64::NEG_INFINITY, 0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(-0.0, 3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(f64::NEG_INFINITY, -0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(f64::NEG_INFINITY, 3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(-3.0, f64::INFINITY), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(0.0, f64::INFINITY), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::new(-0.0, f64::INFINITY), I::new(0.0, 0.0));
    assert_eq!(I::new(0.0, 0.0) / I::ENTIRE, I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) / I::new(-5.0, -3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) / I::new(3.0, 5.0), I::new(0.0, 0.0));
    assert_eq!(
        I::new(-0.0, -0.0) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(0.0, 0.0)
    );
    assert_eq!(I::new(-0.0, -0.0) / I::new(3.0, f64::INFINITY), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(I::new(-0.0, -0.0) / I::new(-3.0, 0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(I::new(-0.0, -0.0) / I::new(-3.0, -0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) / I::new(-3.0, 3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) / I::new(0.0, 3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) / I::new(f64::NEG_INFINITY, 0.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) / I::new(-0.0, 3.0), I::new(0.0, 0.0));
    assert_eq!(
        I::new(-0.0, -0.0) / I::new(f64::NEG_INFINITY, -0.0),
        I::new(0.0, 0.0)
    );
    assert_eq!(I::new(-0.0, -0.0) / I::new(f64::NEG_INFINITY, 3.0), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) / I::new(-3.0, f64::INFINITY), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) / I::new(0.0, f64::INFINITY), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) / I::new(-0.0, f64::INFINITY), I::new(0.0, 0.0));
    assert_eq!(I::new(-0.0, -0.0) / I::ENTIRE, I::new(0.0, 0.0));
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(-5.0, -3.0),
        I::new(3.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(3.0, 5.0),
        I::new(f64::NEG_INFINITY, -3.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(3.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, -15.0) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(-3.0, 0.0),
        I::new(5.0, f64::INFINITY)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, -15.0) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(-3.0, -0.0),
        I::new(5.0, f64::INFINITY)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, -15.0) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(0.0, 3.0),
        I::new(f64::NEG_INFINITY, -5.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(f64::NEG_INFINITY, 0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(-0.0, 3.0),
        I::new(f64::NEG_INFINITY, -5.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(f64::NEG_INFINITY, -0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(f64::NEG_INFINITY, 3.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(-3.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(0.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -15.0) / I::new(-0.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, -15.0) / I::ENTIRE, I::ENTIRE);
    assert_eq!(
        I::new(f64::NEG_INFINITY, 15.0) / I::new(-5.0, -3.0),
        I::new(-5.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 15.0) / I::new(3.0, 5.0),
        I::new(f64::NEG_INFINITY, 5.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 15.0) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(-5.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 15.0) / I::new(3.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 5.0)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, 15.0) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(I::new(f64::NEG_INFINITY, 15.0) / I::new(-3.0, 0.0), I::ENTIRE);
    assert_eq!(I::new(f64::NEG_INFINITY, 15.0) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(I::new(f64::NEG_INFINITY, 15.0) / I::new(-3.0, -0.0), I::ENTIRE);
    assert_eq!(I::new(f64::NEG_INFINITY, 15.0) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(I::new(f64::NEG_INFINITY, 15.0) / I::new(0.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(f64::NEG_INFINITY, 15.0) / I::new(f64::NEG_INFINITY, 0.0),
        I::ENTIRE
    );
    assert_eq!(I::new(f64::NEG_INFINITY, 15.0) / I::new(-0.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(f64::NEG_INFINITY, 15.0) / I::new(f64::NEG_INFINITY, -0.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 15.0) / I::new(f64::NEG_INFINITY, 3.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 15.0) / I::new(-3.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 15.0) / I::new(0.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 15.0) / I::new(-0.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(I::new(f64::NEG_INFINITY, 15.0) / I::ENTIRE, I::ENTIRE);
    assert_eq!(
        I::new(-15.0, f64::INFINITY) / I::new(-5.0, -3.0),
        I::new(f64::NEG_INFINITY, 5.0)
    );
    assert_eq!(
        I::new(-15.0, f64::INFINITY) / I::new(3.0, 5.0),
        I::new(-5.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-15.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(f64::NEG_INFINITY, 5.0)
    );
    assert_eq!(
        I::new(-15.0, f64::INFINITY) / I::new(3.0, f64::INFINITY),
        I::new(-5.0, f64::INFINITY)
    );
    assert_eq!(I::new(-15.0, f64::INFINITY) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(I::new(-15.0, f64::INFINITY) / I::new(-3.0, 0.0), I::ENTIRE);
    assert_eq!(I::new(-15.0, f64::INFINITY) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(I::new(-15.0, f64::INFINITY) / I::new(-3.0, -0.0), I::ENTIRE);
    assert_eq!(I::new(-15.0, f64::INFINITY) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(I::new(-15.0, f64::INFINITY) / I::new(0.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(-15.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, 0.0),
        I::ENTIRE
    );
    assert_eq!(I::new(-15.0, f64::INFINITY) / I::new(-0.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(-15.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, -0.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(-15.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, 3.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(-15.0, f64::INFINITY) / I::new(-3.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(-15.0, f64::INFINITY) / I::new(0.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(-15.0, f64::INFINITY) / I::new(-0.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(I::new(-15.0, f64::INFINITY) / I::ENTIRE, I::ENTIRE);
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(-5.0, -3.0),
        I::new(f64::NEG_INFINITY, -3.0)
    );
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(3.0, 5.0),
        I::new(3.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(3.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(15.0, f64::INFINITY) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(-3.0, 0.0),
        I::new(f64::NEG_INFINITY, -5.0)
    );
    assert_eq!(I::new(15.0, f64::INFINITY) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(-3.0, -0.0),
        I::new(f64::NEG_INFINITY, -5.0)
    );
    assert_eq!(I::new(15.0, f64::INFINITY) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(0.0, 3.0),
        I::new(5.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, 0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(-0.0, 3.0),
        I::new(5.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, -0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, 3.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(-3.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(0.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(15.0, f64::INFINITY) / I::new(-0.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(15.0, f64::INFINITY) / I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(-30.0, 0.0) / I::new(-5.0, -3.0), I::new(0.0, 10.0));
    assert_eq!(I::new(-30.0, 0.0) / I::new(3.0, 5.0), I::new(-10.0, 0.0));
    assert_eq!(
        I::new(-30.0, 0.0) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(0.0, 10.0)
    );
    assert_eq!(I::new(-30.0, 0.0) / I::new(3.0, f64::INFINITY), I::new(-10.0, 0.0));
    assert_eq!(I::new(-30.0, 0.0) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(I::new(-30.0, 0.0) / I::new(-3.0, 0.0), I::new(0.0, f64::INFINITY));
    assert_eq!(I::new(-30.0, 0.0) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(I::new(-30.0, 0.0) / I::new(-3.0, -0.0), I::new(0.0, f64::INFINITY));
    assert_eq!(I::new(-30.0, 0.0) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(I::new(-30.0, 0.0) / I::new(0.0, 3.0), I::new(f64::NEG_INFINITY, 0.0));
    assert_eq!(
        I::new(-30.0, 0.0) / I::new(f64::NEG_INFINITY, 0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-30.0, 0.0) / I::new(-0.0, 3.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(-30.0, 0.0) / I::new(f64::NEG_INFINITY, -0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(-30.0, 0.0) / I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE);
    assert_eq!(I::new(-30.0, 0.0) / I::new(-3.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(
        I::new(-30.0, 0.0) / I::new(0.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(-30.0, 0.0) / I::new(-0.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(-30.0, 0.0) / I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(-30.0, -0.0) / I::new(-5.0, -3.0), I::new(0.0, 10.0));
    assert_eq!(I::new(-30.0, -0.0) / I::new(3.0, 5.0), I::new(-10.0, 0.0));
    assert_eq!(
        I::new(-30.0, -0.0) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(0.0, 10.0)
    );
    assert_eq!(I::new(-30.0, -0.0) / I::new(3.0, f64::INFINITY), I::new(-10.0, 0.0));
    assert_eq!(I::new(-30.0, -0.0) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(I::new(-30.0, -0.0) / I::new(-3.0, 0.0), I::new(0.0, f64::INFINITY));
    assert_eq!(I::new(-30.0, -0.0) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(I::new(-30.0, -0.0) / I::new(-3.0, -0.0), I::new(0.0, f64::INFINITY));
    assert_eq!(I::new(-30.0, -0.0) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(-30.0, -0.0) / I::new(0.0, 3.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(-30.0, -0.0) / I::new(f64::NEG_INFINITY, 0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-30.0, -0.0) / I::new(-0.0, 3.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(-30.0, -0.0) / I::new(f64::NEG_INFINITY, -0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(-30.0, -0.0) / I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE);
    assert_eq!(I::new(-30.0, -0.0) / I::new(-3.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(
        I::new(-30.0, -0.0) / I::new(0.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(-30.0, -0.0) / I::new(-0.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(-30.0, -0.0) / I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(0.0, 30.0) / I::new(-5.0, -3.0), I::new(-10.0, 0.0));
    assert_eq!(I::new(0.0, 30.0) / I::new(3.0, 5.0), I::new(0.0, 10.0));
    assert_eq!(
        I::new(0.0, 30.0) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(-10.0, 0.0)
    );
    assert_eq!(I::new(0.0, 30.0) / I::new(3.0, f64::INFINITY), I::new(0.0, 10.0));
    assert_eq!(I::new(0.0, 30.0) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(I::new(0.0, 30.0) / I::new(-3.0, 0.0), I::new(f64::NEG_INFINITY, 0.0));
    assert_eq!(I::new(0.0, 30.0) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(
        I::new(0.0, 30.0) / I::new(-3.0, -0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(0.0, 30.0) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(I::new(0.0, 30.0) / I::new(0.0, 3.0), I::new(0.0, f64::INFINITY));
    assert_eq!(
        I::new(0.0, 30.0) / I::new(f64::NEG_INFINITY, 0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(0.0, 30.0) / I::new(-0.0, 3.0), I::new(0.0, f64::INFINITY));
    assert_eq!(
        I::new(0.0, 30.0) / I::new(f64::NEG_INFINITY, -0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(0.0, 30.0) / I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE);
    assert_eq!(I::new(0.0, 30.0) / I::new(-3.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(
        I::new(0.0, 30.0) / I::new(0.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(0.0, 30.0) / I::new(-0.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(0.0, 30.0) / I::ENTIRE, I::ENTIRE);
    assert_eq!(I::new(-0.0, 30.0) / I::new(-5.0, -3.0), I::new(-10.0, 0.0));
    assert_eq!(I::new(-0.0, 30.0) / I::new(3.0, 5.0), I::new(0.0, 10.0));
    assert_eq!(
        I::new(-0.0, 30.0) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(-10.0, 0.0)
    );
    assert_eq!(I::new(-0.0, 30.0) / I::new(3.0, f64::INFINITY), I::new(0.0, 10.0));
    assert_eq!(I::new(-0.0, 30.0) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(
        I::new(-0.0, 30.0) / I::new(-3.0, 0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(-0.0, 30.0) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(
        I::new(-0.0, 30.0) / I::new(-3.0, -0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(-0.0, 30.0) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(I::new(-0.0, 30.0) / I::new(0.0, 3.0), I::new(0.0, f64::INFINITY));
    assert_eq!(
        I::new(-0.0, 30.0) / I::new(f64::NEG_INFINITY, 0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(-0.0, 30.0) / I::new(-0.0, 3.0), I::new(0.0, f64::INFINITY));
    assert_eq!(
        I::new(-0.0, 30.0) / I::new(f64::NEG_INFINITY, -0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(-0.0, 30.0) / I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE);
    assert_eq!(I::new(-0.0, 30.0) / I::new(-3.0, f64::INFINITY), I::ENTIRE);
    assert_eq!(
        I::new(-0.0, 30.0) / I::new(0.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-0.0, 30.0) / I::new(-0.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(-0.0, 30.0) / I::ENTIRE, I::ENTIRE);
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(-5.0, -3.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(3.0, 5.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(3.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, 0.0) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(-3.0, 0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, 0.0) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(-3.0, -0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, 0.0) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(0.0, 3.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(f64::NEG_INFINITY, 0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(-0.0, 3.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(f64::NEG_INFINITY, -0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(f64::NEG_INFINITY, 3.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(-3.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(0.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0) / I::new(-0.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, 0.0) / I::ENTIRE, I::ENTIRE);
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(-5.0, -3.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(3.0, 5.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(3.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, -0.0) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(-3.0, 0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, -0.0) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(-3.0, -0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, -0.0) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(0.0, 3.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(f64::NEG_INFINITY, 0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(-0.0, 3.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(f64::NEG_INFINITY, -0.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(f64::NEG_INFINITY, 3.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(-3.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(0.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0) / I::new(-0.0, f64::INFINITY),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, -0.0) / I::ENTIRE, I::ENTIRE);
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(-5.0, -3.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(3.0, 5.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(3.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(0.0, f64::INFINITY) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(-3.0, 0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(0.0, f64::INFINITY) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(-3.0, -0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(0.0, f64::INFINITY) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(0.0, 3.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, 0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(-0.0, 3.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, -0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, 3.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(-3.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(0.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(0.0, f64::INFINITY) / I::new(-0.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(0.0, f64::INFINITY) / I::ENTIRE, I::ENTIRE);
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(-5.0, -3.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(3.0, 5.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, -3.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(3.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(-0.0, f64::INFINITY) / I::new(0.0, 0.0), I::EMPTY);
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(-3.0, 0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(-0.0, f64::INFINITY) / I::new(-0.0, -0.0), I::EMPTY);
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(-3.0, -0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(-0.0, f64::INFINITY) / I::new(-3.0, 3.0), I::ENTIRE);
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(0.0, 3.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, 0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(-0.0, 3.0),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, -0.0),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(f64::NEG_INFINITY, 3.0),
        I::ENTIRE
    );
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(-3.0, f64::INFINITY),
        I::ENTIRE
    );
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(0.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(
        I::new(-0.0, f64::INFINITY) / I::new(-0.0, f64::INFINITY),
        I::new(0.0, f64::INFINITY)
    );
    assert_eq!(I::new(-0.0, f64::INFINITY) / I::ENTIRE, I::ENTIRE);
    assert_eq!(
        I::new(-2.0, -1.0) / I::new(-10.0, -3.0),
        I::new(0.09999999999999999, 0.6666666666666667)
    );
    assert_eq!(
        I::new(-2.0, -1.0) / I::new(0.0, 10.0),
        I::new(f64::NEG_INFINITY, -0.09999999999999999)
    );
    assert_eq!(
        I::new(-2.0, -1.0) / I::new(-0.0, 10.0),
        I::new(f64::NEG_INFINITY, -0.09999999999999999)
    );
    assert_eq!(I::new(-1.0, 2.0) / I::new(10.0, f64::INFINITY), I::new(-0.1, 0.2));
    assert_eq!(
        I::new(1.0, 3.0) / I::new(f64::NEG_INFINITY, -10.0),
        I::new(-0.30000000000000004, 0.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -1.0) / I::new(1.0, 3.0),
        I::new(f64::NEG_INFINITY, -0.3333333333333333)
    );
}


#[test]
fn minimal_recip_test() {
    assert_eq!(I::new(-50.0, -10.0).recip(), I::new(-0.1, -0.019999999999999997));
    assert_eq!(I::new(10.0, 50.0).recip(), I::new(0.019999999999999997, 0.1));
    assert_eq!(I::new(f64::NEG_INFINITY, -10.0).recip(), I::new(-0.1, 0.0));
    assert_eq!(I::new(10.0, f64::INFINITY).recip(), I::new(0.0, 0.1));
    assert_eq!(I::new(0.0, 0.0).recip(), I::EMPTY);
    assert_eq!(I::new(-0.0, -0.0).recip(), I::EMPTY);
    assert_eq!(
        I::new(-10.0, 0.0).recip(),
        I::new(f64::NEG_INFINITY, -0.09999999999999999)
    );
    assert_eq!(
        I::new(-10.0, -0.0).recip(),
        I::new(f64::NEG_INFINITY, -0.09999999999999999)
    );
    assert_eq!(I::new(-10.0, 10.0).recip(), I::ENTIRE);
    assert_eq!(
        I::new(0.0, 10.0).recip(),
        I::new(0.09999999999999999, f64::INFINITY)
    );
    assert_eq!(
        I::new(-0.0, 10.0).recip(),
        I::new(0.09999999999999999, f64::INFINITY)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, 0.0).recip(),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, -0.0).recip(),
        I::new(f64::NEG_INFINITY, 0.0)
    );
    assert_eq!(I::new(f64::NEG_INFINITY, 10.0).recip(), I::ENTIRE);
    assert_eq!(I::new(-10.0, f64::INFINITY).recip(), I::ENTIRE);
    assert_eq!(I::new(0.0, f64::INFINITY).recip(), I::new(0.0, f64::INFINITY));
    assert_eq!(I::new(-0.0, f64::INFINITY).recip(), I::new(0.0, f64::INFINITY));
    assert_eq!(I::ENTIRE.recip(), I::ENTIRE);
}


// #[test]
// fn minimal_sqr_test() {
//     assert_eq!(I::EMPTY.sqr(), I::EMPTY);
//     assert_eq!(I::ENTIRE.sqr(), I::new(0.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -5e-324).sqr(),
//         I::new(0.0, f64::INFINITY)
//     );
//     assert_eq!(I::new(-1.0, 1.0).sqr(), I::new(0.0, 1.0));
//     assert_eq!(I::new(0.0, 1.0).sqr(), I::new(0.0, 1.0));
//     assert_eq!(I::new(-0.0, 1.0).sqr(), I::new(0.0, 1.0));
//     assert_eq!(I::new(-5.0, 3.0).sqr(), I::new(0.0, 25.0));
//     assert_eq!(I::new(-5.0, 0.0).sqr(), I::new(0.0, 25.0));
//     assert_eq!(I::new(-5.0, -0.0).sqr(), I::new(0.0, 25.0));
//     assert_eq!(I::new(0.1, 0.1).sqr(), I::new(0.01, 0.010000000000000002));
//     assert_eq!(
//         I::new(-1.9999999999999964, 0.1).sqr(),
//         I::new(0.0, 3.9999999999999862)
//     );
//     assert_eq!(
//         I::new(-1.9999999999999964, -1.9999999999999964).sqr(),
//         I::new(3.999999999999986, 3.9999999999999862)
//     );
// }


// #[test]
// fn minimal_sqrt_test() {
//     assert_eq!(I::EMPTY.sqrt(), I::EMPTY);
//     assert_eq!(I::ENTIRE.sqrt(), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(f64::NEG_INFINITY, -5e-324).sqrt(), I::EMPTY);
//     assert_eq!(I::new(-1.0, 1.0).sqrt(), I::new(0.0, 1.0));
//     assert_eq!(I::new(0.0, 1.0).sqrt(), I::new(0.0, 1.0));
//     assert_eq!(I::new(-0.0, 1.0).sqrt(), I::new(0.0, 1.0));
//     assert_eq!(I::new(-5.0, 25.0).sqrt(), I::new(0.0, 5.0));
//     assert_eq!(I::new(0.0, 25.0).sqrt(), I::new(0.0, 5.0));
//     assert_eq!(I::new(-0.0, 25.0).sqrt(), I::new(0.0, 5.0));
//     assert_eq!(I::new(-5.0, f64::INFINITY).sqrt(), I::new(0.0, f64::INFINITY));
//     assert_eq!(
//         I::new(0.1, 0.1).sqrt(),
//         I::new(0.31622776601683794, 0.316227766016838)
//     );
//     assert_eq!(
//         I::new(-1.9999999999999964, 0.1).sqrt(),
//         I::new(0.0, 0.316227766016838)
//     );
//     assert_eq!(
//         I::new(0.1, 1.9999999999999964).sqrt(),
//         I::new(0.31622776601683794, 1.4142135623730938)
//     );
// }


// #[test]
// fn minimal_fma_test() {
//     assert_eq!(I::EMPTY.mul_add(I::EMPTY, I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-1.0, 1.0).mul_add(I::EMPTY, I::EMPTY), I::EMPTY);
//     assert_eq!(I::EMPTY.mul_add(I::new(-1.0, 1.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::EMPTY.mul_add(I::ENTIRE, I::EMPTY), I::EMPTY);
//     assert_eq!(I::ENTIRE.mul_add(I::EMPTY, I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::EMPTY, I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-0.0, -0.0).mul_add(I::EMPTY, I::EMPTY), I::EMPTY);
//     assert_eq!(I::EMPTY.mul_add(I::new(0.0, 0.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::EMPTY.mul_add(I::new(-0.0, -0.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::ENTIRE.mul_add(I::new(0.0, 0.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::ENTIRE.mul_add(I::new(-0.0, -0.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::ENTIRE.mul_add(I::new(-5.0, -1.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::ENTIRE.mul_add(I::new(-5.0, 3.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::ENTIRE.mul_add(I::new(1.0, 3.0), I::EMPTY), I::EMPTY);
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(f64::NEG_INFINITY, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(f64::NEG_INFINITY, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(-5.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(1.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(I::ENTIRE.mul_add(I::ENTIRE, I::EMPTY), I::EMPTY);
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(0.0, 0.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-0.0, -0.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(1.0, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(1.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::ENTIRE, I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(0.0, 0.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-0.0, -0.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(1.0, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(1.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::ENTIRE, I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(0.0, 0.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-0.0, -0.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(1.0, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(1.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::ENTIRE, I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(0.0, 0.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-0.0, -0.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(1.0, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(1.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::ENTIRE, I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::new(0.0, 0.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::new(-0.0, -0.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::new(-5.0, -1.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::new(-5.0, 3.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::new(1.0, 3.0), I::EMPTY), I::EMPTY);
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-5.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(1.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::ENTIRE, I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-0.0, -0.0).mul_add(I::new(0.0, 0.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-0.0, -0.0).mul_add(I::new(-0.0, -0.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-0.0, -0.0).mul_add(I::new(-5.0, -1.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-0.0, -0.0).mul_add(I::new(-5.0, 3.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-0.0, -0.0).mul_add(I::new(1.0, 3.0), I::EMPTY), I::EMPTY);
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-5.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(1.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(I::new(-0.0, -0.0).mul_add(I::ENTIRE, I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(1.0, 5.0).mul_add(I::new(0.0, 0.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(1.0, 5.0).mul_add(I::new(-0.0, -0.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(1.0, 5.0).mul_add(I::new(-5.0, -1.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(1.0, 5.0).mul_add(I::new(-5.0, 3.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(1.0, 5.0).mul_add(I::new(1.0, 3.0), I::EMPTY), I::EMPTY);
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-5.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(1.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(I::new(1.0, 5.0).mul_add(I::ENTIRE, I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-1.0, 5.0).mul_add(I::new(0.0, 0.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-1.0, 5.0).mul_add(I::new(-0.0, -0.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-1.0, 5.0).mul_add(I::new(-5.0, -1.0), I::EMPTY), I::EMPTY);
//     //min max
//     assert_eq!(I::new(-1.0, 5.0).mul_add(I::new(-5.0, 3.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-10.0, 2.0).mul_add(I::new(-5.0, 3.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-1.0, 5.0).mul_add(I::new(-1.0, 10.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-2.0, 2.0).mul_add(I::new(-5.0, 3.0), I::EMPTY), I::EMPTY);
//     //end min max
//     assert_eq!(I::new(-1.0, 5.0).mul_add(I::new(1.0, 3.0), I::EMPTY), I::EMPTY);
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-5.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(1.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(I::new(-1.0, 5.0).mul_add(I::ENTIRE, I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-10.0, -5.0).mul_add(I::new(0.0, 0.0), I::EMPTY), I::EMPTY);
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-0.0, -0.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(I::new(-10.0, -5.0).mul_add(I::new(-5.0, 3.0), I::EMPTY), I::EMPTY);
//     assert_eq!(I::new(-10.0, -5.0).mul_add(I::new(1.0, 3.0), I::EMPTY), I::EMPTY);
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(1.0, f64::INFINITY), I::EMPTY),
//         I::EMPTY
//     );
//     assert_eq!(I::new(-10.0, -5.0).mul_add(I::ENTIRE, I::EMPTY), I::EMPTY);
//     assert_eq!(
//         I::EMPTY.mul_add(I::EMPTY, I::new(f64::NEG_INFINITY, 2.0)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, 1.0).mul_add(I::EMPTY, I::new(f64::NEG_INFINITY, 2.0)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::EMPTY.mul_add(I::new(-1.0, 1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::EMPTY.mul_add(I::ENTIRE, I::new(f64::NEG_INFINITY, 2.0)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::EMPTY, I::new(f64::NEG_INFINITY, 2.0)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::EMPTY, I::new(f64::NEG_INFINITY, 2.0)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::EMPTY, I::new(f64::NEG_INFINITY, 2.0)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::EMPTY.mul_add(I::new(0.0, 0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::EMPTY.mul_add(I::new(-0.0, -0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(0.0, 0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(-0.0, -0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(-5.0, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(-5.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(1.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(-5.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(1.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::ENTIRE, I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(0.0, 0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-0.0, -0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 1.0)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(1.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 1.0)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(1.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::ENTIRE, I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(0.0, 0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-0.0, -0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 7.0)
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(1.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(1.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::ENTIRE, I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(0.0, 0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-0.0, -0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(1.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 11.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0)
//             .mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0)
//             .mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(1.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::ENTIRE, I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(0.0, 0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-0.0, -0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(1.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, -1.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0)
//             .mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0)
//             .mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(1.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, -1.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::ENTIRE, I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(0.0, 0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-0.0, -0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-5.0, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-5.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(1.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(1.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::ENTIRE, I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(0.0, 0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-0.0, -0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-5.0, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-5.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(1.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(1.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::ENTIRE, I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(0.0, 0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-0.0, -0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-5.0, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 1.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-5.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 17.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(1.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 17.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 1.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 17.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(1.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::ENTIRE, I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(0.0, 0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-0.0, -0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-5.0, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 7.0)
//     );
//     //min max
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-5.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 17.0)
//     );
//     assert_eq!(
//         I::new(-10.0, 2.0).mul_add(I::new(-5.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 52.0)
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-1.0, 10.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 52.0)
//     );
//     assert_eq!(
//         I::new(-2.0, 2.0).mul_add(I::new(-5.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 12.0)
//     );
//     //end min max
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(1.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 17.0)
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(1.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::ENTIRE, I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(0.0, 0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-0.0, -0.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 52.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 52.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(1.0, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, -3.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, 52.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(1.0, f64::INFINITY), I::new(f64::NEG_INFINITY, 2.0)),
//         I::new(f64::NEG_INFINITY, -3.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::ENTIRE, I::new(f64::NEG_INFINITY, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(I::EMPTY.mul_add(I::EMPTY, I::new(-2.0, 2.0)), I::EMPTY);
//     assert_eq!(I::new(-1.0, 1.0).mul_add(I::EMPTY, I::new(-2.0, 2.0)), I::EMPTY);
//     assert_eq!(I::EMPTY.mul_add(I::new(-1.0, 1.0), I::new(-2.0, 2.0)), I::EMPTY);
//     assert_eq!(I::EMPTY.mul_add(I::ENTIRE, I::new(-2.0, 2.0)), I::EMPTY);
//     assert_eq!(I::ENTIRE.mul_add(I::EMPTY, I::new(-2.0, 2.0)), I::EMPTY);
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::EMPTY, I::new(-2.0, 2.0)), I::EMPTY);
//     assert_eq!(I::new(-0.0, -0.0).mul_add(I::EMPTY, I::new(-2.0, 2.0)), I::EMPTY);
//     assert_eq!(I::EMPTY.mul_add(I::new(0.0, 0.0), I::new(-2.0, 2.0)), I::EMPTY);
//     assert_eq!(I::EMPTY.mul_add(I::new(-0.0, -0.0), I::new(-2.0, 2.0)), I::EMPTY);
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(0.0, 0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(-0.0, -0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(-5.0, -1.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(I::ENTIRE.mul_add(I::new(-5.0, 3.0), I::new(-2.0, 2.0)), I::ENTIRE);
//     assert_eq!(I::ENTIRE.mul_add(I::new(1.0, 3.0), I::new(-2.0, 2.0)), I::ENTIRE);
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(I::ENTIRE.mul_add(I::ENTIRE, I::new(-2.0, 2.0)), I::ENTIRE);
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(0.0, 0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-0.0, -0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, -1.0), I::new(-2.0, 2.0)),
//         I::new(f64::NEG_INFINITY, 1.0)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, 3.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(1.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-1.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, 2.0)),
//         I::new(f64::NEG_INFINITY, 1.0)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::new(-1.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::ENTIRE, I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(0.0, 0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-0.0, -0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, -1.0), I::new(-2.0, 2.0)),
//         I::new(f64::NEG_INFINITY, 7.0)
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, 3.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(1.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-5.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::ENTIRE, I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, 2.0)),
//         I::new(-17.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(f64::NEG_INFINITY, 11.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::ENTIRE, I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, 2.0)),
//         I::new(1.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(f64::NEG_INFINITY, -1.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, 2.0)),
//         I::new(1.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::new(f64::NEG_INFINITY, -1.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::ENTIRE, I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::ENTIRE, I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::ENTIRE, I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, 2.0)),
//         I::new(-27.0, 1.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-27.0, 17.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-1.0, 17.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, 2.0)),
//         I::new(f64::NEG_INFINITY, 1.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, 2.0)),
//         I::new(f64::NEG_INFINITY, 17.0)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::new(-27.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::new(-1.0, f64::INFINITY)
//     );
//     assert_eq!(I::new(1.0, 5.0).mul_add(I::ENTIRE, I::new(-2.0, 2.0)), I::ENTIRE);
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, 2.0)),
//         I::new(-27.0, 7.0)
//     );
//     //min max
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-27.0, 17.0)
//     );
//     assert_eq!(
//         I::new(-10.0, 2.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-32.0, 52.0)
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-1.0, 10.0), I::new(-2.0, 2.0)),
//         I::new(-12.0, 52.0)
//     );
//     assert_eq!(
//         I::new(-2.0, 2.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-12.0, 12.0)
//     );
//     //end min max
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-5.0, 17.0)
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(I::new(-1.0, 5.0).mul_add(I::ENTIRE, I::new(-2.0, 2.0)), I::ENTIRE);
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, 2.0)),
//         I::new(-2.0, 2.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, 2.0)),
//         I::new(3.0, 52.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-32.0, 52.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, 2.0)),
//         I::new(-32.0, -3.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, 2.0)),
//         I::new(3.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, 2.0)),
//         I::new(-32.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::new(f64::NEG_INFINITY, 52.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, 2.0)),
//         I::new(f64::NEG_INFINITY, -3.0)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::ENTIRE, I::new(-2.0, 2.0)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::EMPTY.mul_add(I::EMPTY, I::new(-2.0, f64::INFINITY)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-1.0, 1.0).mul_add(I::EMPTY, I::new(-2.0, f64::INFINITY)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::EMPTY.mul_add(I::new(-1.0, 1.0), I::new(-2.0, f64::INFINITY)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::EMPTY.mul_add(I::ENTIRE, I::new(-2.0, f64::INFINITY)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::EMPTY, I::new(-2.0, f64::INFINITY)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::EMPTY, I::new(-2.0, f64::INFINITY)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::EMPTY, I::new(-2.0, f64::INFINITY)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::EMPTY.mul_add(I::new(0.0, 0.0), I::new(-2.0, f64::INFINITY)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::EMPTY.mul_add(I::new(-0.0, -0.0), I::new(-2.0, f64::INFINITY)),
//         I::EMPTY
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(0.0, 0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(-0.0, -0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(-5.0, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(-5.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(1.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::ENTIRE, I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(0.0, 0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-0.0, -0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(1.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-1.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::new(-1.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::ENTIRE, I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(0.0, 0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-0.0, -0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(1.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-5.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::ENTIRE, I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-17.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::ENTIRE, I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::new(1.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0)
//             .mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::new(1.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::ENTIRE, I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::ENTIRE, I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::ENTIRE, I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-27.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-27.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-1.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::new(-27.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::new(-1.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::ENTIRE, I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-27.0, f64::INFINITY)
//     );
//     //min max
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-27.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-10.0, 2.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-32.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-1.0, 10.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-12.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-2.0, 2.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-12.0, f64::INFINITY)
//     );
//     //end min max
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-5.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::ENTIRE, I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(0.0, 0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-0.0, -0.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::new(3.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-32.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(1.0, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-32.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::new(-2.0, f64::INFINITY)),
//         I::new(3.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-2.0, f64::INFINITY)),
//         I::new(-32.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(1.0, f64::INFINITY), I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::ENTIRE, I::new(-2.0, f64::INFINITY)),
//         I::ENTIRE
//     );
//     assert_eq!(I::EMPTY.mul_add(I::EMPTY, I::ENTIRE), I::EMPTY);
//     assert_eq!(I::new(-1.0, 1.0).mul_add(I::EMPTY, I::ENTIRE), I::EMPTY);
//     assert_eq!(I::EMPTY.mul_add(I::new(-1.0, 1.0), I::ENTIRE), I::EMPTY);
//     assert_eq!(I::EMPTY.mul_add(I::ENTIRE, I::ENTIRE), I::EMPTY);
//     assert_eq!(I::ENTIRE.mul_add(I::EMPTY, I::ENTIRE), I::EMPTY);
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::EMPTY, I::ENTIRE), I::EMPTY);
//     assert_eq!(I::new(-0.0, -0.0).mul_add(I::EMPTY, I::ENTIRE), I::EMPTY);
//     assert_eq!(I::EMPTY.mul_add(I::new(0.0, 0.0), I::ENTIRE), I::EMPTY);
//     assert_eq!(I::EMPTY.mul_add(I::new(-0.0, -0.0), I::ENTIRE), I::EMPTY);
//     assert_eq!(I::ENTIRE.mul_add(I::new(0.0, 0.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::ENTIRE.mul_add(I::new(-0.0, -0.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::ENTIRE.mul_add(I::new(-5.0, -1.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::ENTIRE.mul_add(I::new(-5.0, 3.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::ENTIRE.mul_add(I::new(1.0, 3.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(f64::NEG_INFINITY, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(-5.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::ENTIRE.mul_add(I::new(1.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(I::ENTIRE.mul_add(I::ENTIRE, I::ENTIRE), I::ENTIRE);
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(0.0, 0.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-0.0, -0.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(1.0, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(-5.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::new(1.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, f64::INFINITY).mul_add(I::ENTIRE, I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(0.0, 0.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-0.0, -0.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(1.0, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(-5.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::new(1.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, f64::INFINITY).mul_add(I::ENTIRE, I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(0.0, 0.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-0.0, -0.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(1.0, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(-5.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::new(1.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 3.0).mul_add(I::ENTIRE, I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(0.0, 0.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-0.0, -0.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(1.0, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(-5.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::new(1.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -3.0).mul_add(I::ENTIRE, I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::new(0.0, 0.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::new(-0.0, -0.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::new(-5.0, -1.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::new(-5.0, 3.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::new(1.0, 3.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(-5.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(0.0, 0.0).mul_add(I::new(1.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(I::new(0.0, 0.0).mul_add(I::ENTIRE, I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::new(-0.0, -0.0).mul_add(I::new(0.0, 0.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-0.0, -0.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-5.0, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-5.0, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(I::new(-0.0, -0.0).mul_add(I::new(1.0, 3.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(-5.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-0.0, -0.0).mul_add(I::new(1.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(I::new(-0.0, -0.0).mul_add(I::ENTIRE, I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::new(1.0, 5.0).mul_add(I::new(0.0, 0.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::new(1.0, 5.0).mul_add(I::new(-0.0, -0.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::new(1.0, 5.0).mul_add(I::new(-5.0, -1.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::new(1.0, 5.0).mul_add(I::new(-5.0, 3.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::new(1.0, 5.0).mul_add(I::new(1.0, 3.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(-5.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(1.0, 5.0).mul_add(I::new(1.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(I::new(1.0, 5.0).mul_add(I::ENTIRE, I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::new(-1.0, 5.0).mul_add(I::new(0.0, 0.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-0.0, -0.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-5.0, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     //min max
//     assert_eq!(I::new(-1.0, 5.0).mul_add(I::new(-5.0, 3.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(
//         I::new(-10.0, 2.0).mul_add(I::new(-5.0, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-1.0, 10.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(I::new(-2.0, 2.0).mul_add(I::new(-5.0, 3.0), I::ENTIRE), I::ENTIRE);
//     //end min max
//     assert_eq!(I::new(-1.0, 5.0).mul_add(I::new(1.0, 3.0), I::ENTIRE), I::ENTIRE);
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(-5.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-1.0, 5.0).mul_add(I::new(1.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(I::new(-1.0, 5.0).mul_add(I::ENTIRE, I::ENTIRE), I::ENTIRE);
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(0.0, 0.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-0.0, -0.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(1.0, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(f64::NEG_INFINITY, -1.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(-5.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(
//         I::new(-10.0, -5.0).mul_add(I::new(1.0, f64::INFINITY), I::ENTIRE),
//         I::ENTIRE
//     );
//     assert_eq!(I::new(-10.0, -5.0).mul_add(I::ENTIRE, I::ENTIRE), I::ENTIRE);
//     assert_eq!(
//         I::new(0.1, 0.5).mul_add(I::new(-5.0, 3.0), I::new(-0.1, 0.1)),
//         I::new(-2.6, 1.6)
//     );
//     assert_eq!(
//         I::new(-0.5, 0.2).mul_add(I::new(-5.0, 3.0), I::new(-0.1, 0.1)),
//         I::new(-1.6, 2.6)
//     );
//     assert_eq!(
//         I::new(-0.5, -0.1).mul_add(I::new(2.0, 3.0), I::new(-0.1, 0.1)),
//         I::new(-1.6, -0.1)
//     );
//     assert_eq!(
//         I::new(-0.5, -0.1).mul_add(I::new(f64::NEG_INFINITY, 3.0), I::new(-0.1, 0.1)),
//         I::new(-1.6, f64::INFINITY)
//     );
// }


// #[cfg(feature = "gmp")]
// #[test]
// fn minimal_pown_test() {
//     assert_eq!(I::EMPTY.powi(0), I::EMPTY);
//     assert_eq!(I::ENTIRE.powi(0), I::new(1.0, 1.0));
//     assert_eq!(I::new(0.0, 0.0).powi(0), I::new(1.0, 1.0));
//     assert_eq!(I::new(-0.0, -0.0).powi(0), I::new(1.0, 1.0));
//     assert_eq!(I::new(13.1, 13.1).powi(0), I::new(1.0, 1.0));
//     assert_eq!(I::new(-7451.145, -7451.145).powi(0), I::new(1.0, 1.0));
//     assert_eq!(
//         I::new(1.7976931348623157e+308, 1.7976931348623157e+308).powi(0),
//         I::new(1.0, 1.0)
//     );
//     assert_eq!(
//         I::new(-1.7976931348623157e+308, -1.7976931348623157e+308).powi(0),
//         I::new(1.0, 1.0)
//     );
//     assert_eq!(I::new(0.0, f64::INFINITY).powi(0), I::new(1.0, 1.0));
//     assert_eq!(I::new(-0.0, f64::INFINITY).powi(0), I::new(1.0, 1.0));
//     assert_eq!(I::new(f64::NEG_INFINITY, 0.0).powi(0), I::new(1.0, 1.0));
//     assert_eq!(I::new(f64::NEG_INFINITY, -0.0).powi(0), I::new(1.0, 1.0));
//     assert_eq!(I::new(-324.3, 2.5).powi(0), I::new(1.0, 1.0));
//     assert_eq!(I::EMPTY.powi(2), I::EMPTY);
//     assert_eq!(I::ENTIRE.powi(2), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(0.0, 0.0).powi(2), I::new(0.0, 0.0));
//     assert_eq!(I::new(-0.0, -0.0).powi(2), I::new(0.0, 0.0));
//     assert_eq!(I::new(13.1, 13.1).powi(2), I::new(171.60999999999999, 171.61));
//     assert_eq!(
//         I::new(-7451.145, -7451.145).powi(2),
//         I::new(55519561.811025, 55519561.81102501)
//     );
//     assert_eq!(
//         I::new(1.7976931348623157e+308, 1.7976931348623157e+308).powi(2),
//         I::new(1.7976931348623157e+308, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-1.7976931348623157e+308, -1.7976931348623157e+308).powi(2),
//         I::new(1.7976931348623157e+308, f64::INFINITY)
//     );
//     assert_eq!(I::new(0.0, f64::INFINITY).powi(2), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(-0.0, f64::INFINITY).powi(2), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(f64::NEG_INFINITY, 0.0).powi(2), I::new(0.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -0.0).powi(2),
//         I::new(0.0, f64::INFINITY)
//     );
//     assert_eq!(I::new(-324.3, 2.5).powi(2), I::new(0.0, 105170.49000000002));
//     assert_eq!(
//         I::new(0.01, 2.33).powi(2),
//         I::new(9.999999999999999e-05, 5.4289000000000005)
//     );
//     assert_eq!(I::new(-1.9, -0.33).powi(2), I::new(0.1089, 3.61));
//     assert_eq!(I::EMPTY.powi(8), I::EMPTY);
//     assert_eq!(I::ENTIRE.powi(8), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(0.0, 0.0).powi(8), I::new(0.0, 0.0));
//     assert_eq!(I::new(-0.0, -0.0).powi(8), I::new(0.0, 0.0));
//     assert_eq!(
//         I::new(13.1, 13.1).powi(8),
//         I::new(867302034.6900622, 867302034.6900623)
//     );
//     assert_eq!(
//         I::new(-7451.145, -7451.145).powi(8),
//         I::new(9.501323805961965e+30, 9.501323805961966e+30)
//     );
//     assert_eq!(
//         I::new(1.7976931348623157e+308, 1.7976931348623157e+308).powi(8),
//         I::new(1.7976931348623157e+308, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-1.7976931348623157e+308, -1.7976931348623157e+308).powi(8),
//         I::new(1.7976931348623157e+308, f64::INFINITY)
//     );
//     assert_eq!(I::new(0.0, f64::INFINITY).powi(8), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(-0.0, f64::INFINITY).powi(8), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(f64::NEG_INFINITY, 0.0).powi(8), I::new(0.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -0.0).powi(8),
//         I::new(0.0, f64::INFINITY)
//     );
//     assert_eq!(I::new(-324.3, 2.5).powi(8), I::new(0.0, 1.2234200379867188e+20));
//     assert_eq!(
//         I::new(0.01, 2.33).powi(8),
//         I::new(1.0000000000000001e-16, 868.6550888106664)
//     );
//     assert_eq!(
//         I::new(-1.9, -0.33).powi(8),
//         I::new(0.00014064086182410005, 169.83563040999996)
//     );
//     assert_eq!(I::EMPTY.powi(1), I::EMPTY);
//     assert_eq!(I::ENTIRE.powi(1), I::ENTIRE);
//     assert_eq!(I::new(0.0, 0.0).powi(1), I::new(0.0, 0.0));
//     assert_eq!(I::new(-0.0, -0.0).powi(1), I::new(0.0, 0.0));
//     assert_eq!(I::new(13.1, 13.1).powi(1), I::new(13.1, 13.1));
//     assert_eq!(I::new(-7451.145, -7451.145).powi(1), I::new(-7451.145, -7451.145));
//     assert_eq!(
//         I::new(1.7976931348623157e+308, 1.7976931348623157e+308).powi(1),
//         I::new(1.7976931348623157e+308, 1.7976931348623157e+308)
//     );
//     assert_eq!(
//         I::new(-1.7976931348623157e+308, -1.7976931348623157e+308).powi(1),
//         I::new(-1.7976931348623157e+308, -1.7976931348623157e+308)
//     );
//     assert_eq!(I::new(0.0, f64::INFINITY).powi(1), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(-0.0, f64::INFINITY).powi(1), I::new(0.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 0.0).powi(1),
//         I::new(f64::NEG_INFINITY, 0.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -0.0).powi(1),
//         I::new(f64::NEG_INFINITY, 0.0)
//     );
//     assert_eq!(I::new(-324.3, 2.5).powi(1), I::new(-324.3, 2.5));
//     assert_eq!(I::new(0.01, 2.33).powi(1), I::new(0.01, 2.33));
//     assert_eq!(I::new(-1.9, -0.33).powi(1), I::new(-1.9, -0.33));
//     assert_eq!(I::EMPTY.powi(3), I::EMPTY);
//     assert_eq!(I::ENTIRE.powi(3), I::ENTIRE);
//     assert_eq!(I::new(0.0, 0.0).powi(3), I::new(0.0, 0.0));
//     assert_eq!(I::new(-0.0, -0.0).powi(3), I::new(0.0, 0.0));
//     assert_eq!(I::new(13.1, 13.1).powi(3), I::new(2248.0909999999994, 2248.091));
//     assert_eq!(
//         I::new(-7451.145, -7451.145).powi(3),
//         I::new(-413684305390.41, -413684305390.4099)
//     );
//     assert_eq!(
//         I::new(1.7976931348623157e+308, 1.7976931348623157e+308).powi(3),
//         I::new(1.7976931348623157e+308, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-1.7976931348623157e+308, -1.7976931348623157e+308).powi(3),
//         I::new(f64::NEG_INFINITY, -1.7976931348623157e+308)
//     );
//     assert_eq!(I::new(0.0, f64::INFINITY).powi(3), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(-0.0, f64::INFINITY).powi(3), I::new(0.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 0.0).powi(3),
//         I::new(f64::NEG_INFINITY, 0.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -0.0).powi(3),
//         I::new(f64::NEG_INFINITY, 0.0)
//     );
//     assert_eq!(I::new(-324.3, 2.5).powi(3), I::new(-34106789.907000005, 15.625));
//     assert_eq!(I::new(0.01, 2.33).powi(3), I::new(1e-06, 12.649337000000003));
//     assert_eq!(
//         I::new(-1.9, -0.33).powi(3),
//         I::new(-6.858999999999999, -0.035937000000000004)
//     );
//     assert_eq!(I::EMPTY.powi(7), I::EMPTY);
//     assert_eq!(I::ENTIRE.powi(7), I::ENTIRE);
//     assert_eq!(I::new(0.0, 0.0).powi(7), I::new(0.0, 0.0));
//     assert_eq!(I::new(-0.0, -0.0).powi(7), I::new(0.0, 0.0));
//     assert_eq!(
//         I::new(13.1, 13.1).powi(7),
//         I::new(66206262.19008108, 66206262.19008109)
//     );
//     assert_eq!(
//         I::new(-7451.145, -7451.145).powi(7),
//         I::new(-1.2751494979579603e+27, -1.27514949795796e+27)
//     );
//     assert_eq!(
//         I::new(1.7976931348623157e+308, 1.7976931348623157e+308).powi(7),
//         I::new(1.7976931348623157e+308, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-1.7976931348623157e+308, -1.7976931348623157e+308).powi(7),
//         I::new(f64::NEG_INFINITY, -1.7976931348623157e+308)
//     );
//     assert_eq!(I::new(0.0, f64::INFINITY).powi(7), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(-0.0, f64::INFINITY).powi(7), I::new(0.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 0.0).powi(7),
//         I::new(f64::NEG_INFINITY, 0.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -0.0).powi(7),
//         I::new(f64::NEG_INFINITY, 0.0)
//     );
//     assert_eq!(
//         I::new(-324.3, 2.5).powi(7),
//         I::new(-3.77249472089645e+17, 610.3515625)
//     );
//     assert_eq!(I::new(0.01, 2.33).powi(7), I::new(1e-14, 372.8133428371959));
//     assert_eq!(
//         I::new(-1.9, -0.33).powi(7),
//         I::new(-89.38717389999998, -0.0004261844297700001)
//     );
//     assert_eq!(I::EMPTY.powi(-2), I::EMPTY);
//     assert_eq!(I::ENTIRE.powi(-2), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(0.0, 0.0).powi(-2), I::EMPTY);
//     assert_eq!(I::new(-0.0, -0.0).powi(-2), I::EMPTY);
//     assert_eq!(
//         I::new(13.1, 13.1).powi(-2),
//         I::new(0.005827166249053085, 0.005827166249053086)
//     );
//     assert_eq!(
//         I::new(-7451.145, -7451.145).powi(-2),
//         I::new(1.8011669533771807e-08, 1.801166953377181e-08)
//     );
//     assert_eq!(
//         I::new(1.7976931348623157e+308, 1.7976931348623157e+308).powi(-2),
//         I::new(0.0, 5e-324)
//     );
//     assert_eq!(
//         I::new(-1.7976931348623157e+308, -1.7976931348623157e+308).powi(-2),
//         I::new(0.0, 5e-324)
//     );
//     assert_eq!(I::new(0.0, f64::INFINITY).powi(-2), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(-0.0, f64::INFINITY).powi(-2), I::new(0.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 0.0).powi(-2),
//         I::new(0.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -0.0).powi(-2),
//         I::new(0.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-324.3, 2.5).powi(-2),
//         I::new(9.508370646556842e-06, f64::INFINITY)
//     );
//     assert_eq!(I::new(0.01, 2.33).powi(-2), I::new(0.18419937740610434, 10000.0));
//     assert_eq!(
//         I::new(-1.9, -0.33).powi(-2),
//         I::new(0.2770083102493075, 9.182736455463727)
//     );
//     assert_eq!(I::EMPTY.powi(-8), I::EMPTY);
//     assert_eq!(I::ENTIRE.powi(-8), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(0.0, 0.0).powi(-8), I::EMPTY);
//     assert_eq!(I::new(-0.0, -0.0).powi(-8), I::EMPTY);
//     assert_eq!(
//         I::new(13.1, 13.1).powi(-8),
//         I::new(1.1530008693653744e-09, 1.1530008693653746e-09)
//     );
//     assert_eq!(
//         I::new(-7451.145, -7451.145).powi(-8),
//         I::new(1.0524849172833286e-31, 1.0524849172833288e-31)
//     );
//     assert_eq!(
//         I::new(1.7976931348623157e+308, 1.7976931348623157e+308).powi(-8),
//         I::new(0.0, 5e-324)
//     );
//     assert_eq!(
//         I::new(-1.7976931348623157e+308, -1.7976931348623157e+308).powi(-8),
//         I::new(0.0, 5e-324)
//     );
//     assert_eq!(I::new(0.0, f64::INFINITY).powi(-8), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(-0.0, f64::INFINITY).powi(-8), I::new(0.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 0.0).powi(-8),
//         I::new(0.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -0.0).powi(-8),
//         I::new(0.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(-324.3, 2.5).powi(-8),
//         I::new(8.173807596331488e-21, f64::INFINITY)
//     );
//     assert_eq!(I::new(0.01, 2.33).powi(-8), I::new(0.0011512049061603571, 1e+16));
//     assert_eq!(
//         I::new(-1.9, -0.33).powi(-8),
//         I::new(0.005888045974722156, 7110.309102419345)
//     );
//     assert_eq!(I::EMPTY.powi(-1), I::EMPTY);
//     assert_eq!(I::ENTIRE.powi(-1), I::ENTIRE);
//     assert_eq!(I::new(0.0, 0.0).powi(-1), I::EMPTY);
//     assert_eq!(I::new(-0.0, -0.0).powi(-1), I::EMPTY);
//     assert_eq!(
//         I::new(13.1, 13.1).powi(-1),
//         I::new(0.07633587786259541, 0.07633587786259542)
//     );
//     assert_eq!(
//         I::new(-7451.145, -7451.145).powi(-1),
//         I::new(-0.00013420756138821617, -0.00013420756138821614)
//     );
//     assert_eq!(
//         I::new(1.7976931348623157e+308, 1.7976931348623157e+308).powi(-1),
//         I::new(5.562684646268003e-309, 5.56268464626801e-309)
//     );
//     assert_eq!(
//         I::new(-1.7976931348623157e+308, -1.7976931348623157e+308).powi(-1),
//         I::new(-5.56268464626801e-309, -5.562684646268003e-309)
//     );
//     assert_eq!(I::new(0.0, f64::INFINITY).powi(-1), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(-0.0, f64::INFINITY).powi(-1), I::new(0.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 0.0).powi(-1),
//         I::new(f64::NEG_INFINITY, 0.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -0.0).powi(-1),
//         I::new(f64::NEG_INFINITY, 0.0)
//     );
//     assert_eq!(I::new(-324.3, 2.5).powi(-1), I::ENTIRE);
//     assert_eq!(I::new(0.01, 2.33).powi(-1), I::new(0.42918454935622313, 100.0));
//     assert_eq!(
//         I::new(-1.9, -0.33).powi(-1),
//         I::new(-3.0303030303030303, -0.5263157894736842)
//     );
//     assert_eq!(I::EMPTY.powi(-3), I::EMPTY);
//     assert_eq!(I::ENTIRE.powi(-3), I::ENTIRE);
//     assert_eq!(I::new(0.0, 0.0).powi(-3), I::EMPTY);
//     assert_eq!(I::new(-0.0, -0.0).powi(-3), I::EMPTY);
//     assert_eq!(
//         I::new(13.1, 13.1).powi(-3),
//         I::new(0.0004448218510727546, 0.00044482185107275467)
//     );
//     assert_eq!(
//         I::new(-7451.145, -7451.145).powi(-3),
//         I::new(-2.4173022446579435e-12, -2.417302244657943e-12)
//     );
//     assert_eq!(
//         I::new(1.7976931348623157e+308, 1.7976931348623157e+308).powi(-3),
//         I::new(0.0, 5e-324)
//     );
//     assert_eq!(
//         I::new(-1.7976931348623157e+308, -1.7976931348623157e+308).powi(-3),
//         I::new(-5e-324, -0.0)
//     );
//     assert_eq!(I::new(0.0, f64::INFINITY).powi(-3), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(-0.0, f64::INFINITY).powi(-3), I::new(0.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 0.0).powi(-3),
//         I::new(f64::NEG_INFINITY, 0.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -0.0).powi(-3),
//         I::new(f64::NEG_INFINITY, 0.0)
//     );
//     assert_eq!(I::new(-324.3, 2.5).powi(-3), I::ENTIRE);
//     assert_eq!(
//         I::new(0.01, 2.33).powi(-3),
//         I::new(0.07905552678373577, 1000000.0)
//     );
//     assert_eq!(
//         I::new(-1.9, -0.33).powi(-3),
//         I::new(-27.82647410746584, -0.14579384749963553)
//     );
//     assert_eq!(I::EMPTY.powi(-7), I::EMPTY);
//     assert_eq!(I::ENTIRE.powi(-7), I::ENTIRE);
//     assert_eq!(I::new(0.0, 0.0).powi(-7), I::EMPTY);
//     assert_eq!(I::new(-0.0, -0.0).powi(-7), I::EMPTY);
//     assert_eq!(
//         I::new(13.1, 13.1).powi(-7),
//         I::new(1.5104311388686403e-08, 1.5104311388686407e-08)
//     );
//     assert_eq!(
//         I::new(-7451.145, -7451.145).powi(-7),
//         I::new(-7.842217728991088e-28, -7.842217728991087e-28)
//     );
//     assert_eq!(
//         I::new(1.7976931348623157e+308, 1.7976931348623157e+308).powi(-7),
//         I::new(0.0, 5e-324)
//     );
//     assert_eq!(
//         I::new(-1.7976931348623157e+308, -1.7976931348623157e+308).powi(-7),
//         I::new(-5e-324, -0.0)
//     );
//     assert_eq!(I::new(0.0, f64::INFINITY).powi(-7), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(-0.0, f64::INFINITY).powi(-7), I::new(0.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 0.0).powi(-7),
//         I::new(f64::NEG_INFINITY, 0.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -0.0).powi(-7),
//         I::new(f64::NEG_INFINITY, 0.0)
//     );
//     assert_eq!(I::new(-324.3, 2.5).powi(-7), I::ENTIRE);
//     assert_eq!(
//         I::new(0.01, 2.33).powi(-7),
//         I::new(0.002682307431353632, 100000000000000.0)
//     );
//     assert_eq!(
//         I::new(-1.9, -0.33).powi(-7),
//         I::new(-2346.402003798384, -0.011187287351972096)
//     );
// }


// #[test]
// fn minimal_sign_test() {
//     assert_eq!(I::EMPTY.sign(), I::EMPTY);
//     assert_eq!(I::new(1.0, 2.0).sign(), I::new(1.0, 1.0));
//     assert_eq!(I::new(-1.0, 2.0).sign(), I::new(-1.0, 1.0));
//     assert_eq!(I::new(-1.0, 0.0).sign(), I::new(-1.0, 0.0));
//     assert_eq!(I::new(0.0, 2.0).sign(), I::new(0.0, 1.0));
//     assert_eq!(I::new(-0.0, 2.0).sign(), I::new(0.0, 1.0));
//     assert_eq!(I::new(-5.0, -2.0).sign(), I::new(-1.0, -1.0));
//     assert_eq!(I::new(0.0, 0.0).sign(), I::new(0.0, 0.0));
//     assert_eq!(I::new(-0.0, -0.0).sign(), I::new(0.0, 0.0));
//     assert_eq!(I::new(-0.0, 0.0).sign(), I::new(0.0, 0.0));
//     assert_eq!(I::ENTIRE.sign(), I::new(-1.0, 1.0));
// }

// #[test]
// fn minimal_ceil_test() {
//     assert_eq!(I::EMPTY.ceil(), I::EMPTY);
//     assert_eq!(I::ENTIRE.ceil(), I::ENTIRE);
//     assert_eq!(I::new(1.1, 2.0).ceil(), I::new(2.0, 2.0));
//     assert_eq!(I::new(-1.1, 2.0).ceil(), I::new(-1.0, 2.0));
//     assert_eq!(I::new(-1.1, 0.0).ceil(), I::new(-1.0, 0.0));
//     assert_eq!(I::new(-1.1, -0.0).ceil(), I::new(-1.0, 0.0));
//     assert_eq!(I::new(-1.1, -0.4).ceil(), I::new(-1.0, 0.0));
//     assert_eq!(I::new(-1.9, 2.2).ceil(), I::new(-1.0, 3.0));
//     assert_eq!(I::new(-1.0, 2.2).ceil(), I::new(-1.0, 3.0));
//     assert_eq!(I::new(0.0, 2.2).ceil(), I::new(0.0, 3.0));
//     assert_eq!(I::new(-0.0, 2.2).ceil(), I::new(0.0, 3.0));
//     assert_eq!(I::new(-1.5, f64::INFINITY).ceil(), I::new(-1.0, f64::INFINITY));
//     assert_eq!(
//         I::new(1.7976931348623157e+308, f64::INFINITY).ceil(),
//         I::new(1.7976931348623157e+308, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 2.2).ceil(),
//         I::new(f64::NEG_INFINITY, 3.0)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, -1.7976931348623157e+308).ceil(),
//         I::new(f64::NEG_INFINITY, -1.7976931348623157e+308)
//     );
// }


// #[test]
// fn minimal_floor_test() {
//     assert_eq!(I::EMPTY.floor(), I::EMPTY);
//     assert_eq!(I::ENTIRE.floor(), I::ENTIRE);
//     assert_eq!(I::new(1.1, 2.0).floor(), I::new(1.0, 2.0));
//     assert_eq!(I::new(-1.1, 2.0).floor(), I::new(-2.0, 2.0));
//     assert_eq!(I::new(-1.1, 0.0).floor(), I::new(-2.0, 0.0));
//     assert_eq!(I::new(-1.1, -0.0).floor(), I::new(-2.0, 0.0));
//     assert_eq!(I::new(-1.1, -0.4).floor(), I::new(-2.0, -1.0));
//     assert_eq!(I::new(-1.9, 2.2).floor(), I::new(-2.0, 2.0));
//     assert_eq!(I::new(-1.0, 2.2).floor(), I::new(-1.0, 2.0));
//     assert_eq!(I::new(0.0, 2.2).floor(), I::new(0.0, 2.0));
//     assert_eq!(I::new(-0.0, 2.2).floor(), I::new(0.0, 2.0));
//     assert_eq!(I::new(-1.5, f64::INFINITY).floor(), I::new(-2.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 2.2).floor(),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
// }

// #[test]
// fn minimal_trunc_test() {
//     assert_eq!(I::EMPTY.trunc(), I::EMPTY);
//     assert_eq!(I::ENTIRE.trunc(), I::ENTIRE);
//     assert_eq!(I::new(1.1, 2.1).trunc(), I::new(1.0, 2.0));
//     assert_eq!(I::new(-1.1, 2.0).trunc(), I::new(-1.0, 2.0));
//     assert_eq!(I::new(-1.1, 0.0).trunc(), I::new(-1.0, 0.0));
//     assert_eq!(I::new(-1.1, -0.0).trunc(), I::new(-1.0, 0.0));
//     assert_eq!(I::new(-1.1, -0.4).trunc(), I::new(-1.0, 0.0));
//     assert_eq!(I::new(-1.9, 2.2).trunc(), I::new(-1.0, 2.0));
//     assert_eq!(I::new(-1.0, 2.2).trunc(), I::new(-1.0, 2.0));
//     assert_eq!(I::new(0.0, 2.2).trunc(), I::new(0.0, 2.0));
//     assert_eq!(I::new(-0.0, 2.2).trunc(), I::new(0.0, 2.0));
//     assert_eq!(I::new(-1.5, f64::INFINITY).trunc(), I::new(-1.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 2.2).trunc(),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
// }

// #[test]
// fn minimal_round_ties_to_even_test() {
//     assert_eq!(I::EMPTY.round_ties_to_even(), I::EMPTY);
//     assert_eq!(I::ENTIRE.round_ties_to_even(), I::ENTIRE);
//     assert_eq!(I::new(1.1, 2.1).round_ties_to_even(), I::new(1.0, 2.0));
//     assert_eq!(I::new(-1.1, 2.0).round_ties_to_even(), I::new(-1.0, 2.0));
//     assert_eq!(I::new(-1.1, -0.4).round_ties_to_even(), I::new(-1.0, 0.0));
//     assert_eq!(I::new(-1.1, 0.0).round_ties_to_even(), I::new(-1.0, 0.0));
//     assert_eq!(I::new(-1.1, -0.0).round_ties_to_even(), I::new(-1.0, 0.0));
//     assert_eq!(I::new(-1.9, 2.2).round_ties_to_even(), I::new(-2.0, 2.0));
//     assert_eq!(I::new(-1.0, 2.2).round_ties_to_even(), I::new(-1.0, 2.0));
//     assert_eq!(I::new(1.5, 2.1).round_ties_to_even(), I::new(2.0, 2.0));
//     assert_eq!(I::new(-1.5, 2.0).round_ties_to_even(), I::new(-2.0, 2.0));
//     assert_eq!(I::new(-1.1, -0.5).round_ties_to_even(), I::new(-1.0, 0.0));
//     assert_eq!(I::new(-1.9, 2.5).round_ties_to_even(), I::new(-2.0, 2.0));
//     assert_eq!(I::new(0.0, 2.5).round_ties_to_even(), I::new(0.0, 2.0));
//     assert_eq!(I::new(-0.0, 2.5).round_ties_to_even(), I::new(0.0, 2.0));
//     assert_eq!(I::new(-1.5, 2.5).round_ties_to_even(), I::new(-2.0, 2.0));
//     assert_eq!(
//         I::new(-1.5, f64::INFINITY).round_ties_to_even(),
//         I::new(-2.0, f64::INFINITY)
//     );
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 2.2).round_ties_to_even(),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
// }


// #[test]
// fn minimal_round_ties_to_away_test() {
//     assert_eq!(I::EMPTY.round(), I::EMPTY);
//     assert_eq!(I::ENTIRE.round(), I::ENTIRE);
//     assert_eq!(I::new(1.1, 2.1).round(), I::new(1.0, 2.0));
//     assert_eq!(I::new(-1.1, 2.0).round(), I::new(-1.0, 2.0));
//     assert_eq!(I::new(-1.1, 0.0).round(), I::new(-1.0, 0.0));
//     assert_eq!(I::new(-1.1, -0.0).round(), I::new(-1.0, -0.0));
//     assert_eq!(I::new(-1.1, -0.4).round(), I::new(-1.0, 0.0));
//     assert_eq!(I::new(-1.9, 2.2).round(), I::new(-2.0, 2.0));
//     assert_eq!(I::new(-1.0, 2.2).round(), I::new(-1.0, 2.0));
//     assert_eq!(I::new(0.5, 2.1).round(), I::new(1.0, 2.0));
//     assert_eq!(I::new(-2.5, 2.0).round(), I::new(-3.0, 2.0));
//     assert_eq!(I::new(-1.1, -0.5).round(), I::new(-1.0, -1.0));
//     assert_eq!(I::new(-1.9, 2.5).round(), I::new(-2.0, 3.0));
//     assert_eq!(I::new(-1.5, 2.5).round(), I::new(-2.0, 3.0));
//     assert_eq!(I::new(0.0, 2.5).round(), I::new(0.0, 3.0));
//     assert_eq!(I::new(-0.0, 2.5).round(), I::new(0.0, 3.0));
//     assert_eq!(I::new(-1.5, f64::INFINITY).round(), I::new(-2.0, f64::INFINITY));
//     assert_eq!(
//         I::new(f64::NEG_INFINITY, 2.2).round(),
//         I::new(f64::NEG_INFINITY, 2.0)
//     );
// }

// #[test]
// fn minimal_abs_test() {
//     assert_eq!(I::EMPTY.abs(), I::EMPTY);
//     assert_eq!(I::ENTIRE.abs(), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(1.1, 2.1).abs(), I::new(1.1, 2.1));
//     assert_eq!(I::new(-1.1, 2.0).abs(), I::new(0.0, 2.0));
//     assert_eq!(I::new(-1.1, 0.0).abs(), I::new(0.0, 1.1));
//     assert_eq!(I::new(-1.1, -0.0).abs(), I::new(0.0, 1.1));
//     assert_eq!(I::new(-1.1, -0.4).abs(), I::new(0.4, 1.1));
//     assert_eq!(I::new(-1.9, 0.2).abs(), I::new(0.0, 1.9));
//     assert_eq!(I::new(0.0, 0.2).abs(), I::new(0.0, 0.2));
//     assert_eq!(I::new(-0.0, 0.2).abs(), I::new(0.0, 0.2));
//     assert_eq!(I::new(-1.5, f64::INFINITY).abs(), I::new(0.0, f64::INFINITY));
//     assert_eq!(I::new(f64::NEG_INFINITY, -2.2).abs(), I::new(2.2, f64::INFINITY));
// }

// #[test]
// fn minimal_min_test() {
//     assert_eq!(I::EMPTY.min(I::new(1.0, 2.0)), I::EMPTY);
//     assert_eq!(I::new(1.0, 2.0).min(I::EMPTY), I::EMPTY);
//     assert_eq!(I::EMPTY.min(I::EMPTY), I::EMPTY);
//     assert_eq!(I::ENTIRE.min(I::new(1.0, 2.0)), I::new(f64::NEG_INFINITY, 2.0));
//     assert_eq!(I::new(1.0, 2.0).min(I::ENTIRE), I::new(f64::NEG_INFINITY, 2.0));
//     assert_eq!(I::ENTIRE.min(I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::EMPTY.min(I::ENTIRE), I::EMPTY);
//     assert_eq!(I::new(1.0, 5.0).min(I::new(2.0, 4.0)), I::new(1.0, 4.0));
//     assert_eq!(I::new(0.0, 5.0).min(I::new(2.0, 4.0)), I::new(0.0, 4.0));
//     assert_eq!(I::new(-0.0, 5.0).min(I::new(2.0, 4.0)), I::new(0.0, 4.0));
//     assert_eq!(I::new(1.0, 5.0).min(I::new(2.0, 8.0)), I::new(1.0, 5.0));
//     assert_eq!(I::new(1.0, 5.0).min(I::ENTIRE), I::new(f64::NEG_INFINITY, 5.0));
//     assert_eq!(I::new(-7.0, -5.0).min(I::new(2.0, 4.0)), I::new(-7.0, -5.0));
//     assert_eq!(I::new(-7.0, 0.0).min(I::new(2.0, 4.0)), I::new(-7.0, 0.0));
//     assert_eq!(I::new(-7.0, -0.0).min(I::new(2.0, 4.0)), I::new(-7.0, 0.0));
// }

// #[test]
// fn minimal_max_test() {
//     assert_eq!(I::EMPTY.max(I::new(1.0, 2.0)), I::EMPTY);
//     assert_eq!(I::new(1.0, 2.0).max(I::EMPTY), I::EMPTY);
//     assert_eq!(I::EMPTY.max(I::EMPTY), I::EMPTY);
//     assert_eq!(I::ENTIRE.max(I::new(1.0, 2.0)), I::new(1.0, f64::INFINITY));
//     assert_eq!(I::new(1.0, 2.0).max(I::ENTIRE), I::new(1.0, f64::INFINITY));
//     assert_eq!(I::ENTIRE.max(I::ENTIRE), I::ENTIRE);
//     assert_eq!(I::EMPTY.max(I::ENTIRE), I::EMPTY);
//     assert_eq!(I::new(1.0, 5.0).max(I::new(2.0, 4.0)), I::new(2.0, 5.0));
//     assert_eq!(I::new(1.0, 5.0).max(I::new(2.0, 8.0)), I::new(2.0, 8.0));
//     assert_eq!(I::new(-1.0, 5.0).max(I::ENTIRE), I::new(-1.0, f64::INFINITY));
//     assert_eq!(I::new(-7.0, -5.0).max(I::new(2.0, 4.0)), I::new(2.0, 4.0));
//     assert_eq!(I::new(-7.0, -5.0).max(I::new(0.0, 4.0)), I::new(0.0, 4.0));
//     assert_eq!(I::new(-7.0, -5.0).max(I::new(-0.0, 4.0)), I::new(0.0, 4.0));
//     assert_eq!(I::new(-7.0, -5.0).max(I::new(-2.0, 0.0)), I::new(-2.0, 0.0));
//     assert_eq!(I::new(-7.0, -5.0).max(I::new(-2.0, -0.0)), I::new(-2.0, 0.0));
// }
