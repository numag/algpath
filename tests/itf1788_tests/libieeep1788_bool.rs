/*
 *
 * Unit tests from libieeep1788 for interval boolean operations
 * (Original author: Marco Nehmeier)
 * converted into portable ITL format by Oliver Heimlich.
 *
 * Copyright 2013-2015 Marco Nehmeier (nehmeier@informatik.uni-wuerzburg.de)
 * Copyright 2015-2017 Oliver Heimlich (oheim@posteo.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
//Language imports

//Test library imports

//Arithmetic library imports

//Preamble
use algpath::reckless_interval::Interval as I;

#[test]
fn minimal_is_empty_test() {
    assert!(I::EMPTY.is_empty());
    assert_eq!(I::new(f64::NEG_INFINITY, f64::INFINITY).is_empty(), false);
    assert_eq!(I::new(1.0, 2.0).is_empty(), false);
    assert_eq!(I::new(-1.0, 2.0).is_empty(), false);
    assert_eq!(I::new(-3.0, -2.0).is_empty(), false);
    assert_eq!(I::new(f64::NEG_INFINITY, 2.0).is_empty(), false);
    assert_eq!(I::new(f64::NEG_INFINITY, 0.0).is_empty(), false);
    assert_eq!(I::new(f64::NEG_INFINITY, -0.0).is_empty(), false);
    assert_eq!(I::new(0.0, f64::INFINITY).is_empty(), false);
    assert_eq!(I::new(-0.0, f64::INFINITY).is_empty(), false);
    assert_eq!(I::new(-0.0, 0.0).is_empty(), false);
    assert_eq!(I::new(0.0, -0.0).is_empty(), false);
    assert_eq!(I::new(0.0, 0.0).is_empty(), false);
    assert_eq!(I::new(-0.0, -0.0).is_empty(), false);
}

#[test]
fn minimal_is_entire_test() {
    assert_eq!(I::EMPTY.is_entire(), false);
    assert!(I::new(f64::NEG_INFINITY, f64::INFINITY).is_entire());
    assert_eq!(I::new(1.0, 2.0).is_entire(), false);
    assert_eq!(I::new(-1.0, 2.0).is_entire(), false);
    assert_eq!(I::new(-3.0, -2.0).is_entire(), false);
    assert_eq!(I::new(f64::NEG_INFINITY, 2.0).is_entire(), false);
    assert_eq!(I::new(f64::NEG_INFINITY, 0.0).is_entire(), false);
    assert_eq!(I::new(f64::NEG_INFINITY, -0.0).is_entire(), false);
    assert_eq!(I::new(0.0, f64::INFINITY).is_entire(), false);
    assert_eq!(I::new(-0.0, f64::INFINITY).is_entire(), false);
    assert_eq!(I::new(-0.0, 0.0).is_entire(), false);
    assert_eq!(I::new(0.0, -0.0).is_entire(), false);
    assert_eq!(I::new(0.0, 0.0).is_entire(), false);
    assert_eq!(I::new(-0.0, -0.0).is_entire(), false);
}

#[test]
fn minimal_equal_test() {
    assert!(I::new(1.0, 2.0) == I::new(1.0, 2.0));
    assert_eq!(I::new(1.0, 2.1) == I::new(1.0, 2.0), false);
    assert!(I::EMPTY == I::EMPTY);
    assert_eq!(I::EMPTY == I::new(1.0, 2.0), false);
    assert!(I::new(f64::NEG_INFINITY, f64::INFINITY) == I::new(f64::NEG_INFINITY, f64::INFINITY));
    assert_eq!(
        I::new(1.0, 2.4) == I::new(f64::NEG_INFINITY, f64::INFINITY),
        false
    );
    assert!(I::new(1.0, f64::INFINITY) == I::new(1.0, f64::INFINITY));
    assert_eq!(I::new(1.0, 2.4) == I::new(1.0, f64::INFINITY), false);
    assert!(I::new(f64::NEG_INFINITY, 2.0) == I::new(f64::NEG_INFINITY, 2.0));
    assert_eq!(
        I::new(f64::NEG_INFINITY, 2.4) == I::new(f64::NEG_INFINITY, 2.0),
        false
    );
    assert!(I::new(-2.0, 0.0) == I::new(-2.0, 0.0));
    assert!(I::new(-0.0, 2.0) == I::new(0.0, 2.0));
    assert!(I::new(-0.0, -0.0) == I::new(0.0, 0.0));
    assert!(I::new(-0.0, 0.0) == I::new(0.0, 0.0));
    assert!(I::new(0.0, -0.0) == I::new(0.0, 0.0));
}

#[test]
fn minimal_subset_test() {
    assert!(I::EMPTY.subset(I::EMPTY));
    assert!(I::EMPTY.subset(I::new(0.0, 4.0)));
    assert!(I::EMPTY.subset(I::new(-0.0, 4.0)));
    assert!(I::EMPTY.subset(I::new(-0.1, 1.0)));
    assert!(I::EMPTY.subset(I::new(-0.1, 0.0)));
    assert!(I::EMPTY.subset(I::new(-0.1, -0.0)));
    assert!(I::EMPTY.subset(I::new(f64::NEG_INFINITY, f64::INFINITY)));
    assert_eq!(I::new(0.0, 4.0).subset(I::EMPTY), false);
    assert_eq!(I::new(-0.0, 4.0).subset(I::EMPTY), false);
    assert_eq!(I::new(-0.1, 1.0).subset(I::EMPTY), false);
    assert_eq!(
        I::new(f64::NEG_INFINITY, f64::INFINITY).subset(I::EMPTY),
        false
    );
    assert!(I::new(0.0, 4.0).subset(I::new(f64::NEG_INFINITY, f64::INFINITY)));
    assert!(I::new(-0.0, 4.0).subset(I::new(f64::NEG_INFINITY, f64::INFINITY)));
    assert!(I::new(-0.1, 1.0).subset(I::new(f64::NEG_INFINITY, f64::INFINITY)));
    assert!(I::new(f64::NEG_INFINITY, f64::INFINITY).subset(I::new(f64::NEG_INFINITY, f64::INFINITY)));
    assert!(I::new(1.0, 2.0).subset(I::new(1.0, 2.0)));
    assert!(I::new(1.0, 2.0).subset(I::new(0.0, 4.0)));
    assert!(I::new(1.0, 2.0).subset(I::new(-0.0, 4.0)));
    assert!(I::new(0.1, 0.2).subset(I::new(0.0, 4.0)));
    assert!(I::new(0.1, 0.2).subset(I::new(-0.0, 4.0)));
    assert!(I::new(-0.1, -0.1).subset(I::new(-4.0, 3.4)));
    assert!(I::new(0.0, 0.0).subset(I::new(-0.0, -0.0)));
    assert!(I::new(-0.0, -0.0).subset(I::new(0.0, 0.0)));
    assert!(I::new(-0.0, 0.0).subset(I::new(0.0, 0.0)));
    assert!(I::new(-0.0, 0.0).subset(I::new(0.0, -0.0)));
    assert!(I::new(0.0, -0.0).subset(I::new(0.0, 0.0)));
    assert!(I::new(0.0, -0.0).subset(I::new(-0.0, 0.0)));
}

#[test]
fn minimal_less_test() {
    assert!(I::EMPTY.less(I::EMPTY));
    assert_eq!(I::new(1.0, 2.0).less(I::EMPTY), false);
    assert_eq!(I::EMPTY.less(I::new(1.0, 2.0)), false);
    assert!(I::new(f64::NEG_INFINITY, f64::INFINITY).less(I::new(f64::NEG_INFINITY, f64::INFINITY)));
    assert_eq!(
        I::new(1.0, 2.0).less(I::new(f64::NEG_INFINITY, f64::INFINITY)),
        false
    );
    assert_eq!(
        I::new(0.0, 2.0).less(I::new(f64::NEG_INFINITY, f64::INFINITY)),
        false
    );
    assert_eq!(
        I::new(-0.0, 2.0).less(I::new(f64::NEG_INFINITY, f64::INFINITY)),
        false
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, f64::INFINITY).less(I::new(1.0, 2.0)),
        false
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, f64::INFINITY).less(I::new(0.0, 2.0)),
        false
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, f64::INFINITY).less(I::new(-0.0, 2.0)),
        false
    );
    assert!(I::new(0.0, 2.0).less(I::new(0.0, 2.0)));
    assert!(I::new(0.0, 2.0).less(I::new(-0.0, 2.0)));
    assert!(I::new(0.0, 2.0).less(I::new(1.0, 2.0)));
    assert!(I::new(-0.0, 2.0).less(I::new(1.0, 2.0)));
    assert!(I::new(1.0, 2.0).less(I::new(1.0, 2.0)));
    assert!(I::new(1.0, 2.0).less(I::new(3.0, 4.0)));
    assert!(I::new(1.0, 3.5).less(I::new(3.0, 4.0)));
    assert!(I::new(1.0, 4.0).less(I::new(3.0, 4.0)));
    assert!(I::new(-2.0, -1.0).less(I::new(-2.0, -1.0)));
    assert!(I::new(-3.0, -1.5).less(I::new(-2.0, -1.0)));
    assert!(I::new(0.0, 0.0).less(I::new(-0.0, -0.0)));
    assert!(I::new(-0.0, -0.0).less(I::new(0.0, 0.0)));
    assert!(I::new(-0.0, 0.0).less(I::new(0.0, 0.0)));
    assert!(I::new(-0.0, 0.0).less(I::new(0.0, -0.0)));
    assert!(I::new(0.0, -0.0).less(I::new(0.0, 0.0)));
    assert!(I::new(0.0, -0.0).less(I::new(-0.0, 0.0)));
}

#[test]
fn minimal_precedes_test() {
    assert!(I::EMPTY.precedes(I::new(3.0, 4.0)));
    assert!(I::new(3.0, 4.0).precedes(I::EMPTY));
    assert!(I::EMPTY.precedes(I::EMPTY));
    assert_eq!(
        I::new(1.0, 2.0).precedes(I::new(f64::NEG_INFINITY, f64::INFINITY)),
        false
    );
    assert_eq!(
        I::new(0.0, 2.0).precedes(I::new(f64::NEG_INFINITY, f64::INFINITY)),
        false
    );
    assert_eq!(
        I::new(-0.0, 2.0).precedes(I::new(f64::NEG_INFINITY, f64::INFINITY)),
        false
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, f64::INFINITY).precedes(I::new(1.0, 2.0)),
        false
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, f64::INFINITY).precedes(I::new(f64::NEG_INFINITY, f64::INFINITY)),
        false
    );
    assert!(I::new(1.0, 2.0).precedes(I::new(3.0, 4.0)));
    assert!(I::new(1.0, 3.0).precedes(I::new(3.0, 4.0)));
    assert!(I::new(-3.0, -1.0).precedes(I::new(-1.0, 0.0)));
    assert!(I::new(-3.0, -1.0).precedes(I::new(-1.0, -0.0)));
    assert_eq!(I::new(1.0, 3.5).precedes(I::new(3.0, 4.0)), false);
    assert_eq!(I::new(1.0, 4.0).precedes(I::new(3.0, 4.0)), false);
    assert_eq!(I::new(-3.0, -0.1).precedes(I::new(-1.0, 0.0)), false);
    assert!(I::new(0.0, 0.0).precedes(I::new(-0.0, -0.0)));
    assert!(I::new(-0.0, -0.0).precedes(I::new(0.0, 0.0)));
    assert!(I::new(-0.0, 0.0).precedes(I::new(0.0, 0.0)));
    assert!(I::new(-0.0, 0.0).precedes(I::new(0.0, -0.0)));
    assert!(I::new(0.0, -0.0).precedes(I::new(0.0, 0.0)));
    assert!(I::new(0.0, -0.0).precedes(I::new(-0.0, 0.0)));
}

#[test]
fn minimal_interior_test() {
    assert!(I::EMPTY.interior(I::EMPTY));
    assert!(I::EMPTY.interior(I::new(0.0, 4.0)));
    assert_eq!(I::new(0.0, 4.0).interior(I::EMPTY), false);
    assert!(I::new(f64::NEG_INFINITY, f64::INFINITY).interior(I::new(f64::NEG_INFINITY, f64::INFINITY)));
    assert!(I::new(0.0, 4.0).interior(I::new(f64::NEG_INFINITY, f64::INFINITY)));
    assert!(I::EMPTY.interior(I::new(f64::NEG_INFINITY, f64::INFINITY)));
    assert_eq!(
        I::new(f64::NEG_INFINITY, f64::INFINITY).interior(I::new(0.0, 4.0)),
        false
    );
    assert_eq!(I::new(0.0, 4.0).interior(I::new(0.0, 4.0)), false);
    assert!(I::new(1.0, 2.0).interior(I::new(0.0, 4.0)));
    assert_eq!(I::new(-2.0, 2.0).interior(I::new(-2.0, 4.0)), false);
    assert!(I::new(-0.0, -0.0).interior(I::new(-2.0, 4.0)));
    assert!(I::new(0.0, 0.0).interior(I::new(-2.0, 4.0)));
    assert_eq!(I::new(0.0, 0.0).interior(I::new(-0.0, -0.0)), false);
    assert_eq!(I::new(0.0, 4.4).interior(I::new(0.0, 4.0)), false);
    assert_eq!(I::new(-1.0, -1.0).interior(I::new(0.0, 4.0)), false);
    assert_eq!(I::new(2.0, 2.0).interior(I::new(-2.0, -1.0)), false);
}

#[test]
fn minimal_strictly_less_test() {
    assert!(I::EMPTY.strict_less(I::EMPTY));
    assert_eq!(I::new(1.0, 2.0).strict_less(I::EMPTY), false);
    assert_eq!(I::EMPTY.strict_less(I::new(1.0, 2.0)), false);
    assert!(
        I::new(f64::NEG_INFINITY, f64::INFINITY).strict_less(I::new(f64::NEG_INFINITY, f64::INFINITY))
    );
    assert_eq!(
        I::new(1.0, 2.0).strict_less(I::new(f64::NEG_INFINITY, f64::INFINITY)),
        false
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, f64::INFINITY).strict_less(I::new(1.0, 2.0)),
        false
    );
    assert_eq!(I::new(1.0, 2.0).strict_less(I::new(1.0, 2.0)), false);
    assert!(I::new(1.0, 2.0).strict_less(I::new(3.0, 4.0)));
    assert!(I::new(1.0, 3.5).strict_less(I::new(3.0, 4.0)));
    assert_eq!(I::new(1.0, 4.0).strict_less(I::new(3.0, 4.0)), false);
    assert_eq!(I::new(0.0, 4.0).strict_less(I::new(0.0, 4.0)), false);
    assert_eq!(I::new(-0.0, 4.0).strict_less(I::new(0.0, 4.0)), false);
    assert_eq!(I::new(-2.0, -1.0).strict_less(I::new(-2.0, -1.0)), false);
    assert!(I::new(-3.0, -1.5).strict_less(I::new(-2.0, -1.0)));
}

#[test]
fn minimal_strictly_precedes_test() {
    assert!(I::EMPTY.strict_precedes(I::new(3.0, 4.0)));
    assert!(I::new(3.0, 4.0).strict_precedes(I::EMPTY));
    assert!(I::EMPTY.strict_precedes(I::EMPTY));
    assert_eq!(
        I::new(1.0, 2.0).strict_precedes(I::new(f64::NEG_INFINITY, f64::INFINITY)),
        false
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, f64::INFINITY).strict_precedes(I::new(1.0, 2.0)),
        false
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, f64::INFINITY)
            .strict_precedes(I::new(f64::NEG_INFINITY, f64::INFINITY)),
        false
    );
    assert!(I::new(1.0, 2.0).strict_precedes(I::new(3.0, 4.0)));
    assert_eq!(I::new(1.0, 3.0).strict_precedes(I::new(3.0, 4.0)), false);
    assert_eq!(I::new(-3.0, -1.0).strict_precedes(I::new(-1.0, 0.0)), false);
    assert_eq!(I::new(-3.0, -0.0).strict_precedes(I::new(0.0, 1.0)), false);
    assert_eq!(I::new(-3.0, 0.0).strict_precedes(I::new(-0.0, 1.0)), false);
    assert_eq!(I::new(1.0, 3.5).strict_precedes(I::new(3.0, 4.0)), false);
    assert_eq!(I::new(1.0, 4.0).strict_precedes(I::new(3.0, 4.0)), false);
    assert_eq!(I::new(-3.0, -0.1).strict_precedes(I::new(-1.0, 0.0)), false);
}

#[test]
fn minimal_disjoint_test() {
    assert!(I::EMPTY.disjoint(I::new(3.0, 4.0)));
    assert!(I::new(3.0, 4.0).disjoint(I::EMPTY));
    assert!(I::EMPTY.disjoint(I::EMPTY));
    assert!(I::new(3.0, 4.0).disjoint(I::new(1.0, 2.0)));
    assert_eq!(I::new(0.0, 0.0).disjoint(I::new(-0.0, -0.0)), false);
    assert_eq!(I::new(0.0, -0.0).disjoint(I::new(-0.0, 0.0)), false);
    assert_eq!(I::new(3.0, 4.0).disjoint(I::new(1.0, 7.0)), false);
    assert_eq!(
        I::new(3.0, 4.0).disjoint(I::new(f64::NEG_INFINITY, f64::INFINITY)),
        false
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, f64::INFINITY).disjoint(I::new(1.0, 7.0)),
        false
    );
    assert_eq!(
        I::new(f64::NEG_INFINITY, f64::INFINITY).disjoint(I::new(f64::NEG_INFINITY, f64::INFINITY)),
        false
    );
}
