use std::{collections::HashMap, path::PathBuf};

use algpath::{num::CIF, prog::Evaluator, reckless_interval::Interval};

#[test]
fn a_bug_on_circuit_evaluation() {
    let conf = algpath::cli::Configuration {
        diff: algpath::circuit::DifferentiationMode::Forward,
        homogenize: false,
        horner: false,
        jobs: Some(1),
        legacy: false,
        markdown_help: false,
        path: Some(PathBuf::from("data/deg3-n4-l3-SINGULAR.json")),
    };

    let mut input_problem = algpath::cli::Problem::new_from_input(&conf).unwrap();
    let parametric_system = input_problem.build_parametric_system(&conf).unwrap();

    dbg!(parametric_system.f[0].instructions.len());
    dbg!(parametric_system.df[0].instructions.len());

    let mut pt = HashMap::<_, CIF>::new();
    pt.insert(
        input_problem.parameters[0].clone(),
        Interval::new(2.0, 2.0).into(),
    );
    for i in 0..4 {
        pt.insert(
            input_problem.variables[i].clone(),
            Interval::new(3.0 + (i as f64), 3.0 + (i as f64)).into(),
        );
    }

    let mut allvars = Vec::new();
    allvars.extend_from_slice(&input_problem.parameters);
    allvars.extend_from_slice(&input_problem.variables);

    let prog = algpath::prog::Program::compile(
        &[input_problem.system[3].derivative(&input_problem.variables[2])].to_vec(),
        &allvars,
    );

    assert_eq!(
        algpath::prog::DefaultEvaluator::eval_map(&prog, &pt, &mut Vec::new())[0],
        input_problem.system[3]
            .derivative(&input_problem.variables[2])
            .eval(&pt)
            .unwrap()
    );

    assert_eq!(
        algpath::prog::DefaultEvaluator::eval_map(&parametric_system.df[0], &pt, &mut Vec::new())[11],
        input_problem.system[3]
            .derivative(&input_problem.variables[2])
            .eval(&pt)
            .unwrap()
    );

}
