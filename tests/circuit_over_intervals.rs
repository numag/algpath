// Copyright 2024 Pierre Lairez
// SPDX-License-Identifier: GPL-3.0-or-later

use std::collections::HashMap;

use algpath::{
    circuit::{Circuit, Context},
    num::{Interval, CIF, ComplexIntervalEvaluator},
    prog::{Program, DefaultEvaluator, Evaluator},
    taylormodel::TaylorModel,
};
use num_traits::One;

#[test]
fn hermite_polynomials() {
    let ctx = Context::<CIF>::default();
    let x = ctx.var("x");

    let two = ctx.scalar(Interval::new(2.0, 2.0).into());

    let mut hpols = Vec::<Circuit<CIF>>::default();

    hpols.push(ctx.scalar(CIF::one()));
    hpols.push(two.clone() * &x);

    for _ in 2..20 {
        let n = hpols.len();
        let n_minus_1 = (n as f64) - 1.0;
        hpols.push(
            two.clone()
                * (x.clone() * &hpols[n - 1]
                    - ctx.scalar(Interval::new(n_minus_1, n_minus_1).into()) * &hpols[n - 2]),
        );
    }

    let mut pt = HashMap::default();
    pt.insert(x.clone(), CIF::one());

    assert!(hpols[4].eval(&pt) == Some(Interval::new(-20.0, -20.0).into()));
    assert!(hpols[10].eval(&pt) == Some(Interval::new(8224.0, 8224.0).into()));

    let der1 = &hpols[12].grad()[&x];
    let der2 = hpols[12].derivative(&x);

    pt.insert(x.clone(), Interval::new(0.5, 0.5).into());

    assert!(der1.eval(&pt) == Some(Interval::new(-2568696.0, -2568696.0).into()));
    assert!(der2.eval(&pt) == Some(Interval::new(-2568696.0, -2568696.0).into()));

    {
        let prog = Program::compile(&[hpols[12].clone(), der1.clone()][..], &[x.clone()][..]);
        let mut buf = Vec::new();

        assert!(DefaultEvaluator::eval_map(&prog, &pt, &mut buf)[0] == hpols[12].eval(&pt).unwrap());
        assert!(DefaultEvaluator::eval_map(&prog, &pt, &mut buf)[1] == der1.eval(&pt).unwrap());
        assert!(ComplexIntervalEvaluator::eval_map(&prog, &pt, &mut buf)[0] == hpols[12].eval(&pt).unwrap());
        assert!(ComplexIntervalEvaluator::eval_map(&prog, &pt, &mut buf)[1] == der1.eval(&pt).unwrap());

    }

    {
        const N: usize = 4;
        let tm = TaylorModel::<N>::affine(
            Interval::new(0.5, 0.5).into(),
            Interval::new(1.0, 1.0).into(),
            Interval::new(-0.1, 0.1),
        );

        let mut pt = HashMap::default();
        pt.insert(x.clone(), tm);
        let prog = Program::compile(&[hpols[12].clone(), der1.clone()][..], &[x.clone()][..]);
        let mut buf = Vec::new();

        let ev = DefaultEvaluator::eval_map(&prog, &pt, &mut buf);
        assert!(ev[0].coeffs[2] == Interval::new(5964024.0, 5964024.0).into());
        assert!(ev[1].coeffs[2] == Interval::new(34219680.0, 34219680.0).into());

        println!("{:?}", ev[0]);

        // Maple:
        // p := expand(HermiteH(12, x));
        // subs(t=1/10,quo(expand(subs(x=1/2+t, p), t), t^3, t));
        // subs(t=-1/10,quo(expand(subs(x=1/2+t, p), t), t^3, t));

        assert!(ev[0].coeffs[3].re().contains(20670183037288. / 1953125.));
        assert!(ev[0].coeffs[3].re().contains(23431923363672. / 1953125.));
    }
}
