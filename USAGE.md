# Command-Line Help for `algpath`

This document contains the help content for the `algpath` command-line program.

**Command Overview:**

* [`algpath`↴](#algpath)

## `algpath`

**Usage:** `algpath [OPTIONS] [PATH]`

###### **Arguments:**

* `<PATH>` — Input file in JSON syntax. If none given, read standard input

###### **Options:**

* `--horner` — puts the equations into Horner form before processing
* `--homogenize` — enable homogenization of equations, does nothing if the input is already homogeneous
* `--diff <DIFF>` — differentiation mode for circuits

  Default value: `forward`

  Possible values: `forward`, `backward`

* `--jobs <JOBS>` — Number of threads. Default to the number of available CPUs, or 1 if the program fails to determine this number


<hr/>

<small><i>
    This document was generated automatically by
    <a href="https://crates.io/crates/clap-markdown"><code>clap-markdown</code></a>.
</i></small>

